import React from 'react'
import { 
    Text, 
    StyleSheet,
    TouchableOpacity,
    View,
    Dimensions
} from 'react-native'
import UnityView, { UnityModule } from 'react-native-unity-view';

const UnityComponent = ({ onPress }) => {

    return (
        <UnityView 
        style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} onUnityMessage={onPress} />
    )

}



export default UnityComponent