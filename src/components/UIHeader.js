import React from 'react';
import {Text, View} from 'react-native';
import { Header } from 'react-native-elements';

const UIHeader = (props) =>
{
    const { textStyle, viewStyle } = styles;

    return (
        <View>
            <Header
                leftComponent={{ icon: 'menu', color: '#fff' }}
                centerComponent={{ text: 'MY TITLE', style: { color: '#fff' } }}
                rightComponent={{ icon: 'home', color: '#fff' }}
            />
        </View>
    );
};


const styles = {
    viewStyle: {
        backgroundColor: '#F8F8F8',
        alignItems: 'center',
        justifyContent: 'center',
        height: 60,
        paddingtop: 15,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 20},
        shadowOpacity: 0.5,
        elevation: 2,
        posotion: 'relative',
    },
    textStyle: {
        fontSize: 20,
    }
};

export default UIHeader;