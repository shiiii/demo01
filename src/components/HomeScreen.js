import React, {Component} from 'react';
import { View, Image, TouchableOpacity} from 'react-native';
//import { Icon } from 'react-native-elements';
import {createStackNavigator} from 'react-navigation-stack';
import {createAppContainer} from 'react-navigation';
//import UnityView, {UnityModule} from 'react-native-unity-view';
import MenuItems from '../Screens/MenuItems';
import UnityDisplay from '../Screens/UnityDisplay';
import Accounts from './Accounts';
import Promotion from '../Screens/Promotion';

const HomeNavigation = createStackNavigator(
    {
        Home: {
            screen: MenuItems, 
            // navigationOptions: {
            //     title: 'Home',
            //     headerTitle: '',
            //     headerLeft: 
            //         <TouchableOpacity onPress={_ => navigation.navigate("Home")}>
            //             <Image style = {{
            //                 marginLeft : 15,
            //                 width : 48,
            //                 height : 48,
            //                 }} 
            //                 source = {require('../uiAssets/menu.png')}>
            //             </Image>
            //         </TouchableOpacity>,
            //     headerRight:  
            //         <TouchableOpacity /*style = {styles.playButton} onPress = {this.onToggleUnity.bind(this)}*/>
            //             <Image style = {{
            //                 marginRight : 15,
            //                 width : 32,
            //                 height : 32,
            //                 }} 
            //                 source = {require('../uiAssets/logout.png')}></Image>
            //         </TouchableOpacity>,
            //     headerStyle:{ backgroundColor: '#00b1f4'},
            //     //Sets Header text of Status Bar
            //     // headerStyle: {
            //     //   backgroundColor: '#f4511e',
            //     //   //Sets Header color
            //     // },
            //     headerTintColor: '#fff',
            //     //Sets Header text color
            //         flex:1,
            //         headerTitleStyle: {
            //         textAlign:"center",
            //         alignSelf: 'center',
            //     //fontWeight: 'bold',
            //     //paddingRight: 50,
            //     //Sets Header text style
            //     },
            // }
            // navigationOptions: {
            //     headerMode: 'none',
            //     header: null,
            // },
        },
        Accounts: {
            screen: Accounts,
            navigationOptions : {
                title: 'Accounts',
            }
        },
        DuitNow: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'DuitNow',
            }
        },
        Transfer: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'Transfer',
            }
        },
        Jompay: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'Jompay',
            }
        },
        Payments: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'Payments',
            }
        },
        QrCode: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'QrCode',
            }
        },
        TopUp: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'TopUp',
            }
        },
        Promo: {
            screen: Promotion,
            // navigationOptions: {
            //     headerMode: 'none',
            //     header: null,
            // }
            // navigationOptions :  ({ navigation }) =>  {
            //     return {
            //         headerTitle: 'Promition',
            //         headerLeft: (<HeaderBackButton onPress={_ => navigation.navigate("Home")}/>),
            //         headerRight:  
            //             <TouchableOpacity onPress={ () => {navigation.navigate("Game")}} >
            //                 <Image style = { style.headerRightImage} source = {require('../uiAssets/game_pad1.png')}></Image>
            //             </TouchableOpacity>,
            //         headerStyle:{ backgroundColor: '#00b1f4'},
            //     }
            // }
        },
        Game: {
            screen: UnityDisplay,
            // navigationOptions: {
            //     headerMode: 'none',
            //     header: null,
            // }
        },
        Settings: {
            screen: UnityDisplay,
            navigationOptions : {
                title: 'Settings',
            }
        }
    },

);

const HomeScreenNavigator = createAppContainer(HomeNavigation);

export default class HomeScreen extends Component {
    render() {
      return (
            <HomeScreenNavigator />
      );
    }
}

// class HomeScreen extends Component {

//     constructor(props) {
//         super(props);
//         this.state = {
//             albums: [] ,
//             clickCount: 0,
//             renderUnity: false,
//             unityPaused: false
//         };
//     }

//     onToggleUnity() {
//         this.setState({ renderUnity: !this.state.renderUnity });
//     }

//     onSendMessageToUnity()
//     {
//         // UnityModule.pause();
//         UnityModule.postMessageToUnityManager({
//             name: 'TestMessage',
//             data: 'i was sent from onSendMessageToUnity in react native app',
//             callBack: (data) => {
//                 Alert.alert('Tip', JSON.stringify(data));
//             }
//         });
//     }

//     onUnityMessage(event) {
//         //this.setState({ clickCount: this.state.clickCount + 1 });
//         console.log('Message received from unity!!! yay')
//         setTimeout(() => {
//             event.send('I am click callback!');
//           }, 2000);
//         //Alert.alert('Receive Message', 'Message receive from unity view');
//         // UnityModule.postMessageToUnityManager({
//         //     name: 'Receive Message',
//         //     data: 'Message send fron react native',
//         // });
//     }
    
//     componentDidMount()
//     {
//         fetch('https://rallycoding.herokuapp.com/api/music_albums')
//        .then (response => response.json())
//        .then((responseData) => this.setState( { albums: responseData }));
//     }

//     renderAlbums ()
//     {
//         return this.state.albums.map(album => 
//             <AlbumDetail key = {album.title} album = {album}></AlbumDetail>
//         );
//     }

//     renderHomeScreen()
//     {
//         return (
//             <View>
//                 <MenuItems/>
//                 { <View style = {styles.unityButonContainer}>
//                     <Button onPress={this.onToggleUnity.bind(this)}></Button>
//                 </View> }
//             </View>
//         );
//     }

//     render ()
//     {
//         const { renderUnity, unityPaused, clickCount } = this.state;
//         let unityElement;

//         if (renderUnity) {
//             unityElement = <UnityView style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} onUnityMessage={this.onUnityMessage.bind(this)} /> 
//         }

//         return(
//             <View style = {{height: '100%'}}>
//                 {unityElement}
//                 {!renderUnity ? this.renderHomeScreen() : null }
//                 {renderUnity ? <Button label="Send Message!" style={styles.button} onPress={this.onSendMessageToUnity.bind(this)} /> : null }
//             </View>
//         );
//     }
// }

// const styles = 
// {
//     homeScreen: {
//         position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,
//         //justifyContent: 'center',
//         //alignItems: 'center',
//         //backgroundColor: '#F5FCFF',
//     },
//     UserDetails: {
//         //justifyContent: 'flex-start',
//     },
//     unityButonContainer: {
//         width: '100%',       
//         //height: '10%',
//         justifyContent: 'center',
//         alignItems: 'flex-start',
//         // borderWidth: 1,
//         // borderRadius: 2,
//         // bordercolor: '#ddd',
//         backgroundColor: '#F5FCFF',
//     },


// }

// export default HomeScreen;