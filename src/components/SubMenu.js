import React from 'react';
import { View, Text, Image } from "react-native";

const SubMenu = (props) =>
{
    return (
        <View style = {styles.containerStyle}>
            {props.children}
        </View>
    );
};

const styles = 
{
    containerStyle: {
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#f3f8f9', e6f7fb
        backgroundColor: '#e6f7fb',
        // backgroundOpacity: 0.1,
        // shadowColor: '#000',
        // shadowOffset: { width: 0, height: 2 },
        // shadowOpacity: 0.1,
        // shadowRadius: 2,
        // elevation: 1,
        // marginLeft: 5,
        // marginRight: 5,
        // marginTop: 10,
        //height: '100%',
        position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,
        width: '100%', 
    }
};

export default SubMenu;