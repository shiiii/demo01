import React from 'react';
// import { Platform, StyleSheet, Text, View, StatusBar, Alert } from 'react-native';
import UnityView, {UnityModule} from 'react-native-unity-view';

const LoadUnityView = ({ onPressed }) =>
{
    return(
         <UnityView 
            style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} 
            onUnityMessage = {onPressed} 
        />
    );
};

export default LoadUnityView;