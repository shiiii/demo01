import React from 'react';
import {View } from 'react-native';

const MenuItem = (props) =>
{
    return (
        <View style = {styles.containerStyle}>
            {props.children}
        </View>
    );
};

const styles = 
{
    containerStyle: {
        paddingTop: 10, 
        backgroundColor: '#00000000',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    }
}

export default MenuItem;