import React from 'react';
import { View, Text, TouchableOpacity, Image, ImageBackground} from 'react-native';

const UIButton = ({onPress, buttonIcon, buttonText }) =>
{
    const {buttonStyle, textStyle, ImageStyle} = styles;



    return (
        <View>
            <TouchableOpacity  style = {buttonStyle}  onPress = {() => onPress && onPress()} >
                {/* <Image resizeMode = 'cover' style = {ImageStyle} source = {buttonIcon}></Image> */}
                <Text style = {textStyle}>{buttonText}</Text>
            </TouchableOpacity>
        </View>
    );
};

// Button.defaultProps = {
//     enabled: true
// };

const styles = {
    buttonStyle: {
        //flex: 1,
        //alignSelf: '7bb9f2',
        backgroundColor: '#7bb9f2',
        backgroundColorOpacity: 0.1,
        // borderwidth: 10,
        borderRadius: 10,
        //borderColor: '#fff',
        minWidth: 100,
        height: 80,
        justifyContent: 'center',
        alignItems: 'center',
        shadowColor: '#000',
        shadowOffset: { width: 5, height: 5 },
        shadowOpacity: 0.7,
        shadowRadius: 9,
        elevation: 1,
        //borderOpacity: 0.8,
        // marginleft: 5,
        // marginRight: 5,
        // marginTop: 5,
        // marginBottom: 5,
        //pandding: 5,
        //width: 100,
       
        //borderRadius: 4,
        //backgroundColor: '#448cfe',

    },
    textStyle: {
        color: '#fff',
        //fontSize: 10,
        fontWeight: 'bold',
        alignSelf: 'center',
    },
    ImageStyle: {
        width: 80,
        height: 80,
        alignSelf: 'center',

    }

}

export default UIButton;