
import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { createBottomTabNavigator} from 'react-navigation-tabs';
import { createAppContainer } from 'react-navigation';
import { Icon } from 'react-native-elements';
import MenuItems from '../Screens/MenuItems';
import AccountDetails from '../Screens/AccountDetails';
import Notifications from '../Screens/Notifications';


const AccountsTabNavigator = createBottomTabNavigator(
  {
    Home : {
      screen : MenuItems,
      navigationOptions: {
        title: 'Home',
        tabBarIcon: ({ tintColor }) => (
          <Icon name='home' type='Entypo' style={{ color: tintColor }} />
        )
      },
    },
    AccountDetails: { 
        screen: AccountDetails,
        navigationOptions: {
          title: 'AccountDetails',
          tabBarIcon: ({ tintColor }) => (
            <Icon name='payment' type='Entypo' style={{ color: tintColor }} />
          )
        },
    },

    Notification: { 
        screen: Notifications,
        navigationOptions: {
          title: 'Notifications',
          tabBarIcon: ({ tintColor }) => (
            <Icon name='notifications' type='MaterialIcons' style={{ color: tintColor }} />
          )
        }
    },
  },
  // {
    //order: ['Home', 'AccountDetails', 'Notification'],
    // initialRouteName: 'AccountDetails',
    // tabBarOptions: {
    //   activeTintColor: '#D4AF37',
    //   inactiveTintColor: 'gray',
    //   style: {
    //     backgroundColor: 'white',
    //   }
    // },
  // },
);

  const AccountsTabs = createAppContainer(AccountsTabNavigator);

  // export default AccountsTabs;

  export default class Accounts extends Component {
      render() {
        return (
          <View >
            <Text>
              Accounts!!
            </Text>
          </View>
            
        );
      }
  }

 style = StyleSheet.create({
  AccountsContainer: {
    //flex: 1,
    justifyContent: 'center',
    alignItems: 'center' ,
    position: 'absolute', 
    left: 0,
    right: 0,
    top: 0,
    bottom: 0 
  }
});

  //export default createAppContainer(TabNavigator);