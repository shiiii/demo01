import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet, Text, View, Image, Alert } from 'react-native';
import UnityView, { UnityModule } from 'react-native-unity-view';
//import {createStackNavigator, HeaderBackButton} from 'react-navigation-stack';
// import UIButton from '../components/UIButton';
import Promotion from './Promotion';
//import HomeScreen from '../components/HomeScreen';

const buttonImage = require('../uiAssets/game_pad1.png');

class UnityDisplay extends Component {

    constructor(props) {
        super(props);
        this.state = {
            albums: [],
            clickCount: 0,
            renderUnity: false,
            unityLoaded: false,
            buttonImage: require('../uiAssets/game_pad1.png'),
        };
        this.state = { count: 0};
        //Settin up an interval for the counter
        this.t = setInterval(() => {
        this.setState({ count: this.state.count + 1 });
        }, 1000);
    }

    static navigationOptions = ({navigation}) => {
        //const {params = {}} = navigation.state;
        //title: 'Game',
        // /headerTitle: 'Promition',
        return {
            headerLeft: //null,
                <TouchableOpacity onPress = {navigation.getParam('home', () => console.log('callback: error loading main menu screen!!')) }/**/>
                    {<Image style = {{height: 48, width: 48,}}/**/ source = {require('../uiAssets/menu.png')}></Image>}
                </TouchableOpacity>,
            headerRight:  //null,
            <TouchableOpacity /*onPress = {navigation.navigate('Home')}*/>
                <Image style = {{height: 32, width: 32}}/**/ source = {require('../uiAssets/close-icon.png')}></Image>
            </TouchableOpacity>,
            headerStyle:{ backgroundColor: '#00b1f4'},
            //Sets Header text of Status Bar
            // headerStyle: {
            //   backgroundColor: '#f4511e',
            //   //Sets Header color
            // },
            headerTintColor: '#fff',
            //Sets Header text color
            headerTitleStyle: {
            fontWeight: 'bold',
            //Sets Header text style
            },
        }
    };
    componentDidMount() {
        console.log('!!!!!!!!!!!!!!!!!!!componentDidMount()');

        this.props.navigation.setParams({ home : this.onReturnToMainMenu})
       
        //Here is the Trick
        const { navigation } = this.props;
        // //Adding an event listner om focus
        //So whenever the screen will have focus it will set the state to zero
        this.focusListener = navigation.addListener('didFocus', () =>  
        {
            this.setState({ count: 0 })
        });
    };

    //     console.log('Component did mount. as listener');
    // }

    componentWillUnmount() {
        // Remove the event listener before removing the screen
        console.log('@@@@@@@@@@@@@@@@@@@@@@@@componentWillUnmount()');
        
        clearTimeout(this.t);
        this.focusListener.remove();
    }
    
    onToggleUnity() {
        // Set renderUnity boolean
        this.setState({ renderUnity: !this.state.renderUnity });

        // change button image
        if ( this.state.renderUnity)
        {
            // ingame image
            this.setState({buttonImage : require('../uiAssets/close-icon.png')});
        } else 
        {
            // promotion image
            this.setState({buttonImage : require('../uiAssets/game_pad1.png')});
        }
    }

    onReturnToMainMenu = () =>
    {
        console.log('Return to main menu');

        if (this.state.unityLoaded)
        {
            console.log('Pause Unity');
            UnityModule.pause();
        }

        this.props.navigation.navigate('Home');
    }

    onPauseAndResumeUnity() {
        if (this.state.unityPaused) {
            UnityModule.pause();
        }
        else {
            UnityModule.resume();
        }

        this.setState({ unityPaused: !this.state.unityPaused });
    }

    onSendMessageToUnity() {
        // UnityModule.pause();
        UnityModule.postMessageToUnityManager({
            // gameObject: 'GameController',
            // methodName: 'Pause',
            message: 'Sent from react native app',
            // callBack: (data) => {
            //     Alert.alert('Tip', JSON.stringify(data));
            // }
        });
    }

    onUnityMessage(event) {
        //this.setState({ clickCount: this.state.clickCount + 1 });
        console.log('Message received from unity!!! yay');

        setTimeout(() => {
            event.send('I am click callback!');
        }, 1000);
        //Alert.alert('Receive Message', 'Message receive from unity view');
        // UnityModule.postMessageToUnityManager({
        //     name: 'Receive Message',
        //     data: 'Message send fron react native',
        // });
    }

    // componentDidMount() 
    // {
    //     fetch('https://rallycoding.herokuapp.com/api/music_albums')
    //         .then(response => response.json())
    //         .then((responseData) => this.setState({ albums: responseData }));
    // }

    onRenderPromotions() {
        return (
            <View style = {styles.UnityViewContainer}>
                <Promotion/>
            </View>
        )
    }
    
    onUnityLoaded()
    {
        { this.state.unityLoaded ? 
            this.setState({unityLoaded: true})
            :
            this. onPauseAndResumeUnity()
        }
    }

    onRenderUnity()
    {
        
        if(this.state.renderUnity) {
            if (!this.state.unityLoaded)
            {
                this.setState({unityLoaded : true})
            } else 
            {
                UnityModule.resume();
            }
            
            return <UnityView style={{ position: 'absolute', left: 0, right: 0, top: 0, bottom: 0 }} onUnityMessage={this.onUnityMessage.bind(this)} />

        } else {

            if (this.state.unityLoaded)
            {
                console.log("pause unity and load home screen")

                UnityModule.pause();

                // return (
                //     this.props.navigation.navigate('Home')
                // );
                // TODO : find ways to go back in main menu
            } 
            else 
            {
                console.log("load promotion screen")
                return (
                    <View style= {{
                        width: '100%',
                        height: '100%', 
                        backgroundColor: '#e6f7fb', 
                        // borderWidth: 5,
                        // borderColor: '#ffff',
                        }}
                    >
                        <TouchableOpacity onPress = {this.onToggleUnity.bind(this)}>
                            <Image  source = {buttonImage}></Image>
                        </TouchableOpacity>
                    </View>
                )
            }

            // rendaer this on first
            
            
        }
    }

    render() {
        return (
            <View style={styles.gameHeader}>
                {
                    this.onRenderUnity()
                }
            </View> 
        );
    }
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         albums: [] ,
    //         clickCount: 0,
    //         renderUnity: false,
    //         unityPaused: false
    //     };
    // }

    // onToggleUnity() {
    //     this.setState({ renderUnity: !this.state.renderUnity });
    // }

    // onSendMessageToUnity()
    // {
    //     UnityModule.destroy();
    //     // UnityModule.postMessageToUnityManager({
    //     //     name: 'TestMessage',
    //     //     data: '',
    //     //     callBack: (data) => {
    //     //         Alert.alert('Tip', JSON.stringify(data));
    //     //     }
    //     // });
    // }

    // onUnityMessage(event) {
    //     //this.setState({ clickCount: this.state.clickCount + 1 });
    //     console.log('Message received from unity!!! yay')
    //     //Alert.alert('Receive Message', 'Message receive from unity view');
    //     // UnityModule.postMessageToUnityManager({
    //     //     name: 'Receive Message',
    //     //     data: 'Message send fron react native',
    //     // });
    // }

    // componentDidMount()
    // {
    //     fetch('https://rallycoding.herokuapp.com/api/music_albums')
    //    .then (response => response.json())
    //    .then((responseData) => this.setState( { albums: responseData }));
    // }

    // renderAlbums ()
    // {
    //     return this.state.albums.map(album => 
    //         <AlbumDetail key = {album.title} album = {album}></AlbumDetail>
    //     );
    // }

    // render()
    // {
    //     const { renderUnity, unityPaused, clickCount } = this.state;
    //     let unityElement;

    //     if (renderUnity) {
    //         unityElement = <LoadUnityView onPressed = {this.onUnityMessage.bind(this)} />;  
    //     }

    //     return (
    //         <View  style = {styles.UnityViewContainer}>
    //             {/* {this.renderAlbums()} */}
    //             {/* <ScreenView> */}
    //                 {unityElement}
    //                 <Text style={{ color: 'black', fontSize: 15 }}>Count: <Text style={{ color: 'red' }}>{clickCount}</Text> </Text>
    //                 <Button label="Toggle Unity" style={styles.button} onPress={this.onToggleUnity.bind(this)} />
    //                 {renderUnity ? <Button label="Send Message!" style={styles.button} onPress={this.onSendMessageToUnity.bind(this)} /> : null }
    //             {/* </ScreenView>  */}
    //             {/* 
    //                 <Button buttonText = 'Load unity' onPress = {this.onToggleUnity.bind(this)}></Button>
    //             */}
    //         </View>
    //     )
    // }
}

const styles = {
    gameContainer: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        //top: 0, bottom: 0, left: 0, right: 0,
        //justifyContent: 'center',
        //alignItems: 'center',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    gameHeader: {
        // width: '100%',
        height: '100%',
        //justifyContent: 'center',
        //alignItems: 'center',
        backgroundColor: '#dddddd',
        // borderWidth: 5,
        // //borderRadius: 2,
        // borderColor: '#ffff',
    },
    UnityViewContainer: {
        width: '100%',
        height: '90%',
        backgroundColor: '#dddd',
        paddingTop: '10%',
        // position: 'absolute', top: 0, bottom: 0, left: 0, right: 0,
        // justifyContent: 'center',
        // alignItems: 'center',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    playButton: {
        width: 50,
        height: 30,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffff',
        borderRadius: 5,
        marginRight: 30,
    },
    playImage: {
        width: 48,
        height: 48,
    },

}

export default UnityDisplay;

