import React from 'react';
import Carousel from 'react-native-banner-carousel';
import { StyleSheet, Image, View, Dimensions } from 'react-native';
 
const BannerWidth = Dimensions.get('window').width;
const BannerHeight = 300;
 
const images = [
    "https://www.pbebank.com/CMSPages/GetFile.aspx?guid=956fbeb5-69da-42f0-9375-3acce3b5e0c3",
    "https://www.pbebank.com/CMSPages/GetFile.aspx?guid=5bb8bd3f-e6f0-4dc8-9905-af28d34b57d8",
    "https://www.pbebank.com/CMSPages/GetFile.aspx?guid=ec65b208-d647-45c8-b245-44617b035d40",
    "https://www.pbebank.com/CMSPages/GetFile.aspx?guid=8af9faea-4c08-4518-8a98-b33db07dc1e6",
];
 
export default class UIBanner extends React.Component {
    renderPage(image, index) {
        return (
            <View style = {styles.imageBannerStyle} key={index}>
                <Image style ={{ alignSelf: 'center', width: '100%', height: '100%', borderRadius: 5,/**/ }} source={{ uri: image }} />
            </View>
        );
    }
 
    render() {
        return (
            <View style={styles.container}>
                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                >
                    {images.map((image, index) => this.renderPage(image, index))}
                </Carousel>
            </View>
        );
    }
}
 
const styles = StyleSheet.create({
    container: {
        //flex: 1,
        width: '100%',
        height:'100%',
        backgroundColor: '#e6f7fb',
        alignItems: 'center',
        justifyContent: 'center',
        // /borderRadius: 10, 
        // /backgroundColor: '#e6f7fb',
        //paddingTop: 10,
    },
    imageBannerStyle : {
        //justifyContent: 'center',
        //alignItems: 'center',
        //height: '100%',
        paddingLeft: 15,
        paddingRight: 15,
    },
});