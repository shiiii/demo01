import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity, Linking } from "react-native";
import {withNavigation} from 'react-navigation';

import { Icon } from 'react-native-elements';
import SubMenu from '../components/SubMenu';
import UIBanner from './UIBanner';
import UIHeader from '../components/UIHeader';

// class MenuItems extends Component 
class MenuItems extends Component{

    constructor(props) {
        super(props);
        this.state = { count: 0};
        //Settin up an interval for the counter
        this.t = setInterval(() => {
          this.setState({ count: this.state.count + 1 });
        }, 1000);
       
    }

    static navigationOptions = ({navigation}) => {
       return {
           title: 'Home',
           headerTitle: '',
           headerLeft: 
               <TouchableOpacity onPress={_ => navigation.navigate("Home")}>
                   <Image style = {{
                       marginLeft : 15,
                       width : 48,
                       height : 48,
                       }} 
                       source = {require('../uiAssets/menu.png')}>
                   </Image>
               </TouchableOpacity>,
           headerRight:  
               <TouchableOpacity /*style = {styles.playButton} onPress = {this.onToggleUnity.bind(this)}*/>
                   <Image style = {{
                       marginRight : 15,
                       width : 32,
                       height : 32,
                       }} 
                       source = {require('../uiAssets/logout.png')}></Image>
               </TouchableOpacity>,
           headerStyle:{ backgroundColor: '#00b1f4'},
           headerTintColor: '#fff',
               flex:1,
               headerTitleStyle: {
               textAlign:"center",
               alignSelf: 'center',
           },
       }

    }

    onAddListenerCallback()
    {
        console.log('MenuItems onAddListenerCallback : On did Focus!!');
        this.setState({ count: 0 });
    }

    componentDidMount() {
        console.log('MenuItems : !!!!!!!!!!!!!!!!!!!componentDidMount()');

        //this.props.navigation.setParams({ home : this.onReturnToMainMenu})
       
        //Here is the Trick
        const { navigation } = this.props;
        // //Adding an event listner om focus
        //So whenever the screen will have focus it will set the state to zero
        this.focusListener = navigation.addListener('didFocus', () =>  
        {
            this.setState({ count: 0 })
        });
    };

    componentWillUnmount() {
        // Remove the event listener before removing the screen
        console.log('MenuItems : @@@@@@@@@@@@@@@@@@@@@@@@componentWillUnmount()');
        
        clearTimeout(this.t);
        this.focusListener.remove();
    }

    render() {
        const {navigate} = this.props.navigation;
        return (
            <SubMenu>
                {/* <View style = {styles.header}>
                    <View style = {menuIconContainer}>
                        <TouchableOpacity >
                            <Image source = {require('../uiAssets/menu.png')} style = {menuIcon} ></Image>
                        </TouchableOpacity>
                    </View>
                    <View style = {menuUserContainer} >
                        <Text style = {userName} >App Demo</Text>
                    </View>
                    <View style = {menuPbbIconContainer}>
                        <TouchableOpacity >
                            <Image style = {pbbIcon} source = {require('../uiAssets/logout.png')}></Image>
                        </TouchableOpacity>
                    </View>   
                </View> */}
                <View style = {styles.bannerContainer}>
                    <UIBanner/>
                    <TouchableOpacity   style = {{paddingTop: 20,}/*buttonStyle*/} onPress = {()  => Linking.openURL('https://www.pbebank.com/Personal-Banking.aspx')}>
                        <Text style = {styles.buttonTextStyle}>Learn more!</Text> 
                    </TouchableOpacity>
                </View>
                <View style = {styles.buttonContainer}>
                    <View style = {styles.buttonList}>
                        <TouchableOpacity   style = {styles.buttonStyle} onPress = {() => navigate('Accounts')}>
                            <Image resizeMode = 'cover' style = {styles.ImageStyle}  source = {require('../uiAssets/card.png')}></Image>
                            <Text style = {styles.buttonTextStyle}>Accounts</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('')}>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/JomPay.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>JomPay</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('Promo')}>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/promo.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>Promo's</Text>
                        </TouchableOpacity>
                    </View>
                    <View style = {styles.buttonList}>
                        <TouchableOpacity   style = {styles.buttonStyle} onPress = {() => navigate('')}>
                            <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {{uri: 'https://av.sc.com/my/content/images/my-duitnow-campaign-2019-benefits-4.png'}}></Image>
                            <Text style = {styles.buttonTextStyle}>DuitNow</Text>
                        </TouchableOpacity>
                       
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('')}>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/payment.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>Payments</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('')}>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/topup.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>Top Up</Text>
                        </TouchableOpacity>
                       
                    </View>
                    <View style = {styles.buttonList}>
                        <TouchableOpacity   style = {styles.buttonStyle} onPress = {() => navigate('')}>
                            <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/transfer.png')}></Image>
                            <Text style = {styles.buttonTextStyle}>Transfer</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('')}>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/qr-code.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>QR CODE</Text>
                        </TouchableOpacity>
                        <TouchableOpacity  style = {styles.buttonStyle} onPress = {() => navigate('Game') }>
                                <Image resizeMode = 'cover' style = {styles.ImageStyle} source = {require('../uiAssets/game_pad1.png')}></Image>
                                <Text style = {styles.buttonTextStyle}>Play Time!</Text>
                        </TouchableOpacity>
                    </View>
                </View>

            
            </SubMenu>
        );
              

      }
}

const styles = 
{
    header: {
        flexDirection: 'row',
        height: '10%',
        width: '100%',
        paddingTop: 5, 
        backgroundColor: '#00b1f4',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    menuIconContainer: {
        justifyContent: 'center', 
        alignItems: 'flex-start',
        margin: 10,
        paddingLeft: 10, 
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
       
    },
    menuIcon: {
        alignSelf: 'stretch',
        width: 32,
        height: 32,
    },
    menuUserContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        marginRight: 10,
        width: '65%',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    userName: {
        fontSize: 20,
        fontFamily: 'sans-serif-condensed',
    },
    userDetails: {
        color: '#0f6fa7',
        fontWeight: 'bold',
    },
    menuPbbIconContainer: {
        justifyContent: 'center', 
        alignItems: 'flex-end',
        margin: 10,
        paddingTop: 5, 
        paddinRight: 50,
        width: 30,
        height: 30,
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    pbbIcon: {
        alignSelf: 'stretch',
        width: 32,
        height: 32,
        //paddinLeft: 50,
    },
    buttonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        height: '42%',
        width: '100%',       
        marginTop: 20,
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
     },
     buttonList: {
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '100%',
        width: '20%',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    ImageStyle: {
        width: 45,
        height: 45,
    },
    buttonStyle: {
        backgroundColorOpacity: 0.1,
        width: '100%',
        height: 60,
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },
    buttonTextStyle: {
        //paddingTop: 10,
        fontFamily: 'sans-serif-condensed',
    },
    bannerContainer: {
        paddingTop: 30,
        paddingBottom: 10,
        height: '50%',
        width: '100%',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        // borderWidth: 1,
        // borderRadius: 2,
        // bordercolor: '#ddd',
    },

};

export default MenuItems;