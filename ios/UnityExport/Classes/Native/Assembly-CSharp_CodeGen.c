﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 <CallbackTest>j__TPar <>f__AnonymousType0`1::get_CallbackTest()
// 0x00000002 System.Void <>f__AnonymousType0`1::.ctor(<CallbackTest>j__TPar)
// 0x00000003 System.Boolean <>f__AnonymousType0`1::Equals(System.Object)
// 0x00000004 System.Int32 <>f__AnonymousType0`1::GetHashCode()
// 0x00000005 System.String <>f__AnonymousType0`1::ToString()
// 0x00000006 <id>j__TPar <>f__AnonymousType1`4::get_id()
// 0x00000007 <seq>j__TPar <>f__AnonymousType1`4::get_seq()
// 0x00000008 <name>j__TPar <>f__AnonymousType1`4::get_name()
// 0x00000009 <data>j__TPar <>f__AnonymousType1`4::get_data()
// 0x0000000A System.Void <>f__AnonymousType1`4::.ctor(<id>j__TPar,<seq>j__TPar,<name>j__TPar,<data>j__TPar)
// 0x0000000B System.Boolean <>f__AnonymousType1`4::Equals(System.Object)
// 0x0000000C System.Int32 <>f__AnonymousType1`4::GetHashCode()
// 0x0000000D System.String <>f__AnonymousType1`4::ToString()
// 0x0000000E System.Void Bomb::Awake()
extern void Bomb_Awake_m1871A2508F99AE9459F2D221FC24DA1FAB62A8CD ();
// 0x0000000F System.Void Bomb::OnEnable()
extern void Bomb_OnEnable_mFDDF049AEE907CF72659E5C2B0F2CE6CAF963626 ();
// 0x00000010 System.Void Bomb::OnDisable()
extern void Bomb_OnDisable_mF16F07FC94D14254F3A7CFA1D07D838C85245F9D ();
// 0x00000011 System.Void Bomb::Destroy()
extern void Bomb_Destroy_m0B9F24232D52CB0B0A54E6E4779E7B8AE5961731 ();
// 0x00000012 System.Void Bomb::Explode()
extern void Bomb_Explode_mC997F2D084663D2CEBBF89C58EF1341330B0272A ();
// 0x00000013 System.Void Bomb::PlayVfx()
extern void Bomb_PlayVfx_m37C81E56D30A7193C404CF82B1B4CA229FA2F7B9 ();
// 0x00000014 System.Void Bomb::.ctor()
extern void Bomb__ctor_m168276948FC869A7BA91027858FABF173B76EFD7 ();
// 0x00000015 System.Void Bomb::<Explode>b__8_0()
extern void Bomb_U3CExplodeU3Eb__8_0_mB9059AFD8CE8BA54C5B61F8047BF329FF2433A72 ();
// 0x00000016 System.Void BombSpawner::OnEnable()
extern void BombSpawner_OnEnable_m58F76EEB5EAB4DFA95326CBD7710159AD494E1AB ();
// 0x00000017 System.Void BombSpawner::OnDisable()
extern void BombSpawner_OnDisable_mA9AFEF6A2D531D0C49F4588D9836B66120855776 ();
// 0x00000018 System.Void BombSpawner::Destroy()
extern void BombSpawner_Destroy_m7095A5DC7036ECFF10D3B132D698AB3FF2EE1D31 ();
// 0x00000019 System.Void BombSpawner::Start()
extern void BombSpawner_Start_mEEF4FD796E6F125D8FA6BD431C31D046CDBBF2A8 ();
// 0x0000001A System.Void BombSpawner::OnLevelUp()
extern void BombSpawner_OnLevelUp_mD77BE132A1513F6D31470389BA0CC01B8F4E1188 ();
// 0x0000001B System.Void BombSpawner::RestBomb()
extern void BombSpawner_RestBomb_m297B7C66792EB033E3E951D47C79B9B60FA59585 ();
// 0x0000001C System.Void BombSpawner::ShowBomb()
extern void BombSpawner_ShowBomb_mEE720FB9CCDCE4A03C598141F51A09F0DAF36899 ();
// 0x0000001D System.Void BombSpawner::.ctor()
extern void BombSpawner__ctor_mE300FC2E154A29A155E45AEB87AEF64E847F3747 ();
// 0x0000001E System.Void CollisionEventSender::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void CollisionEventSender_OnCollisionEnter2D_m50C6C8F61004CD2982CB3050A86B1F879AB9F9DF ();
// 0x0000001F System.Void CollisionEventSender::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void CollisionEventSender_OnTriggerEnter2D_mF5BDD747F0EFF340174C87D7535372FB5834393D ();
// 0x00000020 System.Void CollisionEventSender::.ctor()
extern void CollisionEventSender__ctor_m091C8963FAFDF08FE8863D2DDEA6F31354172807 ();
// 0x00000021 System.Void CountDown::OnEnable()
extern void CountDown_OnEnable_m31671BAB6EBFD5655F551DD3442390EC6F71C50D ();
// 0x00000022 System.Void CountDown::Awake()
extern void CountDown_Awake_m71BF2CCC7110F3FB236A15B42160B290801CBCEF ();
// 0x00000023 System.Void CountDown::OnDisable()
extern void CountDown_OnDisable_mC08463E83AA77DD1B8306A2087D779C39238EE6B ();
// 0x00000024 System.Void CountDown::Destroy()
extern void CountDown_Destroy_m4FB44DBF6A73BB67E9DEC52807C78A0BBD50C050 ();
// 0x00000025 System.Void CountDown::Update()
extern void CountDown_Update_m6926FE9AF0240BD9DA967D4196B5226CD9310D6B ();
// 0x00000026 System.Void CountDown::OnStartCountDown()
extern void CountDown_OnStartCountDown_m835FE8CD623FDC0721DA5613200ECF6E24199CD0 ();
// 0x00000027 System.Void CountDown::onCountDown()
extern void CountDown_onCountDown_m3E373D82E09193C859D3BD29627006CBBB626AC4 ();
// 0x00000028 System.Void CountDown::.ctor()
extern void CountDown__ctor_mAC752453639C81ADEA1772600F23B37BE6CB92F8 ();
// 0x00000029 System.Void CountDown::<Update>b__7_0()
extern void CountDown_U3CUpdateU3Eb__7_0_m5595728916741855A9745D8FB950499DD52494B1 ();
// 0x0000002A System.Void CountDown::<OnStartCountDown>b__8_0()
extern void CountDown_U3COnStartCountDownU3Eb__8_0_m6A24F2E2A100657F88470FE3A4E1BE09712B4BBC ();
// 0x0000002B System.Void EventSender::onSendEvent(System.String)
extern void EventSender_onSendEvent_m19AC0E9199ED02F0127A325F282D1D9DF8843955 ();
// 0x0000002C System.Void EventSender::.ctor()
extern void EventSender__ctor_m42541AEBF045C22F8CCACC48788C2C92E035C8FB ();
// 0x0000002D GameController GameController::get_Instance()
extern void GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260 ();
// 0x0000002E System.Void GameController::Awake()
extern void GameController_Awake_m0F7A2663599EE488BA03E9010D12DF3D9EF623BE ();
// 0x0000002F System.Void GameController::OnEnable()
extern void GameController_OnEnable_mBEE9B7EF36A93A7A5F894132B82125FF79DF25B3 ();
// 0x00000030 System.Void GameController::OnDisable()
extern void GameController_OnDisable_m82B20B33A46E9AA09B73EB4207D5262755B809D6 ();
// 0x00000031 System.Void GameController::Destroy()
extern void GameController_Destroy_m0D53E77855F55548A52B7CD8D90BDBAA30EBE6D3 ();
// 0x00000032 System.Void GameController::InitializeGameUI()
extern void GameController_InitializeGameUI_m2B4A1DEF8AD71C97E996578E3E190B5871D901E5 ();
// 0x00000033 System.Void GameController::InitializeLevelData()
extern void GameController_InitializeLevelData_m9F21D6859D3EB3055B33350B89008E0C7A2AD72F ();
// 0x00000034 System.Void GameController::PopulateLevelData()
extern void GameController_PopulateLevelData_mA521856D1E39DC0AB18F000E034B45530D644852 ();
// 0x00000035 System.Void GameController::ResetGame()
extern void GameController_ResetGame_mEF25CCAA937DBCDEF25CE02BA08AA7ADA5E47C11 ();
// 0x00000036 System.Void GameController::ResetLevel()
extern void GameController_ResetLevel_m5A74E8E43186E5877F8BACED00826E8F921D3068 ();
// 0x00000037 System.Void GameController::OnClickedPlay()
extern void GameController_OnClickedPlay_mD010769ECE4714958A7E4BC3A1B4FB483ADB512A ();
// 0x00000038 System.Void GameController::Playing()
extern void GameController_Playing_m3398723576EE27851C4E79F9B4434F18DE0B6738 ();
// 0x00000039 System.Void GameController::LevelUp()
extern void GameController_LevelUp_m11B9DF1271A342AA3A427135CE4EF37AC8EA61D7 ();
// 0x0000003A System.Void GameController::OnCollisionWithSameObject()
extern void GameController_OnCollisionWithSameObject_m77FC37EFF7F54F8D2FFDCFBF231558F4D3325CD9 ();
// 0x0000003B System.Void GameController::GameOver()
extern void GameController_GameOver_m902C548A5DD5F90EA64A70B00806086D6CEB37F4 ();
// 0x0000003C System.Void GameController::HasGiftbox()
extern void GameController_HasGiftbox_m48FD038BB7FB93BCB24EC6C1C2BA3FDB01CA3224 ();
// 0x0000003D System.Void GameController::OnGiftboxOpened()
extern void GameController_OnGiftboxOpened_m20A26BC5F36F698ADCAC426C15F460DCE7AC9431 ();
// 0x0000003E System.Void GameController::PlayAgain()
extern void GameController_PlayAgain_m6DA61EBDF5EFCD8532E143B10B8C753CD5A29196 ();
// 0x0000003F System.Void GameController::Pause()
extern void GameController_Pause_mB4B24208C38F0D4F7D61553CA988AA6411A9ED08 ();
// 0x00000040 System.Void GameController::.ctor()
extern void GameController__ctor_m839A9CFB9635B009C1DA139BEAD0E38467E57464 ();
// 0x00000041 System.Void GameFlow::Start()
extern void GameFlow_Start_mA899CB26EFEAEC2D5F8C7EB63FE38F8BC8573ABC ();
// 0x00000042 System.Void GameFlow::Update()
extern void GameFlow_Update_mA8C4D304821E21FC8804ED00B89DA8E13006B7F7 ();
// 0x00000043 System.Void GameFlow::.ctor()
extern void GameFlow__ctor_m64C71C093D36011B93F9D1A3C4F509DF3A398826 ();
// 0x00000044 System.Void GameOverAdapter::OnEnable()
extern void GameOverAdapter_OnEnable_m61B20D2EB7D1769A044DECCEDF4200ABBAF8A142 ();
// 0x00000045 System.Void GameOverAdapter::OnDisable()
extern void GameOverAdapter_OnDisable_m6604153AEED3ECD75501DEEFDED6D4D7D40306B5 ();
// 0x00000046 System.Void GameOverAdapter::Destroy()
extern void GameOverAdapter_Destroy_m12CF85C551CF14E51DEC8FFA79391BD13BAE2BCD ();
// 0x00000047 System.Void GameOverAdapter::OnGameOverShow()
extern void GameOverAdapter_OnGameOverShow_mF25B7A0948CE93CB946092E08D8F95B870FB912F ();
// 0x00000048 System.Void GameOverAdapter::OnShowAnimFinished()
extern void GameOverAdapter_OnShowAnimFinished_mF03C61AC85B01BEB9B8F902BEFFD1F5C36A8CFE0 ();
// 0x00000049 System.Void GameOverAdapter::OnGameOverHide()
extern void GameOverAdapter_OnGameOverHide_mECF9C4267E0A7ED9B7D4FC99131B4AD9EE88B1E3 ();
// 0x0000004A System.Void GameOverAdapter::OnHideAnimFinished()
extern void GameOverAdapter_OnHideAnimFinished_mF20D487144070F92327305E8EE8AB609F0BBDF0C ();
// 0x0000004B System.Void GameOverAdapter::OnPlayAgain()
extern void GameOverAdapter_OnPlayAgain_mFEF339E32CA0C940466033FDA68F31F57FE2A899 ();
// 0x0000004C System.Void GameOverAdapter::.ctor()
extern void GameOverAdapter__ctor_m35D65EAD6A6C50339BD1877E73DC365949F8D92E ();
// 0x0000004D System.Void GameOverAdapter::<OnGameOverShow>b__7_0()
extern void GameOverAdapter_U3COnGameOverShowU3Eb__7_0_mDCEE3FB117EBB4590A0689D15FF0AE4DBD3E3766 ();
// 0x0000004E System.Void GameOverAdapter::<OnGameOverHide>b__9_0()
extern void GameOverAdapter_U3COnGameOverHideU3Eb__9_0_mA39944598DE9C6FBC460770C39E962507F217733 ();
// 0x0000004F System.Void Giftbox::Awake()
extern void Giftbox_Awake_mC9BD872C12F3782D026B92185EC80F8C25766444 ();
// 0x00000050 System.Void Giftbox::OnEnable()
extern void Giftbox_OnEnable_m45210D230F061A186B50FA251B44535794B29491 ();
// 0x00000051 System.Void Giftbox::OnDisable()
extern void Giftbox_OnDisable_m806B7A223695B468DA31A7DD02FDFB9A04949FF4 ();
// 0x00000052 System.Void Giftbox::Destroy()
extern void Giftbox_Destroy_mE7B945818C92C8EB57BBC2B3C70A1A1280CF77AD ();
// 0x00000053 System.Void Giftbox::Explode()
extern void Giftbox_Explode_m41346615A7EC8315FDDBD1E00DF2FCBCF2B9DE59 ();
// 0x00000054 System.Void Giftbox::PlayVfx()
extern void Giftbox_PlayVfx_m78DE09B993C4BDCE407E96C2C78BB7A5667355F8 ();
// 0x00000055 System.Void Giftbox::.ctor()
extern void Giftbox__ctor_m8208F54535430BF8889162BB812F60C3A63ED984 ();
// 0x00000056 System.Void Giftbox::<Explode>b__8_0()
extern void Giftbox_U3CExplodeU3Eb__8_0_m27C785A2CABF099C717CC9D92A3E2114EFB1EB99 ();
// 0x00000057 System.Void GiftboxAdapter::OnEnable()
extern void GiftboxAdapter_OnEnable_m1C32282370A08A3E8374F2DD9379B48FA75A294C ();
// 0x00000058 System.Void GiftboxAdapter::OnDestroy()
extern void GiftboxAdapter_OnDestroy_m137B3A7822C8AC9AF3F2498C590DA7B438661AFF ();
// 0x00000059 System.Void GiftboxAdapter::OnDisable()
extern void GiftboxAdapter_OnDisable_m73AF9C8D1236B0983B759FDC21EBDADEF86F37E2 ();
// 0x0000005A System.Void GiftboxAdapter::OnShowGiftbox()
extern void GiftboxAdapter_OnShowGiftbox_m057086E330A4EF83DA1C389C970C9C37FF32E375 ();
// 0x0000005B System.Void GiftboxAdapter::OnShowDone()
extern void GiftboxAdapter_OnShowDone_m2DD77B01BBD5B77B25F974BFC8478B4CF4130028 ();
// 0x0000005C System.Void GiftboxAdapter::PlayAnimation()
extern void GiftboxAdapter_PlayAnimation_mB51EF0A81B31AA59EE3371657854818A9BB74D09 ();
// 0x0000005D System.Void GiftboxAdapter::OnPlayAgain()
extern void GiftboxAdapter_OnPlayAgain_m41B5677E42AF1B12887DCC99D144C984E75D9FF9 ();
// 0x0000005E System.Void GiftboxAdapter::OnHideDone()
extern void GiftboxAdapter_OnHideDone_m1D36FECF732C2739F66E28DECC19E2C7A7214DE8 ();
// 0x0000005F System.Void GiftboxAdapter::.ctor()
extern void GiftboxAdapter__ctor_m9EBC7A946E98CD6DE49F869674FB77828091FBAA ();
// 0x00000060 System.Void GiftboxAdapter::<OnShowGiftbox>b__7_0()
extern void GiftboxAdapter_U3COnShowGiftboxU3Eb__7_0_m129453A9165FEB7BA16F85B6E76317FFE47CF913 ();
// 0x00000061 System.Void GiftboxAdapter::<OnPlayAgain>b__10_0()
extern void GiftboxAdapter_U3COnPlayAgainU3Eb__10_0_mD3F4BD4ECDEC5790272C66116B73F5F0BCE43427 ();
// 0x00000062 System.Void GiftboxAnimController::OnGiftboxOpen()
extern void GiftboxAnimController_OnGiftboxOpen_m73F98F6355B5EBC526A2B928F6615FC98C7889EC ();
// 0x00000063 System.Void GiftboxAnimController::.ctor()
extern void GiftboxAnimController__ctor_m62B2504289DC8C9182190BDC9AECB909E0211CE7 ();
// 0x00000064 System.Void GiftboxSpawner::OnEnable()
extern void GiftboxSpawner_OnEnable_m8ED48FEB1E5C6C8E5C6BD19E15CF6175DDF67D0D ();
// 0x00000065 System.Void GiftboxSpawner::OnDisable()
extern void GiftboxSpawner_OnDisable_m894A2924CEF3EB938276894711BC88D7E091E2A2 ();
// 0x00000066 System.Void GiftboxSpawner::Destroy()
extern void GiftboxSpawner_Destroy_m097521601779A280F011A23CB1EF978F6F1EED84 ();
// 0x00000067 System.Void GiftboxSpawner::Start()
extern void GiftboxSpawner_Start_mDDF44B2C47E80562D99346B0E47D12CEC8838FBD ();
// 0x00000068 System.Void GiftboxSpawner::OnLevelUp()
extern void GiftboxSpawner_OnLevelUp_m33B8194C6F5E66E021F0EA72FFB6CEAC8865CB7F ();
// 0x00000069 System.Void GiftboxSpawner::ResetGiftbox()
extern void GiftboxSpawner_ResetGiftbox_mF5DAD423BCEAF08D1D22A06C2A3EDC7F834C42FE ();
// 0x0000006A System.Void GiftboxSpawner::ShowGiftbox()
extern void GiftboxSpawner_ShowGiftbox_m4CCEBB85A9E876F37861BC45AABE73036AC4174E ();
// 0x0000006B System.Void GiftboxSpawner::.ctor()
extern void GiftboxSpawner__ctor_m62E1A6372FCADB968F1B1C659537A009215272C0 ();
// 0x0000006C System.Void HudController::Start()
extern void HudController_Start_m5FE57FFC9DA2F8C2BFA6D2E256002DC69CA2A5F2 ();
// 0x0000006D System.Collections.IEnumerator HudController::tempChecker()
extern void HudController_tempChecker_m6AEB042BB7CBDF53CF40316D99EBD8582C55A765 ();
// 0x0000006E System.Void HudController::.ctor()
extern void HudController__ctor_m6F98E6BFB55B45B88A369616792A4A28C1C69468 ();
// 0x0000006F System.Void KnifeCollisionAdapter::OnEnable()
extern void KnifeCollisionAdapter_OnEnable_mAD449D64AACA564373D1C7C4ECB8D5524651A81E ();
// 0x00000070 System.Void KnifeCollisionAdapter::OnDisable()
extern void KnifeCollisionAdapter_OnDisable_m1B68E0FE3711E99966726269CB1AA21CFB2E7887 ();
// 0x00000071 System.Void KnifeCollisionAdapter::Awake()
extern void KnifeCollisionAdapter_Awake_mA64C4040DA63F421865AE9A46DFDCBF1F2F90598 ();
// 0x00000072 System.Void KnifeCollisionAdapter::Update()
extern void KnifeCollisionAdapter_Update_mC5C1FA055367A1A7869396A1D197BC6F4F566042 ();
// 0x00000073 System.Void KnifeCollisionAdapter::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void KnifeCollisionAdapter_OnCollisionEnter2D_m395C6284DBA5B8904E36C10ED8DBB7D2004E47A3 ();
// 0x00000074 System.Void KnifeCollisionAdapter::ResetObjectData()
extern void KnifeCollisionAdapter_ResetObjectData_m1E35748C19942D1907E281324D0556A2B345465D ();
// 0x00000075 System.Void KnifeCollisionAdapter::.ctor()
extern void KnifeCollisionAdapter__ctor_m4D19C9284948D82A5E8EF17AE86AD50853CE2ACD ();
// 0x00000076 System.Void LevelAdapter::OnEnable()
extern void LevelAdapter_OnEnable_mCFC251B22019AAC984F4520B184C687D3D53D3C8 ();
// 0x00000077 System.Void LevelAdapter::Awake()
extern void LevelAdapter_Awake_m979C8882060DD183D14C284E82FA51C9E6E2B3A5 ();
// 0x00000078 System.Void LevelAdapter::OnDisable()
extern void LevelAdapter_OnDisable_m3918646A9E04689180DE65A35B5167452F1D4F2F ();
// 0x00000079 System.Void LevelAdapter::Destroy()
extern void LevelAdapter_Destroy_m06207A298080E7D8EA2D92F9BD0B7B0BA0C3A338 ();
// 0x0000007A System.Void LevelAdapter::UpdateLevel()
extern void LevelAdapter_UpdateLevel_mFAC90A0E015E2A716025D0B41C709E06ED0B954B ();
// 0x0000007B System.Void LevelAdapter::OnInitializeLevel()
extern void LevelAdapter_OnInitializeLevel_m9D96796E9D0448F676FA1645A584AFACC73F37E8 ();
// 0x0000007C System.Void LevelAdapter::OnLevelUp()
extern void LevelAdapter_OnLevelUp_m2859F1DED36666F4F1E27990987D492DAFD32E93 ();
// 0x0000007D System.Void LevelAdapter::.ctor()
extern void LevelAdapter__ctor_mE8DCE26C277D780F688A14A099F74CC3BC606385 ();
// 0x0000007E System.Void ObjectRotation::Awake()
extern void ObjectRotation_Awake_m10F6A8E16522E5C6771172FB63ECC10BF85D0001 ();
// 0x0000007F System.Void ObjectRotation::OnEnable()
extern void ObjectRotation_OnEnable_mEE3B8203BD701A3113264BEEC3F8CF6376BE6B6A ();
// 0x00000080 System.Void ObjectRotation::OnDisable()
extern void ObjectRotation_OnDisable_m9FEC2CC5A4479FB24DD1E5A35F1334D7FF7EC2C2 ();
// 0x00000081 System.Void ObjectRotation::Destroy()
extern void ObjectRotation_Destroy_m2CDC8A5EA540F0B6086D598A16ED65C839295D56 ();
// 0x00000082 System.Void ObjectRotation::InitializeRotation()
extern void ObjectRotation_InitializeRotation_mA33BB1810E53A8834E9E205578E7536BBAE02C68 ();
// 0x00000083 System.Collections.IEnumerator ObjectRotation::StartRotation()
extern void ObjectRotation_StartRotation_m846A5CE791C63F80E37609EDB4FCFCA2A81F3149 ();
// 0x00000084 System.Void ObjectRotation::StopRotation()
extern void ObjectRotation_StopRotation_m0CBCCFFB5D1D401CA0E0F49F40F49A5D852D6FEE ();
// 0x00000085 System.Void ObjectRotation::OnGameOver()
extern void ObjectRotation_OnGameOver_m69FAA1033081771819BFD921B72D251E5DFAB924 ();
// 0x00000086 System.Void ObjectRotation::.ctor()
extern void ObjectRotation__ctor_mCF31CD9B1DBDAE89C8591004917C3413348B9CE8 ();
// 0x00000087 System.Void ObjectSpawner::Awake()
extern void ObjectSpawner_Awake_m24368C54B5CFF7B69B312D4E005AF0307E5FEA53 ();
// 0x00000088 System.Void ObjectSpawner::OnEnable()
extern void ObjectSpawner_OnEnable_m906149C0704DF14D48F204C0A36CD1BB807CA105 ();
// 0x00000089 System.Void ObjectSpawner::Start()
extern void ObjectSpawner_Start_m9E9806FBFDF0D7443C886E6D3B429CFF2B70B2C2 ();
// 0x0000008A System.Void ObjectSpawner::SpawnGameObject()
extern void ObjectSpawner_SpawnGameObject_mA301986D0B43BB3407E92CC2623513E438A42C1E ();
// 0x0000008B System.Void ObjectSpawner::DespawnGameobject()
extern void ObjectSpawner_DespawnGameobject_mC093504EABC7F2130EB0E2B7B1F9DE7937A6981E ();
// 0x0000008C System.Void ObjectSpawner::DespawnGameobject(UnityEngine.GameObject)
extern void ObjectSpawner_DespawnGameobject_m9F9B3D79A941AAD539DC5E60FCF7A5C0728EBFE5 ();
// 0x0000008D System.Void ObjectSpawner::DespawnAll()
extern void ObjectSpawner_DespawnAll_m41D675E384010AC38EE710B282B790BDFFF1276C ();
// 0x0000008E System.Void ObjectSpawner::.ctor()
extern void ObjectSpawner__ctor_m37C806347D6F834F55235541B95E441FCC6A0C6E ();
// 0x0000008F System.Void OverlayAdapter::OnEnable()
extern void OverlayAdapter_OnEnable_mB6C36227A730F8879F1A0F5B31C07BE20D4FBA5E ();
// 0x00000090 System.Void OverlayAdapter::Awake()
extern void OverlayAdapter_Awake_mD801FE3812595FD5B26377A57FAD6870CDFF1CDB ();
// 0x00000091 System.Void OverlayAdapter::OnDisable()
extern void OverlayAdapter_OnDisable_m4BFC0C940506F21885CD296A132ADA440FFEBC9A ();
// 0x00000092 System.Void OverlayAdapter::Destroy()
extern void OverlayAdapter_Destroy_mFD9A80902BD322FCC1A6DE500D4C147B4C091B65 ();
// 0x00000093 System.Void OverlayAdapter::OnHideOverlay()
extern void OverlayAdapter_OnHideOverlay_m25878225CBC1F1C0A1710E9EC95A61AA47AF3A29 ();
// 0x00000094 System.Void OverlayAdapter::OnShowOverlay()
extern void OverlayAdapter_OnShowOverlay_mE68F13F1738CDC724FB44A28E14CAFD142250E3F ();
// 0x00000095 System.Void OverlayAdapter::.ctor()
extern void OverlayAdapter__ctor_mE75BF2A5B416F53EC0181692CC2FCE500E14A87F ();
// 0x00000096 System.Void PlayAdapter::OnEnable()
extern void PlayAdapter_OnEnable_m24D8499589071E5E289596482DE2995014728A6D ();
// 0x00000097 System.Void PlayAdapter::Awake()
extern void PlayAdapter_Awake_m2BCEF0C8C32ECFE2F491697E7F65AB17C359DF8B ();
// 0x00000098 System.Void PlayAdapter::OnDisable()
extern void PlayAdapter_OnDisable_mCB7369C6BF89438DBE1866F67135F2C39A0A19A3 ();
// 0x00000099 System.Void PlayAdapter::Destroy()
extern void PlayAdapter_Destroy_m2E6DB281B5C9B6C4A598B6CCF832659B0AC1CBAF ();
// 0x0000009A System.Void PlayAdapter::OnShowPlayButton()
extern void PlayAdapter_OnShowPlayButton_m78A9B16C74AD5382CFAE69E068326DEA40990E60 ();
// 0x0000009B System.Void PlayAdapter::OnHidePlay()
extern void PlayAdapter_OnHidePlay_m2497A9CED229B9D0C1D9989E83F5AB76D3E9D045 ();
// 0x0000009C System.Void PlayAdapter::OnPLay()
extern void PlayAdapter_OnPLay_mBE36A58F51071B415A2D96A4E74614CF739C92A6 ();
// 0x0000009D System.Void PlayAdapter::.ctor()
extern void PlayAdapter__ctor_mC294EE8C8466D0930C4A913A2E3926E1C3AA80C6 ();
// 0x0000009E System.Void PlayAdapter::<OnHidePlay>b__10_0()
extern void PlayAdapter_U3COnHidePlayU3Eb__10_0_mB10EFC20B252AB96507C39299B711B56540068C8 ();
// 0x0000009F System.Void PlayAgainAdapter::Awake()
extern void PlayAgainAdapter_Awake_mB54DFCF4EFB38C61336AF4B3CB4DE1FBE6FD6928 ();
// 0x000000A0 System.Void PlayAgainAdapter::OnEnable()
extern void PlayAgainAdapter_OnEnable_m5CC5F1BD08E176F27921D6DD066FF21DAA4ADC3E ();
// 0x000000A1 System.Void PlayAgainAdapter::OnDisable()
extern void PlayAgainAdapter_OnDisable_m9034EE25962CD28E6D75D4597760BC7DA03500BA ();
// 0x000000A2 System.Void PlayAgainAdapter::OnDestroy()
extern void PlayAgainAdapter_OnDestroy_m6CBBC3EC4E6DED1626195D72A90AC9AFE4473BE8 ();
// 0x000000A3 System.Void PlayAgainAdapter::OnShowPlayAgain()
extern void PlayAgainAdapter_OnShowPlayAgain_mF24CF10D4BE9347B49AD5490CEC63679F9A9D135 ();
// 0x000000A4 System.Void PlayAgainAdapter::OnShowDone()
extern void PlayAgainAdapter_OnShowDone_m2FEEFF66D83AED02E66F4D0EC88D179B11E32093 ();
// 0x000000A5 System.Void PlayAgainAdapter::OnHide()
extern void PlayAgainAdapter_OnHide_m088D138EA451338986EFBB5F67AF4BD6B830CF03 ();
// 0x000000A6 System.Void PlayAgainAdapter::onHodeDone()
extern void PlayAgainAdapter_onHodeDone_m14F18B577772092FDB3BC6928692CC3A7388D9E9 ();
// 0x000000A7 System.Void PlayAgainAdapter::PlayAgain()
extern void PlayAgainAdapter_PlayAgain_m0900DB1C365367AD0753B92C7E27A7F9D89DB068 ();
// 0x000000A8 System.Void PlayAgainAdapter::.ctor()
extern void PlayAgainAdapter__ctor_mBA49D442B2D5B458A9525DDF44DBD7B97F3F650F ();
// 0x000000A9 System.Void PlayAgainAdapter::<OnShowPlayAgain>b__7_0()
extern void PlayAgainAdapter_U3COnShowPlayAgainU3Eb__7_0_m76E9E1FE8BBDAD7EE7880D0AC53B289C115DB3EE ();
// 0x000000AA System.Void PlayAgainAdapter::<OnHide>b__9_0()
extern void PlayAgainAdapter_U3COnHideU3Eb__9_0_mE8DA0045B8DE87D1B7C8A6B0797C4963B6C55A01 ();
// 0x000000AB Score Score::get_Instance()
extern void Score_get_Instance_m439146EDC19D68D79ED5BF39A4FD794FAB3E1A01 ();
// 0x000000AC System.Void Score::set_Instance(Score)
extern void Score_set_Instance_m8866BA5DA4C1E9A562E94E40C5E3CCD1C0B2852E ();
// 0x000000AD System.Void Score::Awake()
extern void Score_Awake_m481CF8ED9C2A4383D06EEEEE40EA8A0D6D4E85AA ();
// 0x000000AE System.Void Score::OnEnable()
extern void Score_OnEnable_m2A91D9BA7577AA26EAB7A552EAC70519D27D3C73 ();
// 0x000000AF System.Void Score::OnDisable()
extern void Score_OnDisable_m103F900910779688AAFF297606EFFBA37ED8851C ();
// 0x000000B0 System.Void Score::Destroy()
extern void Score_Destroy_m69193D269D15C985896A6A56467A18276CC05C21 ();
// 0x000000B1 System.Int32 Score::get_ScoreValue()
extern void Score_get_ScoreValue_m822B6EFA0A9B250A38D90D517D71E81711958DB0 ();
// 0x000000B2 System.Void Score::Start()
extern void Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E ();
// 0x000000B3 System.Void Score::AddScore(System.String)
extern void Score_AddScore_mA192249C9863A59333D2046823539B564EECA29B ();
// 0x000000B4 System.Void Score::ResetScore()
extern void Score_ResetScore_mBB6FC67BBB10F4958FF57CCFA276E3B365D3C9B0 ();
// 0x000000B5 System.Void Score::OnShowScore()
extern void Score_OnShowScore_m776FE3AAA2F29564DC0BC54CA4D57AE57358DB28 ();
// 0x000000B6 System.Void Score::.ctor()
extern void Score__ctor_mE2F9D741565800994F2DFAF0C4036F5E7ADA4D1F ();
// 0x000000B7 System.Int32 Level::get_Score()
extern void Level_get_Score_mE465427199E91854BF31F6621777EF82065CF8D9 ();
// 0x000000B8 UnityEngine.Sprite Level::get_StickSprite()
extern void Level_get_StickSprite_mE373850BEBD58058FE3362C5979ADE0194E4EB65 ();
// 0x000000B9 System.Int32 Level::get_ObjectCount()
extern void Level_get_ObjectCount_mC9B7EC462ADFD82F060F01462F7BC3DC15011A15 ();
// 0x000000BA UnityEngine.Sprite Level::get_RotatingSprite()
extern void Level_get_RotatingSprite_m6D628B0174976A0EEC43F92B280AC7B43B037657 ();
// 0x000000BB System.Single Level::get_RotationSpeed()
extern void Level_get_RotationSpeed_m1059ED3728D41F342942E52E5FED998177D1EF09 ();
// 0x000000BC System.Single Level::get_RotationDuration()
extern void Level_get_RotationDuration_mE38F35F23C9742DFBDCE903300742F584087F7B6 ();
// 0x000000BD System.Boolean Level::get_ShowGiftBox()
extern void Level_get_ShowGiftBox_m8D6E103DC4C8036E53E3B6112CC7E7FC652063D5 ();
// 0x000000BE System.Boolean Level::get_ShowBomb()
extern void Level_get_ShowBomb_m276D85440A3028E2A7924B96B6F47A7B25FD641F ();
// 0x000000BF System.Int32 Level::get_NumberOfBomb()
extern void Level_get_NumberOfBomb_mCBB4CF3E07275AF86F1CDC1FBA4B3FAE29775F4B ();
// 0x000000C0 System.Int32 Level::get_NumberOfGiftbox()
extern void Level_get_NumberOfGiftbox_m079FA9943B3BFAD750A87A1D27454F4883530B2B ();
// 0x000000C1 System.Void Level::.ctor()
extern void Level__ctor_m7BAC6CA179FB0475A672991C18A04BC93712F4ED ();
// 0x000000C2 System.Void Levels::.ctor()
extern void Levels__ctor_m71996D6A3C681A73B1FCDC87D15AC87A2DDCB7AC ();
// 0x000000C3 UnityEngine.GameObject Spawner::Spawn()
extern void Spawner_Spawn_mFCA22A3A7C3F13BAEB54F0D4B2AE47767567623C ();
// 0x000000C4 System.Void Spawner::SpawnAll(UnityEngine.GameObject,System.Int32,UnityEngine.Transform)
extern void Spawner_SpawnAll_m3F2B25BEC54AC571796D9CA2A8BE1B3678490C6B ();
// 0x000000C5 System.Void Spawner::DespawnGameObject(UnityEngine.GameObject)
extern void Spawner_DespawnGameObject_mA7B74EA001CC03EFC12758578F3AD30DED48FCD2 ();
// 0x000000C6 System.Void Spawner::DespawnAll()
extern void Spawner_DespawnAll_mC3431725EE43767357684B0200EFC1EECA0DC619 ();
// 0x000000C7 System.Void Spawner::Despawn(UnityEngine.GameObject)
extern void Spawner_Despawn_m457F724CCCB86DC3D2B90424CEB185BE43D96CFA ();
// 0x000000C8 System.Void Spawner::Despawn()
extern void Spawner_Despawn_m505D7FBF55A8D47D5134ED17AA6F286F755759D9 ();
// 0x000000C9 System.Void Spawner::.ctor()
extern void Spawner__ctor_mA37BC0F6624E147B76FC192F6E53162998BCA0A5 ();
// 0x000000CA System.Void StickPreviewSpawner::Awake()
extern void StickPreviewSpawner_Awake_mDEFB97F229EF73245BFDE75B32AFF9E705071933 ();
// 0x000000CB System.Void StickPreviewSpawner::OnEnable()
extern void StickPreviewSpawner_OnEnable_m2FDEC0217872F4B9EC0BA07E9E02B268CC63C669 ();
// 0x000000CC System.Void StickPreviewSpawner::OnDisable()
extern void StickPreviewSpawner_OnDisable_m46401F78BEE6FBA11631D4F01404ABB226FC8855 ();
// 0x000000CD System.Void StickPreviewSpawner::Destroy()
extern void StickPreviewSpawner_Destroy_m510D6F5D305A491111E6C2494A252C28D516F413 ();
// 0x000000CE System.Void StickPreviewSpawner::Start()
extern void StickPreviewSpawner_Start_m7170740E6E965ED8B2CA85609DD6EE2217329CC2 ();
// 0x000000CF System.Void StickPreviewSpawner::InitializePreview()
extern void StickPreviewSpawner_InitializePreview_m7A58F4DBBD494017628AA40942511C44BFE996E3 ();
// 0x000000D0 System.Void StickPreviewSpawner::ResetPreview()
extern void StickPreviewSpawner_ResetPreview_mBBB6CFA7197046262F41BCE1240E3C3E15791147 ();
// 0x000000D1 System.Void StickPreviewSpawner::SpawnPreview()
extern void StickPreviewSpawner_SpawnPreview_m333A0BB118050429A99B61177838C2FC347E7BA9 ();
// 0x000000D2 System.Void StickPreviewSpawner::DespawnObject()
extern void StickPreviewSpawner_DespawnObject_mEBD17A5156B5307C728469E7DD7C40F29A20D4FF ();
// 0x000000D3 System.Void StickPreviewSpawner::OnLevelUp()
extern void StickPreviewSpawner_OnLevelUp_m1F134AD5828A575F11580AEAE3E3C21843967382 ();
// 0x000000D4 System.Void StickPreviewSpawner::OnGameOver()
extern void StickPreviewSpawner_OnGameOver_m4F9829D7A2AAE37A1E6940D5D99FDC2A10A3D085 ();
// 0x000000D5 System.Void StickPreviewSpawner::.ctor()
extern void StickPreviewSpawner__ctor_mE5EC46757F61DF2CA1600BC2D0277D6E564DB33A ();
// 0x000000D6 System.Void StickSpawnerController::Awake()
extern void StickSpawnerController_Awake_m8A2D63B8E0FA427613448292E3EA8C2802BDB12E ();
// 0x000000D7 System.Void StickSpawnerController::OnEnable()
extern void StickSpawnerController_OnEnable_mE9D04035778E0C8710CA867B3FF9A146492BC0D6 ();
// 0x000000D8 System.Void StickSpawnerController::OnDisable()
extern void StickSpawnerController_OnDisable_m28F26EDBF34053A8C23CA5A5AE63E431DACDF3A3 ();
// 0x000000D9 System.Void StickSpawnerController::Destroy()
extern void StickSpawnerController_Destroy_m41F4186E3F6D8038284E4E6073F0E339B815166B ();
// 0x000000DA System.Void StickSpawnerController::SpawnStick()
extern void StickSpawnerController_SpawnStick_mEFC59DBB0AA159E9042DB2AFCE99664D3A5C37E0 ();
// 0x000000DB System.Void StickSpawnerController::ResetStickValue()
extern void StickSpawnerController_ResetStickValue_mDFB37286D63524B52924F2FDF021715C6D3A9627 ();
// 0x000000DC System.Void StickSpawnerController::InitializeStick()
extern void StickSpawnerController_InitializeStick_m8CA884EE10A1E7451667D89216134284CDBB13C0 ();
// 0x000000DD System.Void StickSpawnerController::OnLevelUp()
extern void StickSpawnerController_OnLevelUp_m6162E75E3DC8790182E5B8D5B02204DFC351F7B2 ();
// 0x000000DE System.Void StickSpawnerController::OnGameOver()
extern void StickSpawnerController_OnGameOver_m53776CDFDC4A824AB99B564056CA632A8D327BA5 ();
// 0x000000DF System.Void StickSpawnerController::.ctor()
extern void StickSpawnerController__ctor_m9B4666F424C3C8C8EE8DFD42DFEEA28164071F95 ();
// 0x000000E0 System.Void Timer::Awake()
extern void Timer_Awake_mDBBA7C85E96EDCBE97C62856B37EB1C3A640554F ();
// 0x000000E1 System.Void Timer::OnEnable()
extern void Timer_OnEnable_m3528CB33DE1F38EDE73BF26C9AFE3A4C2B449728 ();
// 0x000000E2 System.Void Timer::OnDisable()
extern void Timer_OnDisable_mE3BBA6A32D085B9C5CCC04ED0C36E99BCD33E443 ();
// 0x000000E3 System.Void Timer::Destroy()
extern void Timer_Destroy_m6CF3B81FA52B7074071BB977EED750F4FA068D1E ();
// 0x000000E4 System.Void Timer::Start()
extern void Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4 ();
// 0x000000E5 System.Void Timer::Update()
extern void Timer_Update_mDC9C24926E911E84E153DA4BFCC1689E82D07566 ();
// 0x000000E6 System.Void Timer::DeductTime()
extern void Timer_DeductTime_m5C15BF2417742A6601F3D21A4978BE726D416085 ();
// 0x000000E7 System.Void Timer::ResetTimer()
extern void Timer_ResetTimer_m3C539ACD37BC8733D3DBCF702BEB00C1C2EFEA14 ();
// 0x000000E8 System.Void Timer::SetTimerDisplayValue()
extern void Timer_SetTimerDisplayValue_m1390495D4996C4CEE5C222F2D3A3FD024F720E93 ();
// 0x000000E9 System.Void Timer::OnInitializeTimer()
extern void Timer_OnInitializeTimer_m2B1BABB6BCFA242738B509303FB540F8606AD5F5 ();
// 0x000000EA System.Void Timer::.ctor()
extern void Timer__ctor_m05F02A43855C43EEF0FE3171A63A895A95D9322C ();
// 0x000000EB System.Void Timer::<OnInitializeTimer>b__16_0()
extern void Timer_U3COnInitializeTimerU3Eb__16_0_mE2BEB80520227E11F97A155E6C1972DB620F7C42 ();
// 0x000000EC System.Void TitleAdapter::OnEnable()
extern void TitleAdapter_OnEnable_mA1AF3760E2D50C46B68672DD2333248799E7A7CD ();
// 0x000000ED System.Void TitleAdapter::Awake()
extern void TitleAdapter_Awake_m4D3D30CEFCAFA963FDD27027EE7BC74DDAD8B6F9 ();
// 0x000000EE System.Void TitleAdapter::OnDisable()
extern void TitleAdapter_OnDisable_m0D3C4D4022F7DD94AE4F22DA4E0D563524F96612 ();
// 0x000000EF System.Void TitleAdapter::Destroy()
extern void TitleAdapter_Destroy_mE498EBB70349645DD6EF6A029EDDE6639BE4090A ();
// 0x000000F0 System.Void TitleAdapter::OnPlay()
extern void TitleAdapter_OnPlay_m24D71D825BB406C9B780F9208AE09D52AB29B361 ();
// 0x000000F1 System.Void TitleAdapter::.ctor()
extern void TitleAdapter__ctor_m1E2374792E4B46CE8ABAAE833FE8DD6D70397678 ();
// 0x000000F2 System.Void TitleAdapter::<OnPlay>b__6_0()
extern void TitleAdapter_U3COnPlayU3Eb__6_0_mB213EA7140D3DC1ABE33AE60E81E81BCE509B4EA ();
// 0x000000F3 System.Void MessageAdapter::OnMouseDown()
extern void MessageAdapter_OnMouseDown_m72FE753ED5E52D5FBFB7C335FD73D65982CCBAA1 ();
// 0x000000F4 System.Void MessageAdapter::.ctor()
extern void MessageAdapter__ctor_m4EB5A62CB7F18947835DE7019DF888352E3A4A65 ();
// 0x000000F5 System.Void MessageTestHandler::OnEnable()
extern void MessageTestHandler_OnEnable_m2E47AB4795ADD9C845AB943D33D7BC9A3715C54D ();
// 0x000000F6 System.Void MessageTestHandler::Awake()
extern void MessageTestHandler_Awake_mBB8519C50A6265C9451398CE5B0D2F6839B4C191 ();
// 0x000000F7 System.Void MessageTestHandler::OnDisable()
extern void MessageTestHandler_OnDisable_m8044EE6BADFEC27CC783C7F9442E9C2B78774FFB ();
// 0x000000F8 System.Void MessageTestHandler::Destroy()
extern void MessageTestHandler_Destroy_mA66E042933400BCC0F3AA154EA844B41AE2FAD91 ();
// 0x000000F9 System.Void MessageTestHandler::OnReceivedReactNativeMessage(MessageHandler)
extern void MessageTestHandler_OnReceivedReactNativeMessage_m579776235CABC0AF77FB51CB639FBA8DADF76764 ();
// 0x000000FA System.Void MessageTestHandler::SendMessageToReactNative()
extern void MessageTestHandler_SendMessageToReactNative_mE0C241CAC952C4FCF3FC384AFF0076B27D9C1511 ();
// 0x000000FB System.Void MessageTestHandler::VerificationMessage()
extern void MessageTestHandler_VerificationMessage_m1F7E7EEEB402333FF89F9F82E65F97D68BA9E63D ();
// 0x000000FC System.Void MessageTestHandler::Verified()
extern void MessageTestHandler_Verified_m3217C0BDC199A1A471541CF6E37549F81B383028 ();
// 0x000000FD System.Collections.IEnumerator MessageTestHandler::QuitGame()
extern void MessageTestHandler_QuitGame_m4D0A49391B042E390B76BBDF91365772D8191AFC ();
// 0x000000FE System.Void MessageTestHandler::OnQuitUnity()
extern void MessageTestHandler_OnQuitUnity_m462BAF7A1E88E4C5CDB7813B5BA818CFCE18C3BD ();
// 0x000000FF System.Void MessageTestHandler::.ctor()
extern void MessageTestHandler__ctor_mA305B3C314B7ECE70F64756228FD1A36EA7953F5 ();
// 0x00000100 System.Void MessageTestHandler::<SendMessageToReactNative>b__9_0(System.Object)
extern void MessageTestHandler_U3CSendMessageToReactNativeU3Eb__9_0_mD0B69CD43F8B07FB9882EECAD97A5B06809A2006 ();
// 0x00000101 System.Void MessageTestHandler::<VerificationMessage>b__10_0(System.Object)
extern void MessageTestHandler_U3CVerificationMessageU3Eb__10_0_m3CA40F7B81EE14688F9E6CA3D84BD1BA45142E51 ();
// 0x00000102 System.Void JsonNetSample::Start()
extern void JsonNetSample_Start_mC580188F2BDC0DA97BA536FCC61C2972D0215D42 ();
// 0x00000103 System.Void JsonNetSample::WriteLine(System.String)
extern void JsonNetSample_WriteLine_mCFF21EB23B15C700A84D0C8FB80117AF81252AC3 ();
// 0x00000104 System.Void JsonNetSample::TestJson()
extern void JsonNetSample_TestJson_mA1E9139B73CDBBEF5EB43E0C23ADD53C97B3A836 ();
// 0x00000105 System.Void JsonNetSample::SerailizeJson()
extern void JsonNetSample_SerailizeJson_mC1C78EAC1B88AE45BF34C98DFCE601ACE3E18984 ();
// 0x00000106 System.Void JsonNetSample::DeserializeJson()
extern void JsonNetSample_DeserializeJson_m25A0086832CAD4216DEFABA6332DD9B6F4E7C30A ();
// 0x00000107 System.Void JsonNetSample::LinqToJson()
extern void JsonNetSample_LinqToJson_mACABD21931966F2DFA717B3AF9552D91B4436EF7 ();
// 0x00000108 System.Void JsonNetSample::JsonPath()
extern void JsonNetSample_JsonPath_mEBA2D157E494E22CC74857C90B9CB7982B655B0C ();
// 0x00000109 System.Void JsonNetSample::.ctor()
extern void JsonNetSample__ctor_m5C88D5BD2710CF7D33C50DC06DC562CB17FACE46 ();
// 0x0000010A System.Void Rotate::Awake()
extern void Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515 ();
// 0x0000010B System.Void Rotate::onDestroy()
extern void Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A ();
// 0x0000010C System.Void Rotate::onMessage(MessageHandler)
extern void Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C ();
// 0x0000010D System.Void Rotate::OnMouseDown()
extern void Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988 ();
// 0x0000010E System.Void Rotate::Update()
extern void Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330 ();
// 0x0000010F System.Void Rotate::.ctor()
extern void Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF ();
// 0x00000110 MessageHandler MessageHandler::Deserialize(System.String)
extern void MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE ();
// 0x00000111 T MessageHandler::getData()
// 0x00000112 System.Void MessageHandler::.ctor(System.Int32,System.String,System.String,Newtonsoft.Json.Linq.JToken)
extern void MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC ();
// 0x00000113 System.Void MessageHandler::send(System.Object)
extern void MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E ();
// 0x00000114 System.Void UnityMessage::.ctor()
extern void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 ();
// 0x00000115 System.Void UnityMessageManager::onUnityMessage(System.String)
extern void UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4 ();
// 0x00000116 System.Int32 UnityMessageManager::generateId()
extern void UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89 ();
// 0x00000117 UnityMessageManager UnityMessageManager::get_Instance()
extern void UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16 ();
// 0x00000118 System.Void UnityMessageManager::set_Instance(UnityMessageManager)
extern void UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE ();
// 0x00000119 System.Void UnityMessageManager::add_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344 ();
// 0x0000011A System.Void UnityMessageManager::remove_OnMessage(UnityMessageManager_MessageDelegate)
extern void UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4 ();
// 0x0000011B System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA ();
// 0x0000011C System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager_MessageHandlerDelegate)
extern void UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E ();
// 0x0000011D System.Void UnityMessageManager::.cctor()
extern void UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B ();
// 0x0000011E System.Void UnityMessageManager::Awake()
extern void UnityMessageManager_Awake_mCBF7274D38E90447257210355EEBED184FF0C035 ();
// 0x0000011F System.Void UnityMessageManager::SendMessageToRN(System.String)
extern void UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250 ();
// 0x00000120 System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
extern void UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E ();
// 0x00000121 System.Void UnityMessageManager::onMessage(System.String)
extern void UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485 ();
// 0x00000122 System.Void UnityMessageManager::onRNMessage(System.String)
extern void UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8 ();
// 0x00000123 System.Void UnityMessageManager::.ctor()
extern void UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7 ();
// 0x00000124 System.Void Verification::Awake()
extern void Verification_Awake_m4CD0EE2E0009E58A617FA08FA6D449DF1B2E226D ();
// 0x00000125 System.Void Verification::OnEnable()
extern void Verification_OnEnable_mE50987A371FD54F5292E20F154AC2A490D011107 ();
// 0x00000126 System.Void Verification::OnDisable()
extern void Verification_OnDisable_m18806AEBCF408EB9266CE92A75C8F7F9407BF21B ();
// 0x00000127 System.Void Verification::Destroy()
extern void Verification_Destroy_m8607EFAAF4C6E7D28461C5B10AFED955AD49C446 ();
// 0x00000128 System.Void Verification::OnReceivedReactNativeMessage(MessageHandler)
extern void Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534 ();
// 0x00000129 System.Void Verification::Start()
extern void Verification_Start_m724EA6CB3B87E138FBBF99835797BCB0BAE3EAB9 ();
// 0x0000012A System.Void Verification::VerificationMessage()
extern void Verification_VerificationMessage_m3C94AE37F59AB2F5AF0B18A9D95BAD98B6188E1F ();
// 0x0000012B System.Void Verification::Verified()
extern void Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC ();
// 0x0000012C System.Void Verification::HideVerificationPopUp()
extern void Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C ();
// 0x0000012D System.Void Verification::OnPlayAgain()
extern void Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309 ();
// 0x0000012E System.Void Verification::ShowVerificationPopUp()
extern void Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318 ();
// 0x0000012F System.Void Verification::.ctor()
extern void Verification__ctor_mF7CEB04FD552F04F0FEE9512769E3571FE91E663 ();
// 0x00000130 System.Void Verification::<VerificationMessage>b__11_0(System.Object)
extern void Verification_U3CVerificationMessageU3Eb__11_0_mF013FC22CF55ECA6FE3AF190613E520947622178 ();
// 0x00000131 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOFade(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOFade_m39CB8EF508F3CF05B29E84981A2A88CC5956D1A0 ();
// 0x00000132 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOPitch(UnityEngine.AudioSource,System.Single,System.Single)
extern void DOTweenModuleAudio_DOPitch_m7F93B8BD0AC634A3F9C4744C0C6966929194C74B ();
// 0x00000133 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleAudio::DOSetFloat(UnityEngine.Audio.AudioMixer,System.String,System.Single,System.Single)
extern void DOTweenModuleAudio_DOSetFloat_m935C7ABBCB182FB62B3DE35EEB19B9475827C175 ();
// 0x00000134 System.Int32 DG.Tweening.DOTweenModuleAudio::DOComplete(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOComplete_m99FA15157545779778E7635788E648CA14A5F298 ();
// 0x00000135 System.Int32 DG.Tweening.DOTweenModuleAudio::DOKill(UnityEngine.Audio.AudioMixer,System.Boolean)
extern void DOTweenModuleAudio_DOKill_m7D939E2BEF59494626A19583D90F10E00E405AC5 ();
// 0x00000136 System.Int32 DG.Tweening.DOTweenModuleAudio::DOFlip(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOFlip_mB770E32FEA8C7992DD40A29014AAB7D9D8E869E9 ();
// 0x00000137 System.Int32 DG.Tweening.DOTweenModuleAudio::DOGoto(UnityEngine.Audio.AudioMixer,System.Single,System.Boolean)
extern void DOTweenModuleAudio_DOGoto_m877E17C8F971BDAD1BC2C766068C4976B553716F ();
// 0x00000138 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPause_m88B2EF1067BAC84AFA75245DC89A73A2E6D26AC6 ();
// 0x00000139 System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlay(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlay_m3614FB2415D70C906BFE9B0C7E32C8DA26270936 ();
// 0x0000013A System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayBackwards(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayBackwards_m97A8444C91B77C69D57581350BCFF78A0DA9BEDA ();
// 0x0000013B System.Int32 DG.Tweening.DOTweenModuleAudio::DOPlayForward(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOPlayForward_m4C6F66E6B3DD5B2A09518BA9F89659A4F7F23F27 ();
// 0x0000013C System.Int32 DG.Tweening.DOTweenModuleAudio::DORestart(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORestart_m0E909925A1AEDCC87B9D39EF62D241BB61531A77 ();
// 0x0000013D System.Int32 DG.Tweening.DOTweenModuleAudio::DORewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DORewind_m537136DC6FB152AD3B3C4789E5B48951168EE41B ();
// 0x0000013E System.Int32 DG.Tweening.DOTweenModuleAudio::DOSmoothRewind(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOSmoothRewind_mC4E761C3D34EDA9F3FDE8AD0E088560ECF7D0984 ();
// 0x0000013F System.Int32 DG.Tweening.DOTweenModuleAudio::DOTogglePause(UnityEngine.Audio.AudioMixer)
extern void DOTweenModuleAudio_DOTogglePause_mD4E4C4AF868EE5F42E2B258809C4461FE0ED1A20 ();
// 0x00000140 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMove(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMove_mA271F3426E3EF7AA8DCF9CA457B4CA7943879AAB ();
// 0x00000141 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveX(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveX_mF6099EC3DBFDE8E813B1D41701221024593E21AC ();
// 0x00000142 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveY(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveY_mD323825CCFD8229556C4FF55A2B86ADC7DADBC74 ();
// 0x00000143 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics::DOMoveZ(UnityEngine.Rigidbody,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOMoveZ_mC77389BF46767B1F72DABC8201AE84B727253279 ();
// 0x00000144 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DORotate(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.RotateMode)
extern void DOTweenModulePhysics_DORotate_mFD933B3DF2A7767A345F1335644AAFE1B078D7B7 ();
// 0x00000145 DG.Tweening.Core.TweenerCore`3<UnityEngine.Quaternion,UnityEngine.Vector3,DG.Tweening.Plugins.Options.QuaternionOptions> DG.Tweening.DOTweenModulePhysics::DOLookAt(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,DG.Tweening.AxisConstraint,System.Nullable`1<UnityEngine.Vector3>)
extern void DOTweenModulePhysics_DOLookAt_m231F5DC2C8DD72117C4CB668B146EC17A66E6069 ();
// 0x00000146 DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics::DOJump(UnityEngine.Rigidbody,UnityEngine.Vector3,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics_DOJump_m45253F124D36FB2A26B5DE8B5EEA2CCCDE773B15 ();
// 0x00000147 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOPath_m798F6F69BB6E19915A2DC9D234A6972DFED348D0 ();
// 0x00000148 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,UnityEngine.Vector3[],System.Single,DG.Tweening.PathType,DG.Tweening.PathMode,System.Int32,System.Nullable`1<UnityEngine.Color>)
extern void DOTweenModulePhysics_DOLocalPath_mFB9AEB7FBA37EB06B7D48287309430C4E54B7343 ();
// 0x00000149 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOPath_m195A56635B845275DEBC57F3847100DCC05A8821 ();
// 0x0000014A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModulePhysics::DOLocalPath(UnityEngine.Rigidbody,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void DOTweenModulePhysics_DOLocalPath_m89AEF519ECCA560058C4149C448FC3D10CCA5078 ();
// 0x0000014B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMove(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMove_mCD3E097FA2B1499BF713027E0AAEBFFA94770D71 ();
// 0x0000014C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveX(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveX_m483FD0BD2030141F8809A812C1640C66C7E9B9E2 ();
// 0x0000014D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModulePhysics2D::DOMoveY(UnityEngine.Rigidbody2D,System.Single,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOMoveY_mC8760E855D826585531D9CBB492E6AA5B39A52AC ();
// 0x0000014E DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModulePhysics2D::DORotate(UnityEngine.Rigidbody2D,System.Single,System.Single)
extern void DOTweenModulePhysics2D_DORotate_m0176C1D02F44023C8C404A82930656D153C87126 ();
// 0x0000014F DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D::DOJump(UnityEngine.Rigidbody2D,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModulePhysics2D_DOJump_mDADF6CC5B3A4C0664CB346FB97566158311E51B2 ();
// 0x00000150 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOColor_mF881B290086523415B55BF72B83146541C2732D7 ();
// 0x00000151 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleSprite::DOFade(UnityEngine.SpriteRenderer,System.Single,System.Single)
extern void DOTweenModuleSprite_DOFade_m99BCBC9FD737A6D3354ABFE7C8A2EC98EE0D7AA8 ();
// 0x00000152 DG.Tweening.Sequence DG.Tweening.DOTweenModuleSprite::DOGradientColor(UnityEngine.SpriteRenderer,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleSprite_DOGradientColor_m57175DC68A81F1142793E83BC4DFD3C0E7457AB8 ();
// 0x00000153 DG.Tweening.Tweener DG.Tweening.DOTweenModuleSprite::DOBlendableColor(UnityEngine.SpriteRenderer,UnityEngine.Color,System.Single)
extern void DOTweenModuleSprite_DOBlendableColor_m57100BAC9DF79F3179A0F17A685DA553728586CC ();
// 0x00000154 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.CanvasGroup,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mAD40AF255234B54E5BA8F81219C9C30E520EFB10 ();
// 0x00000155 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m224983D24DA5CC099E78D8FA2C4FC159F6385CA6 ();
// 0x00000156 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Graphic,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mD3FD501A6776914AE93FF9B9722A9EAD4B2DBAF9 ();
// 0x00000157 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m60BCBDB46E4B4E67BE9D9984CAEDA8065AC3F050 ();
// 0x00000158 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_mF285AA1CEFC18A119A640DFEC3FC8216E92C7407 ();
// 0x00000159 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOFillAmount(UnityEngine.UI.Image,System.Single,System.Single)
extern void DOTweenModuleUI_DOFillAmount_mEFF5F991C07ADB2611F22051613B7ACEF66B9E4A ();
// 0x0000015A DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOGradientColor(UnityEngine.UI.Image,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUI_DOGradientColor_m6CC119CCB3273C03076FCBDAFB9E0CBD169D2D0B ();
// 0x0000015B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOFlexibleSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOFlexibleSize_mFDE51E1609ADFE1E5D1161ED9B2BB55B140DCEEE ();
// 0x0000015C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOMinSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOMinSize_m0BF4109021D52DE8577A8FADFC069409C0BEC366 ();
// 0x0000015D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPreferredSize(UnityEngine.UI.LayoutElement,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPreferredSize_mFCEDB7320442DCD6D7853C62F049BDA0185EF278 ();
// 0x0000015E DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Outline,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_mB023CF7E6FFCFC847151C5AAA0CAEA00C80046E1 ();
// 0x0000015F DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Outline,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m4DB434C44B5B2906B99B8926B6FDA2CADEF8A7B7 ();
// 0x00000160 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOScale(UnityEngine.UI.Outline,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOScale_m9E98A3E1A4F40F42336B7A83EDE00B4EFA25C4B5 ();
// 0x00000161 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos_mCB314AB88AABF23417E5671CB1CC2C35591920AD ();
// 0x00000162 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosX_mC9A3B16335FD5A5157688FF499FE782E5F5C7A33 ();
// 0x00000163 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPosY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPosY_m839C67C3E8C54701B7FC5EAB46B911C0DF192E97 ();
// 0x00000164 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3D(UnityEngine.RectTransform,UnityEngine.Vector3,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3D_m595AA5252C74166C89DFC53A9052E4DCCEAF1921 ();
// 0x00000165 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DX(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DX_m83F31312715DF4E65F5968115CEDB185A4648B8F ();
// 0x00000166 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DY(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DY_mB024133ED57232447FF4EB17C861AD70A44F304A ();
// 0x00000167 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorPos3DZ(UnityEngine.RectTransform,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorPos3DZ_m56234AB8B466C82A5488AFAE36F7153B82BE2CB9 ();
// 0x00000168 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMax(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMax_m789E726166F500902973D121DE47E178BF772AB7 ();
// 0x00000169 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOAnchorMin(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOAnchorMin_m3E6515C288D04F55674513677FE209E2727448BA ();
// 0x0000016A DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivot(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single)
extern void DOTweenModuleUI_DOPivot_m080B5E1942554C274704FE5E024EA9CDF6909492 ();
// 0x0000016B DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotX(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotX_mF68B3F4FE5D0CAAA9E1203C6A3BE2557B2365688 ();
// 0x0000016C DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOPivotY(UnityEngine.RectTransform,System.Single,System.Single)
extern void DOTweenModuleUI_DOPivotY_m2A449ECB26A50F9CA43F852CACE4B876A5094CAC ();
// 0x0000016D DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUI::DOSizeDelta(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOSizeDelta_m1E034E8BDB38D7F4F563AB9523891D7F215AA8CF ();
// 0x0000016E DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOPunchAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOPunchAnchorPos_mF564424EB231C6B1EC4262BD235C6F4C7B954EAE ();
// 0x0000016F DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,System.Single,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m19C5739A636820F747EA76567409F0773A8C7CB8 ();
// 0x00000170 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOShakeAnchorPos(UnityEngine.RectTransform,System.Single,UnityEngine.Vector2,System.Int32,System.Single,System.Boolean,System.Boolean)
extern void DOTweenModuleUI_DOShakeAnchorPos_m14E79F46BF244846CD0C57D4F0ECCF44943875AE ();
// 0x00000171 DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI::DOJumpAnchorPos(UnityEngine.RectTransform,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOJumpAnchorPos_mF579D5EC1A7F51586A2C0409B34CC105BF480F41 ();
// 0x00000172 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DONormalizedPos(UnityEngine.UI.ScrollRect,UnityEngine.Vector2,System.Single,System.Boolean)
extern void DOTweenModuleUI_DONormalizedPos_mAF52FC4BCBDE295E6A2F3C297188469D7BE4DF56 ();
// 0x00000173 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOHorizontalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOHorizontalNormalizedPos_m4DC3AE19F8B0437E7D26930A2897CC479161A134 ();
// 0x00000174 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOVerticalNormalizedPos(UnityEngine.UI.ScrollRect,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOVerticalNormalizedPos_m0B870F709BF1790B6DB4C8F05A6B7ECE8CB1D231 ();
// 0x00000175 DG.Tweening.Core.TweenerCore`3<System.Single,System.Single,DG.Tweening.Plugins.Options.FloatOptions> DG.Tweening.DOTweenModuleUI::DOValue(UnityEngine.UI.Slider,System.Single,System.Single,System.Boolean)
extern void DOTweenModuleUI_DOValue_m6A01D22DCB8142450DCC0C43046C2F0D5C39D00E ();
// 0x00000176 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOColor_m981384A20E9E9EA3BF1B105E1850D8DCFA1D1F60 ();
// 0x00000177 DG.Tweening.Core.TweenerCore`3<UnityEngine.Color,UnityEngine.Color,DG.Tweening.Plugins.Options.ColorOptions> DG.Tweening.DOTweenModuleUI::DOFade(UnityEngine.UI.Text,System.Single,System.Single)
extern void DOTweenModuleUI_DOFade_m0A792EC1128D0C1C77D39B0E859CFFB45A51E479 ();
// 0x00000178 DG.Tweening.Core.TweenerCore`3<System.String,System.String,DG.Tweening.Plugins.Options.StringOptions> DG.Tweening.DOTweenModuleUI::DOText(UnityEngine.UI.Text,System.String,System.Single,System.Boolean,DG.Tweening.ScrambleMode,System.String)
extern void DOTweenModuleUI_DOText_m811BBD8FA9215A129160FC0A7D746E1A3741465F ();
// 0x00000179 DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Graphic,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m2F817AE8F4922B274AF2B1E7BFDD0E5D7C623BFC ();
// 0x0000017A DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Image,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m140B9C6CD1E6402158382C49918DB303E39369B7 ();
// 0x0000017B DG.Tweening.Tweener DG.Tweening.DOTweenModuleUI::DOBlendableColor(UnityEngine.UI.Text,UnityEngine.Color,System.Single)
extern void DOTweenModuleUI_DOBlendableColor_m8666A89B76B8D202DC81C1AB12D8B45688212D51 ();
// 0x0000017C DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_mD3871F90FCE6FE5B23E23FE12F7826E44F8A05CB ();
// 0x0000017D DG.Tweening.Sequence DG.Tweening.DOTweenModuleUnityVersion::DOGradientColor(UnityEngine.Material,UnityEngine.Gradient,System.String,System.Single)
extern void DOTweenModuleUnityVersion_DOGradientColor_m1AFB6E6AED804D3261D124D2D1429134B34AFD1E ();
// 0x0000017E UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForCompletion(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForCompletion_m59773AF9A35804797C6C389A516DA63A0DBAC246 ();
// 0x0000017F UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForRewind(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForRewind_mC6F53B8F61682B52F390434AFCEC775A4EB49F53 ();
// 0x00000180 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForKill(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForKill_m871BB15AC97A221A77DEE06B90AD1EB1DD97667D ();
// 0x00000181 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForElapsedLoops(DG.Tweening.Tween,System.Int32,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForElapsedLoops_m10EBF8473875B14ADA1C065043BE47B9C3DE99D4 ();
// 0x00000182 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForPosition(DG.Tweening.Tween,System.Single,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForPosition_mF7210B505549168AF420CC3453D42DC85ACFE977 ();
// 0x00000183 UnityEngine.CustomYieldInstruction DG.Tweening.DOTweenModuleUnityVersion::WaitForStart(DG.Tweening.Tween,System.Boolean)
extern void DOTweenModuleUnityVersion_WaitForStart_mC3E5A8737A87F6DD8ED2380957AE33B786DA1401 ();
// 0x00000184 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOOffset(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOOffset_m2A623BF72AA0289033B412F4D93DF5317FF620CF ();
// 0x00000185 DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector2,UnityEngine.Vector2,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.DOTweenModuleUnityVersion::DOTiling(UnityEngine.Material,UnityEngine.Vector2,System.Int32,System.Single)
extern void DOTweenModuleUnityVersion_DOTiling_mE9A75D5B8CFD751EDCB4231A9FBB2EC003752D97 ();
// 0x00000186 System.Void DG.Tweening.DOTweenModuleUtils::Init()
extern void DOTweenModuleUtils_Init_mB7D252D24842502558B1DF4DEAC5B08887202DBC ();
// 0x00000187 System.Void DG.Tweening.DOTweenModuleUtils::Preserver()
extern void DOTweenModuleUtils_Preserver_mE88FFEE9E520275AC0F42ACBE27D7F2265E33DBB ();
// 0x00000188 System.Void CollisionEventSender_OnSendEventMessage::.ctor()
extern void OnSendEventMessage__ctor_mC2220D53157F8BECFCC66A9CF407D4D4117252A5 ();
// 0x00000189 System.Void GameController_OnPlayAgain::.ctor(System.Object,System.IntPtr)
extern void OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390 ();
// 0x0000018A System.Void GameController_OnPlayAgain::Invoke()
extern void OnPlayAgain_Invoke_m2EF6E655A0D3B20EAF97B230803551E1B6395B30 ();
// 0x0000018B System.IAsyncResult GameController_OnPlayAgain::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnPlayAgain_BeginInvoke_m11EE5A9E940AA9821321221D52946B82DB372667 ();
// 0x0000018C System.Void GameController_OnPlayAgain::EndInvoke(System.IAsyncResult)
extern void OnPlayAgain_EndInvoke_m9061547C49BDECC0A6DD5017164D0EDC2B8546DC ();
// 0x0000018D System.Void GameController_OnInitializeGameUI::.ctor(System.Object,System.IntPtr)
extern void OnInitializeGameUI__ctor_m2532572CC7CE1285668873571CDCAD597FF3F12C ();
// 0x0000018E System.Void GameController_OnInitializeGameUI::Invoke()
extern void OnInitializeGameUI_Invoke_mB58EB7A06A34732F4EDC0E0DF27F4EA5409F0FE4 ();
// 0x0000018F System.IAsyncResult GameController_OnInitializeGameUI::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnInitializeGameUI_BeginInvoke_mAF66EEFCFC1A29264212C5AE7E836195992AA505 ();
// 0x00000190 System.Void GameController_OnInitializeGameUI::EndInvoke(System.IAsyncResult)
extern void OnInitializeGameUI_EndInvoke_mF6628DDE27B6A05D06358E300AF108CB3AFD1320 ();
// 0x00000191 System.Void GameController_OnGameStart::.ctor(System.Object,System.IntPtr)
extern void OnGameStart__ctor_m90DE9F22B1C9203ACF924759EAFAC019C7BB53C5 ();
// 0x00000192 System.Void GameController_OnGameStart::Invoke()
extern void OnGameStart_Invoke_m92DE6668CCE4F068DDCE1CC48053F60A2ED0E792 ();
// 0x00000193 System.IAsyncResult GameController_OnGameStart::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnGameStart_BeginInvoke_m0E788DFACAB2911942CA8D716E0DF748DFF8C774 ();
// 0x00000194 System.Void GameController_OnGameStart::EndInvoke(System.IAsyncResult)
extern void OnGameStart_EndInvoke_m5BFDA21CA96642DBD19AE74BFF59E41155D0BE07 ();
// 0x00000195 System.Void GameController_OnPlaying::.ctor(System.Object,System.IntPtr)
extern void OnPlaying__ctor_m9ED87EC988E36BF3B65CE46F7E62A98278E81774 ();
// 0x00000196 System.Void GameController_OnPlaying::Invoke()
extern void OnPlaying_Invoke_mFC97A0E03A72D930F91ED5645567541F72D09E48 ();
// 0x00000197 System.IAsyncResult GameController_OnPlaying::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnPlaying_BeginInvoke_m3CC0DA9476A9C96D8575C22C8844F7B45DF2F080 ();
// 0x00000198 System.Void GameController_OnPlaying::EndInvoke(System.IAsyncResult)
extern void OnPlaying_EndInvoke_m94191BBBE2853F05E7D2B577CCFA62EC8294798A ();
// 0x00000199 System.Void GameController_OnLevelUp::.ctor(System.Object,System.IntPtr)
extern void OnLevelUp__ctor_m856D0386FD83CF7E6596DDD0D1B4437FA8765596 ();
// 0x0000019A System.Void GameController_OnLevelUp::Invoke()
extern void OnLevelUp_Invoke_m7831D64E99A8DC18A7E805DAB1341EF30D76BC7A ();
// 0x0000019B System.IAsyncResult GameController_OnLevelUp::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnLevelUp_BeginInvoke_m3C47CA2F0BD60C2577FC68CC5625E40951C8CE20 ();
// 0x0000019C System.Void GameController_OnLevelUp::EndInvoke(System.IAsyncResult)
extern void OnLevelUp_EndInvoke_mD429D63FC4E7AE15A0D410837A792C2F654896B9 ();
// 0x0000019D System.Void GameController_OnGameOver::.ctor(System.Object,System.IntPtr)
extern void OnGameOver__ctor_m567F5D197A7D21C2A9CC4040BFC0E734E1D7E68A ();
// 0x0000019E System.Void GameController_OnGameOver::Invoke()
extern void OnGameOver_Invoke_m1D82E032223044BAAAF28191266C2EEAFC7A6D54 ();
// 0x0000019F System.IAsyncResult GameController_OnGameOver::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnGameOver_BeginInvoke_mACC07A55BA95350EB457298C48D7B1C69397E907 ();
// 0x000001A0 System.Void GameController_OnGameOver::EndInvoke(System.IAsyncResult)
extern void OnGameOver_EndInvoke_m1BE0930C2AA10E20F9E6CDB827C672B0F4793E0E ();
// 0x000001A1 System.Void GameController_OnShowGiftbox::.ctor(System.Object,System.IntPtr)
extern void OnShowGiftbox__ctor_m1AB838BF8ED671A7428F4661D0677566C4CBBC93 ();
// 0x000001A2 System.Void GameController_OnShowGiftbox::Invoke()
extern void OnShowGiftbox_Invoke_m03ACAB74F660F6651BA9C5B2676E0B737392344A ();
// 0x000001A3 System.IAsyncResult GameController_OnShowGiftbox::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnShowGiftbox_BeginInvoke_m34C953C2DBECBC2FEA626A3F7FC483F52AFA73A3 ();
// 0x000001A4 System.Void GameController_OnShowGiftbox::EndInvoke(System.IAsyncResult)
extern void OnShowGiftbox_EndInvoke_m4ABC998A40947EFD3BCEED7F4B8D4F0DCF58A187 ();
// 0x000001A5 System.Void GameController_OnShowPlayAgain::.ctor(System.Object,System.IntPtr)
extern void OnShowPlayAgain__ctor_mC522658A38477164950C106A67CE7E1AEA275C4F ();
// 0x000001A6 System.Void GameController_OnShowPlayAgain::Invoke()
extern void OnShowPlayAgain_Invoke_m33746D8A5E540BD11C470C653EECF60BC0C4CD45 ();
// 0x000001A7 System.IAsyncResult GameController_OnShowPlayAgain::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnShowPlayAgain_BeginInvoke_m4873EC8B2BCBF00CD1413027BBE1266BE4BB37E2 ();
// 0x000001A8 System.Void GameController_OnShowPlayAgain::EndInvoke(System.IAsyncResult)
extern void OnShowPlayAgain_EndInvoke_m089882F38921D8A7F595148B6EB8BCBB39C0CC28 ();
// 0x000001A9 System.Void GameController_CurrentLevelData::.ctor()
extern void CurrentLevelData__ctor_mCB087A0A7E55A5B870D77CB3370C206049DCFAF5 ();
// 0x000001AA System.Void HudController_<tempChecker>d__4::.ctor(System.Int32)
extern void U3CtempCheckerU3Ed__4__ctor_mFD4DC68E759D5829D60F2406E6098C424FEC6FA9 ();
// 0x000001AB System.Void HudController_<tempChecker>d__4::System.IDisposable.Dispose()
extern void U3CtempCheckerU3Ed__4_System_IDisposable_Dispose_mB85A6FB2D3D1A80833F5A3678A02717004015716 ();
// 0x000001AC System.Boolean HudController_<tempChecker>d__4::MoveNext()
extern void U3CtempCheckerU3Ed__4_MoveNext_mF002131FF191BA766FCAF967ADFBE46659DADC3C ();
// 0x000001AD System.Object HudController_<tempChecker>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CtempCheckerU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AEB75A4D9B4B56439B4C080B89F714B8ECCF6E9 ();
// 0x000001AE System.Void HudController_<tempChecker>d__4::System.Collections.IEnumerator.Reset()
extern void U3CtempCheckerU3Ed__4_System_Collections_IEnumerator_Reset_m0C5FD4AD543F8BDB03EC71996B304B1F66A6F22D ();
// 0x000001AF System.Object HudController_<tempChecker>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CtempCheckerU3Ed__4_System_Collections_IEnumerator_get_Current_m6944757F318827040B06C6C25D6AFBAA2973E6C5 ();
// 0x000001B0 System.Void ObjectRotation_RotationElements::.ctor()
extern void RotationElements__ctor_mD6087F05B15B78D3E379873DD823F58B67848AE4 ();
// 0x000001B1 System.Void ObjectRotation_<StartRotation>d__10::.ctor(System.Int32)
extern void U3CStartRotationU3Ed__10__ctor_m5F6F08022723161FA5D9A565E83771A3C1375CF9 ();
// 0x000001B2 System.Void ObjectRotation_<StartRotation>d__10::System.IDisposable.Dispose()
extern void U3CStartRotationU3Ed__10_System_IDisposable_Dispose_m02925667265D142BE6FA3375207824B0B0227FC8 ();
// 0x000001B3 System.Boolean ObjectRotation_<StartRotation>d__10::MoveNext()
extern void U3CStartRotationU3Ed__10_MoveNext_m2FEBAAD64269967C9DE907E91CC39DEAE23B1034 ();
// 0x000001B4 System.Object ObjectRotation_<StartRotation>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartRotationU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E69544420E2AD796426066A1D429AFBA08AF296 ();
// 0x000001B5 System.Void ObjectRotation_<StartRotation>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartRotationU3Ed__10_System_Collections_IEnumerator_Reset_mE692E63C0CC6C47FE40928776F416A46820B0413 ();
// 0x000001B6 System.Object ObjectRotation_<StartRotation>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartRotationU3Ed__10_System_Collections_IEnumerator_get_Current_m7EF025F4DF265DA6A63EC2DF94C54A4FBB548E65 ();
// 0x000001B7 System.Void PlayAdapter_OnClickedButtonPlay::.ctor(System.Object,System.IntPtr)
extern void OnClickedButtonPlay__ctor_mB83B69AB0F60138DDE17FF181BAC36312525BAFE ();
// 0x000001B8 System.Void PlayAdapter_OnClickedButtonPlay::Invoke()
extern void OnClickedButtonPlay_Invoke_m30DB87D7A7DD7D61B706D3A5ECAB255DF26C599D ();
// 0x000001B9 System.IAsyncResult PlayAdapter_OnClickedButtonPlay::BeginInvoke(System.AsyncCallback,System.Object)
extern void OnClickedButtonPlay_BeginInvoke_m2F6FFB95B978962EA69AE2CC5B5AB4D2B87EE3B4 ();
// 0x000001BA System.Void PlayAdapter_OnClickedButtonPlay::EndInvoke(System.IAsyncResult)
extern void OnClickedButtonPlay_EndInvoke_mBE2BCF14FC5A440E04A6A6A2F729F88FCFABCA42 ();
// 0x000001BB System.Void MessageTestHandler_CreditVerified::.ctor(System.Object,System.IntPtr)
extern void CreditVerified__ctor_m16320AB9A8833C93A22BB3F3DAC58D660F59798B ();
// 0x000001BC System.Void MessageTestHandler_CreditVerified::Invoke()
extern void CreditVerified_Invoke_mA96515B3A615F75CBF824F2B086A2E3555DE424E ();
// 0x000001BD System.IAsyncResult MessageTestHandler_CreditVerified::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreditVerified_BeginInvoke_mDF595969DBC1EF8D642D40F74EF3A6F17BDE06DC ();
// 0x000001BE System.Void MessageTestHandler_CreditVerified::EndInvoke(System.IAsyncResult)
extern void CreditVerified_EndInvoke_m53B3C86CE8DC844C5931DDC3EABFEFF175A425B0 ();
// 0x000001BF System.Void MessageTestHandler_<QuitGame>d__12::.ctor(System.Int32)
extern void U3CQuitGameU3Ed__12__ctor_m9A6AEB162750BD6E3A13033FEB4CCF41FC12FD32 ();
// 0x000001C0 System.Void MessageTestHandler_<QuitGame>d__12::System.IDisposable.Dispose()
extern void U3CQuitGameU3Ed__12_System_IDisposable_Dispose_m64D19BD20997B9FFD808751CF8F3CF4C30F93BFB ();
// 0x000001C1 System.Boolean MessageTestHandler_<QuitGame>d__12::MoveNext()
extern void U3CQuitGameU3Ed__12_MoveNext_m2C3CA77482F2B1D5127DEE502EE60E5C54EE75DC ();
// 0x000001C2 System.Object MessageTestHandler_<QuitGame>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQuitGameU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m068A5D8A334111A3DA1916EA18C18953F1867B44 ();
// 0x000001C3 System.Void MessageTestHandler_<QuitGame>d__12::System.Collections.IEnumerator.Reset()
extern void U3CQuitGameU3Ed__12_System_Collections_IEnumerator_Reset_m70743CA285DA438A4FB80BB7E5FA6EE4AAE04C0E ();
// 0x000001C4 System.Object MessageTestHandler_<QuitGame>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CQuitGameU3Ed__12_System_Collections_IEnumerator_get_Current_m27A6ABBD9EE709849393EBB9C3BD776719329552 ();
// 0x000001C5 System.Boolean JsonNetSample_Product::Equals(System.Object)
extern void Product_Equals_m24013D863F711F93519C34453E26E808E1A0686E ();
// 0x000001C6 System.Int32 JsonNetSample_Product::GetHashCode()
extern void Product_GetHashCode_m1B92DAA11F5D91C027635DEAAB04D7D0198B4E8F ();
// 0x000001C7 System.Void JsonNetSample_Product::.ctor()
extern void Product__ctor_m6EFEF541383756F20A571C74D523F6DC3D5E92B8 ();
// 0x000001C8 System.Int32 JsonNetSample_CharacterListItem::get_Id()
extern void CharacterListItem_get_Id_m9DE6CEC1C0F803779454F78D9AC5B21C7D865DFB ();
// 0x000001C9 System.Void JsonNetSample_CharacterListItem::set_Id(System.Int32)
extern void CharacterListItem_set_Id_m6AEA185207FC1D111A4CE0349143735B5EB19D68 ();
// 0x000001CA System.String JsonNetSample_CharacterListItem::get_Name()
extern void CharacterListItem_get_Name_m680B8EB072A36960C0554346E5E914ED71A80289 ();
// 0x000001CB System.Void JsonNetSample_CharacterListItem::set_Name(System.String)
extern void CharacterListItem_set_Name_mA73EFDEAD55D120E39A2014F916744B6000E3F76 ();
// 0x000001CC System.Int32 JsonNetSample_CharacterListItem::get_Level()
extern void CharacterListItem_get_Level_m10E8192D961A72A0E30E5FA3252514ACA12AB484 ();
// 0x000001CD System.Void JsonNetSample_CharacterListItem::set_Level(System.Int32)
extern void CharacterListItem_set_Level_mFEDA2AB64FAB01BE4C7ABA5753B26824422E61CF ();
// 0x000001CE System.String JsonNetSample_CharacterListItem::get_Class()
extern void CharacterListItem_get_Class_m851A77EA017AAF37F02104A72694FC28B87DFF21 ();
// 0x000001CF System.Void JsonNetSample_CharacterListItem::set_Class(System.String)
extern void CharacterListItem_set_Class_m0895FD4A2B96780726A35D0AF2097EC3183DFA9F ();
// 0x000001D0 System.String JsonNetSample_CharacterListItem::get_Sex()
extern void CharacterListItem_get_Sex_mB32D24EDEBF9CB677D6FA64BFCFE049871BD095D ();
// 0x000001D1 System.Void JsonNetSample_CharacterListItem::set_Sex(System.String)
extern void CharacterListItem_set_Sex_mF6CC4BE8BFF1464B5BC621F4357BDF1ACD93BDE6 ();
// 0x000001D2 System.Void JsonNetSample_CharacterListItem::.ctor()
extern void CharacterListItem__ctor_m4EF5B408AB89F2EA7F49504E5B7DDEE5F1378B94 ();
// 0x000001D3 System.String JsonNetSample_Movie::get_Name()
extern void Movie_get_Name_mF48FCA1C552E188CACB737D0B4831295B47D967C ();
// 0x000001D4 System.Void JsonNetSample_Movie::set_Name(System.String)
extern void Movie_set_Name_m7B776A884B9C2CE80EE8585C9A9594C5E575C69E ();
// 0x000001D5 System.String JsonNetSample_Movie::get_Description()
extern void Movie_get_Description_m6085E85663D3332133D9D49B2D1290186CEFEFCE ();
// 0x000001D6 System.Void JsonNetSample_Movie::set_Description(System.String)
extern void Movie_set_Description_mD08790AD3CA6F08B035BED9103478848CDEA3163 ();
// 0x000001D7 System.String JsonNetSample_Movie::get_Classification()
extern void Movie_get_Classification_m0459DC3BB52DF4E0F0790C799E1EFF3919BE0C6C ();
// 0x000001D8 System.Void JsonNetSample_Movie::set_Classification(System.String)
extern void Movie_set_Classification_mAB8991781F80E23F4B633D95297CD0280BCE0829 ();
// 0x000001D9 System.String JsonNetSample_Movie::get_Studio()
extern void Movie_get_Studio_mD4A42D1F0E1E5B62DF80A419EF7C6950FD3809A1 ();
// 0x000001DA System.Void JsonNetSample_Movie::set_Studio(System.String)
extern void Movie_set_Studio_m320053FBE6C2F94C64716CB445AF505B74D19A15 ();
// 0x000001DB System.Nullable`1<System.DateTime> JsonNetSample_Movie::get_ReleaseDate()
extern void Movie_get_ReleaseDate_m96B6D4EB7A3F7C778BE88725BCA78EA3CD28C423 ();
// 0x000001DC System.Void JsonNetSample_Movie::set_ReleaseDate(System.Nullable`1<System.DateTime>)
extern void Movie_set_ReleaseDate_m1D0E3581E781DEE546E1AF990057263007EFBABD ();
// 0x000001DD System.Collections.Generic.List`1<System.String> JsonNetSample_Movie::get_ReleaseCountries()
extern void Movie_get_ReleaseCountries_m86802D5D1C3A9546DC43BA163B92098F9418F877 ();
// 0x000001DE System.Void JsonNetSample_Movie::set_ReleaseCountries(System.Collections.Generic.List`1<System.String>)
extern void Movie_set_ReleaseCountries_mE815700894B23EBDE170AE332386B8CD9860B346 ();
// 0x000001DF System.Void JsonNetSample_Movie::.ctor()
extern void Movie__ctor_m3DDE2223972B64C3B44DCB2FCB17E7AB6970510F ();
// 0x000001E0 System.Void Rotate_<>c::.cctor()
extern void U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5 ();
// 0x000001E1 System.Void Rotate_<>c::.ctor()
extern void U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48 ();
// 0x000001E2 System.Void Rotate_<>c::<OnMouseDown>b__4_0(System.Object)
extern void U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1 ();
// 0x000001E3 System.Void UnityMessageManager_MessageDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8 ();
// 0x000001E4 System.Void UnityMessageManager_MessageDelegate::Invoke(System.String)
extern void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 ();
// 0x000001E5 System.IAsyncResult UnityMessageManager_MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4 ();
// 0x000001E6 System.Void UnityMessageManager_MessageDelegate::EndInvoke(System.IAsyncResult)
extern void MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35 ();
// 0x000001E7 System.Void UnityMessageManager_MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA ();
// 0x000001E8 System.Void UnityMessageManager_MessageHandlerDelegate::Invoke(MessageHandler)
extern void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 ();
// 0x000001E9 System.IAsyncResult UnityMessageManager_MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
extern void MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E ();
// 0x000001EA System.Void UnityMessageManager_MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6 ();
// 0x000001EB System.Void Verification_CreditVerified::.ctor(System.Object,System.IntPtr)
extern void CreditVerified__ctor_m6D08EA013526D0B1C954C16044BEC3CE0B8841AD ();
// 0x000001EC System.Void Verification_CreditVerified::Invoke()
extern void CreditVerified_Invoke_mFC8A312FC083A8A4CC499D6AF6F8D111811FFA82 ();
// 0x000001ED System.IAsyncResult Verification_CreditVerified::BeginInvoke(System.AsyncCallback,System.Object)
extern void CreditVerified_BeginInvoke_m14F6CC8749546E94279573689A8862B08EAFA4A2 ();
// 0x000001EE System.Void Verification_CreditVerified::EndInvoke(System.IAsyncResult)
extern void CreditVerified_EndInvoke_m3AED88BD6161023EBA8CDF74D437C950355CDCB9 ();
// 0x000001EF System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mF76AB30BEF353B65814DA53ECA69C112D9656B70 ();
// 0x000001F0 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m25F68936FD8BED0ED35D6A3A7896F1859CF0177F ();
// 0x000001F1 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m45287E9F8890C45FE0DE97D8ED6CD1F532F3FFAD ();
// 0x000001F2 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mD956E9EE11DE6923BE5E83A55A8AC88859FB8DD4 ();
// 0x000001F3 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m363ABEC29B7CBD13F98BFDBA491A9732B2D815C9 ();
// 0x000001F4 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass1_0::<DOPitch>b__1(System.Single)
extern void U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mDB296FE590C17FBF30708268F6DA39F2857FA456 ();
// 0x000001F5 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m15003879FD9FF8F13F6D0AC654A12FAAA16A825F ();
// 0x000001F6 System.Single DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m8447CD5F920C973ADB39884E1A6D0672D8E4C21F ();
// 0x000001F7 System.Void DG.Tweening.DOTweenModuleAudio_<>c__DisplayClass2_0::<DOSetFloat>b__1(System.Single)
extern void U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m99540719B3B2E5D89523D7D1C31AEF039088B8F6 ();
// 0x000001F8 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m7DD41ADAF604E6C34E536D1C97A420EAF90D45C2 ();
// 0x000001F9 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mBDD978F428BA10922F372EAB21DE234526590F98 ();
// 0x000001FA System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mE4CA69377D3128AA8557F99767C5C1E30A74A20A ();
// 0x000001FB UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m7CA25B5D3253D1A1BB83175835E6F06B37E6D3CA ();
// 0x000001FC System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m6B860F4710DC7C6C7AE202D892BEA160A389BA2D ();
// 0x000001FD UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m475A4066BB6D47109DBDA7711674E92F8B26F80F ();
// 0x000001FE System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m8E161391F70AADA55A21658A677A53AF02108AAC ();
// 0x000001FF UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass3_0::<DOMoveZ>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mFAD55624F73C8C7678B40AE2B544A4E21440093C ();
// 0x00000200 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m41CDC9DA22960F86F434E7C8EFCC926A7EB3890D ();
// 0x00000201 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass4_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mCA650E955F3C9B985D9781B4E88F9EC8E22B4DF9 ();
// 0x00000202 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m2B425FFF258947715FC7BA48E8628456D77BBB19 ();
// 0x00000203 UnityEngine.Quaternion DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass5_0::<DOLookAt>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_mBD3D65C2B6DAE37DB5CBA1BBF01D666A2908E540 ();
// 0x00000204 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_m66CDD42C56A002D2FCE56A80438EBA75BF339B62 ();
// 0x00000205 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m04813310411D22F519E3142909FCF08E4A83DB05 ();
// 0x00000206 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__1()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m2557DCAC83C5626D57FD6C4F8082A2D6BBD1CD3D ();
// 0x00000207 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mB47776ADD5831D4ED18EDE2DDE38A858AC92A9C9 ();
// 0x00000208 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m67EF6E7E97403AB9053D7811A551A6D052D90CFD ();
// 0x00000209 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass6_0::<DOJump>b__4()
extern void U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m5BE335A0259370532D153F4255FDC2788D42CE02 ();
// 0x0000020A System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m0933CB4C7EDA73C137344E0F5351FB87B9957CDA ();
// 0x0000020B UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass7_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_mC71BA0A22CF6F7A1ACEC09FC99AAC8B963F653F8 ();
// 0x0000020C System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mD6555E0F3FAE1FDE188641F5D0DEE804368CC438 ();
// 0x0000020D UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m9C60CDEB2CFE741DF9088E45428A7881F1E7E398 ();
// 0x0000020E System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass8_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19470CDE4C65D782BB85336F3A6B29D0D0E5A95B ();
// 0x0000020F System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m0C355BA31EC155AAFFE57E885820E87E2D24B630 ();
// 0x00000210 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass9_0::<DOPath>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mF111BF5DF70FA94EDC3C3A6F0D8A299272BC3262 ();
// 0x00000211 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m5158D03E78581B5AC83602167F431A45FD6E3D30 ();
// 0x00000212 UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_mBC510690793F98CC7A4CEC293AA1C1420D77B006 ();
// 0x00000213 System.Void DG.Tweening.DOTweenModulePhysics_<>c__DisplayClass10_0::<DOLocalPath>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mFECC3FFD2469DB3CFFB9B6128E1D28E0CE9A4E30 ();
// 0x00000214 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_mCD42253A116528C3ABB04E9FF155E0CD20643826 ();
// 0x00000215 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass0_0::<DOMove>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m7AAE48D11CDA351B8BD9555E638AA2AD3B967DDF ();
// 0x00000216 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m2197F33D0C489B447A1492CAF86ABEE69D9E44DA ();
// 0x00000217 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass1_0::<DOMoveX>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m6317E8FB8A070BB91F81C94666F3CDA20A2E43E5 ();
// 0x00000218 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m68B12007BF9F693FE0D2887557688AC886D103D1 ();
// 0x00000219 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass2_0::<DOMoveY>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4DB120064C1A7D8A47A52371A6E532DACA6C73A6 ();
// 0x0000021A System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mA0010CB3454F2432226443E92B116B90D98BF0C8 ();
// 0x0000021B System.Single DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass3_0::<DORotate>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mBCBA84515A05353317F5B564D6A0AC630C3CEE7C ();
// 0x0000021C System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m02FBF2CD2B4A617552D10D38C23B6F31FDE5F534 ();
// 0x0000021D UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m1D63D0BA78EB39F89A1B1E066912FFE44287B21B ();
// 0x0000021E System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mC5F1EA7F156F0624F25EA742FCC3909966F9A7F3 ();
// 0x0000021F System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__2()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mF0D4140DD5E81B476903362C39693F8B8F457FB9 ();
// 0x00000220 UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__3()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9B07E2AACF2C5AE01FF2BF681C2CB7D99007B0BD ();
// 0x00000221 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_mF948C7A5035C6296D75BC18DB7F077EE07D0510C ();
// 0x00000222 System.Void DG.Tweening.DOTweenModulePhysics2D_<>c__DisplayClass4_0::<DOJump>b__5()
extern void U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m7E5499FFA14E6263A35F34565AA865D7F812EF0C ();
// 0x00000223 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m31B504FBEDE581BEA37F6C8D539D8BADB9C8FE04 ();
// 0x00000224 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m19FA6E77EF091166B632107C37CF96158B3CE288 ();
// 0x00000225 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass0_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m44D2693D1A6B416C6C306A7040E5B1F009B6ED3D ();
// 0x00000226 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_mB5AA081132A07D10B45B63C3A7D8996C98FFF09A ();
// 0x00000227 UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m493943ED4559E6518634D0901230AAC5808BFEDD ();
// 0x00000228 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass1_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_mF7579710FC82C1F20388F050DF39FEF2D0CE7EDE ();
// 0x00000229 System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mBBED6FCE860513FB15E971A9E4A57EEAE0B35E55 ();
// 0x0000022A UnityEngine.Color DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mEA5E8386E195FF3054C4AC1F7C46FCFA2E3095F5 ();
// 0x0000022B System.Void DG.Tweening.DOTweenModuleSprite_<>c__DisplayClass3_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mC3182245E5D26849A9001AC53256A5856E4DC908 ();
// 0x0000022C UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_Utils::SwitchToRectTransform(UnityEngine.RectTransform,UnityEngine.RectTransform)
extern void Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270 ();
// 0x0000022D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m18A8356F5FF1FB67DC8B3E62188C603519B3124B ();
// 0x0000022E System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m32C5F6AA875203D30B95F0239833E503B58A4080 ();
// 0x0000022F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass0_0::<DOFade>b__1(System.Single)
extern void U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m35704DDE3B470E932AE2394E444E236EF73D0950 ();
// 0x00000230 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m2F3D328081E4370B02A8DC65A5595FD7779319F7 ();
// 0x00000231 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8 ();
// 0x00000232 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass1_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m17953C82468E26BC336CB919379BEA94DB403CF5 ();
// 0x00000233 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m42ECD7DED23372FD451B099F33986255C1F05F24 ();
// 0x00000234 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9 ();
// 0x00000235 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass2_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m22E508875493477F1B41CE7B93A3327597C7CA64 ();
// 0x00000236 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mE21167957C3A82E6A2212EA06099E19A61C888F5 ();
// 0x00000237 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8 ();
// 0x00000238 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass3_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mF414A2392F00D79E324899FFFD8BCCA32B63113A ();
// 0x00000239 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mBBFBBB66427C51F249BBFF57BB9DBE3738E70DF0 ();
// 0x0000023A UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002 ();
// 0x0000023B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass4_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m62D514A0CBEE93DDB5E11A83FFEF88F70887B006 ();
// 0x0000023C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m9C45A8A17A10C098C69BBAF60D2639D9ED29A978 ();
// 0x0000023D System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__0()
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m2CFEEB05691BF628CD9723B772220FBF1B5DEFC5 ();
// 0x0000023E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass5_0::<DOFillAmount>b__1(System.Single)
extern void U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mC2B9793EB03BCFC06E022CB10D2AE3D4CDC2DC9B ();
// 0x0000023F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m5080623FCA82C0A8E5638EDE08125692828AB793 ();
// 0x00000240 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895 ();
// 0x00000241 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass7_0::<DOFlexibleSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mBFC566A897CBAFAFCC74D5DBCEC136A07F1A5F9D ();
// 0x00000242 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m70D737AA33CF6139F30568CAA4BFAD2E9F1D66BA ();
// 0x00000243 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92 ();
// 0x00000244 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass8_0::<DOMinSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mF51654F8E42E61B6D410192E9C4493BC7B50B7A6 ();
// 0x00000245 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m6054B368E828204FC04AC31B20D57ECE521DA2CE ();
// 0x00000246 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7 ();
// 0x00000247 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass9_0::<DOPreferredSize>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m723F0F3AD18E54882602EF10C41A9138C2094C92 ();
// 0x00000248 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m3C2AC3BA68FE823C5EE4ABF01CF043FB14BC77D4 ();
// 0x00000249 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6 ();
// 0x0000024A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass10_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2A91553D7D78ABD3E8125820476F8026322DD1AD ();
// 0x0000024B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_m922DA2833A1846F6E644BACDA5360882FE3CBD5C ();
// 0x0000024C UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92 ();
// 0x0000024D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass11_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m7C9745929EF3497843A96CE489528E972E1696D2 ();
// 0x0000024E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mDB084173826CEF2F054C58D71259B79FABE1AFB1 ();
// 0x0000024F UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F ();
// 0x00000250 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass12_0::<DOScale>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_mB98E48B7CD64FAEDBF41990DFA5EDFB84BEAA635 ();
// 0x00000251 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mC4153B76DCBFB2268DBBA69D56E679A477FCBE2C ();
// 0x00000252 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78 ();
// 0x00000253 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass13_0::<DOAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mE62D5B3135C3EA0973694EDF23FEFBE0879E0431 ();
// 0x00000254 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m179837AE08CE76F84AD7C2DE1C4536FB2BC6724E ();
// 0x00000255 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769 ();
// 0x00000256 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass14_0::<DOAnchorPosX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mC4EA8586148CB5AE824CA16BE35F0C7E15F13927 ();
// 0x00000257 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mD9D66D4A39054D51E9A3A43A3E47FE76D14E8E2A ();
// 0x00000258 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E ();
// 0x00000259 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass15_0::<DOAnchorPosY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m818E4ABC9402DC49D5DC6F8042472F3D08891D95 ();
// 0x0000025A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m266310E4E7253E8AD0DB494DC917560C7312B6E3 ();
// 0x0000025B UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F ();
// 0x0000025C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass16_0::<DOAnchorPos3D>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mE14500ADC3593F53F54C4D66F17888A87B84344E ();
// 0x0000025D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m6866C080BE78AEC33050A808BD28DD4FF4B7004B ();
// 0x0000025E UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1 ();
// 0x0000025F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass17_0::<DOAnchorPos3DX>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mFB2895AADD1CDF00CFBA835E56A9E13904BAD620 ();
// 0x00000260 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_mCDA7BC09C26B4712D699A1FB257E8ACC7F8B2AA6 ();
// 0x00000261 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6 ();
// 0x00000262 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass18_0::<DOAnchorPos3DY>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mE4A2B4E589B150E943E18A30D33E95B1754E2886 ();
// 0x00000263 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mFDD1B1264FCD4CDDB8A38EA8AD5909CC0FEF92D1 ();
// 0x00000264 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A ();
// 0x00000265 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass19_0::<DOAnchorPos3DZ>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD34C0057C888DD82D38BE7593C1E92FF925914DA ();
// 0x00000266 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m5410B8E63F947DE005A20DE7E85E3CCE2D72079B ();
// 0x00000267 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9 ();
// 0x00000268 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass20_0::<DOAnchorMax>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m5729647091DBBF0D726254ABADF2377ADA0101AD ();
// 0x00000269 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mFB3C178CAA653C95BEE3093D575FEB85F8C7367F ();
// 0x0000026A UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__0()
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5 ();
// 0x0000026B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass21_0::<DOAnchorMin>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m7189572DD336DDE6EC85F27770F731FEEAA2FD50 ();
// 0x0000026C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_m7D1B522F01E0419DDBA693585300D0B01AF984FB ();
// 0x0000026D UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D ();
// 0x0000026E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass22_0::<DOPivot>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m10B5380A3AA5CEAE992081A449EE1F74EC79B15C ();
// 0x0000026F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m75C045C8DEAC70D81B02EAFC1EA366AE0A7AA8F5 ();
// 0x00000270 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45 ();
// 0x00000271 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass23_0::<DOPivotX>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m8EABFE51EA9393CC2EC7F0055F02DED35D3B9186 ();
// 0x00000272 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m7157DCBC4845023E76ECA85BE6C3F81EFCD04239 ();
// 0x00000273 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4 ();
// 0x00000274 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass24_0::<DOPivotY>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m62A33BFDF3873451B14AE3EBA0791AF52D082F15 ();
// 0x00000275 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_mAE01B70E706B6F8A90EA06CCE17FC489F1FC71D2 ();
// 0x00000276 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161 ();
// 0x00000277 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass25_0::<DOSizeDelta>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m3F1A0A60B77F9C9AADC0C9BE1969FD929AEC15AE ();
// 0x00000278 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m143FC1CEE9576076B1C3F954E13D9F709DE22292 ();
// 0x00000279 UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC ();
// 0x0000027A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass26_0::<DOPunchAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m2F2781A19452E8F74222AABE3617555A4CEA2DC4 ();
// 0x0000027B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::.ctor()
extern void U3CU3Ec__DisplayClass27_0__ctor_m3A3C7488BD686D2417811F19408B70D93B0FA02F ();
// 0x0000027C UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F ();
// 0x0000027D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass27_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mED3FDE11B0B31284228C7D6A01C66ADFAD06D05B ();
// 0x0000027E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mC9FC42C7A712D81D3E1FFD069C16C866CF7F3A2C ();
// 0x0000027F UnityEngine.Vector3 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28 ();
// 0x00000280 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass28_0::<DOShakeAnchorPos>b__1(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m560D14C7C7B82350B44D745D7DB7AE161730A086 ();
// 0x00000281 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mC30B5380B8DE2DCAC55DA68241D5EF1266F79909 ();
// 0x00000282 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__0()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850 ();
// 0x00000283 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m5B9935C76D1779CA20BEC5C8434C34BDF9ACA7EF ();
// 0x00000284 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__2()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mFAA2F463E2CED9DE0BA520D44BDC7A64ED56BC6E ();
// 0x00000285 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__3()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449 ();
// 0x00000286 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__4(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m41FE20829CE35CC646EF346A2ED2D42A8F7FFF6D ();
// 0x00000287 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass29_0::<DOJumpAnchorPos>b__5()
extern void U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m46AB05BB46F4343B5372269801C929C6BEA4E78C ();
// 0x00000288 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m8786A94537CDE17C192F816195A8D5095A147611 ();
// 0x00000289 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112 ();
// 0x0000028A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass30_0::<DONormalizedPos>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m34D4384708B00929EC0936A3D9818FBB9320124A ();
// 0x0000028B System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_m430D95F36B8C311BCA555E6C9EB4E3D5B6CB52DD ();
// 0x0000028C System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m4B50B77000E1A5B0959F7EFEC780C43A2B3C21A6 ();
// 0x0000028D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass31_0::<DOHorizontalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mE79549F5C6F07819B8FCA4A3670548B94B794980 ();
// 0x0000028E System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m03AFF4E74AF489641F31A7ED7BB588DC48C86B47 ();
// 0x0000028F System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mDFC279CC6199107052C6C57F7C80069AF08B727E ();
// 0x00000290 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass32_0::<DOVerticalNormalizedPos>b__1(System.Single)
extern void U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m69F269F657FA4BD2821E0451B328802CA74E5E4D ();
// 0x00000291 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m44794063561EE5542E8D44B8047DA0EB476D9D99 ();
// 0x00000292 System.Single DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__0()
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m49CBBBEC21C5C4D3517ED220CE99FECCA32D8E17 ();
// 0x00000293 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass33_0::<DOValue>b__1(System.Single)
extern void U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mC23A71E0D945D05C253DAA3C6B29F017AC3C8B7A ();
// 0x00000294 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::.ctor()
extern void U3CU3Ec__DisplayClass34_0__ctor_m31CDFCCAA9BC664B12E06164FE6FD3F6BCF1CE10 ();
// 0x00000295 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__0()
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE ();
// 0x00000296 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass34_0::<DOColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_mC68427660102A2FD7D41ED52EA99141A48A176E5 ();
// 0x00000297 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m5119796185D9945B3DADE92E11D912791044F5F1 ();
// 0x00000298 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14 ();
// 0x00000299 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass35_0::<DOFade>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m6F73BC7514BC81F58222707B2FEA9F1775452C1B ();
// 0x0000029A System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::.ctor()
extern void U3CU3Ec__DisplayClass36_0__ctor_m266E0D445578A8CF42FF8E6E28F8DD0D5816B6C6 ();
// 0x0000029B System.String DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__0()
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m8450222712B70BE65D184CE038DDA30929396E09 ();
// 0x0000029C System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass36_0::<DOText>b__1(System.String)
extern void U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m5EBCE38D43719324BFF117A085F469D6C7A53A4C ();
// 0x0000029D System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_mF7EE64DF2C6519BFCE36A53802C870392DCA681E ();
// 0x0000029E UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F ();
// 0x0000029F System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass37_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m001CE2BECACB83DE0BB4252DFAEFF93D51676EDF ();
// 0x000002A0 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::.ctor()
extern void U3CU3Ec__DisplayClass38_0__ctor_m029E260AE14F07692FEC152AA854A3E7E818F5A8 ();
// 0x000002A1 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146 ();
// 0x000002A2 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass38_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF9673E803B1ED13D3937EA5093B07828944B0D33 ();
// 0x000002A3 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m2D3EEE4A819B3C837A38B0F228978C5DECA02157 ();
// 0x000002A4 UnityEngine.Color DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4 ();
// 0x000002A5 System.Void DG.Tweening.DOTweenModuleUI_<>c__DisplayClass39_0::<DOBlendableColor>b__1(UnityEngine.Color)
extern void U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m9E000E92EC974F7B26E70A9DB887A7B5ECD13C4A ();
// 0x000002A6 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mCB2FA585275686213140C65F568AF5B895DE515D ();
// 0x000002A7 UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB ();
// 0x000002A8 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass8_0::<DOOffset>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m70E3E83723C4C320D2E78FBB32E3C5AB9E483413 ();
// 0x000002A9 System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_m71E4A32D334F5A1DDFE2DB998E25634EC8C9E0D8 ();
// 0x000002AA UnityEngine.Vector2 DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08 ();
// 0x000002AB System.Void DG.Tweening.DOTweenModuleUnityVersion_<>c__DisplayClass9_0::<DOTiling>b__1(UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mEC4AEA77E8E47513A16390421DFE3814DB87DFBD ();
// 0x000002AC System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForCompletion::get_keepWaiting()
extern void WaitForCompletion_get_keepWaiting_m30EF66A11D003F93F4CBF834A856E7298D5955B0 ();
// 0x000002AD System.Void DG.Tweening.DOTweenCYInstruction_WaitForCompletion::.ctor(DG.Tweening.Tween)
extern void WaitForCompletion__ctor_m69692E16010A98F06E13B8668A2C9AC26E470727 ();
// 0x000002AE System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForRewind::get_keepWaiting()
extern void WaitForRewind_get_keepWaiting_m8E68A912E510F991CC524EA2DCB6634BE7F7DF65 ();
// 0x000002AF System.Void DG.Tweening.DOTweenCYInstruction_WaitForRewind::.ctor(DG.Tweening.Tween)
extern void WaitForRewind__ctor_m111D38D621831BAE9D00CCC7F195DBC48A9483D4 ();
// 0x000002B0 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForKill::get_keepWaiting()
extern void WaitForKill_get_keepWaiting_m410BE82C7AEB9A7E45E5E07EEBDA1D2E2CEB50B4 ();
// 0x000002B1 System.Void DG.Tweening.DOTweenCYInstruction_WaitForKill::.ctor(DG.Tweening.Tween)
extern void WaitForKill__ctor_mD127E54E87A255CD19BB9539CE786DD6953C5719 ();
// 0x000002B2 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::get_keepWaiting()
extern void WaitForElapsedLoops_get_keepWaiting_m6A50A75A89879252500ED70BD15C0C04CF6DC2D7 ();
// 0x000002B3 System.Void DG.Tweening.DOTweenCYInstruction_WaitForElapsedLoops::.ctor(DG.Tweening.Tween,System.Int32)
extern void WaitForElapsedLoops__ctor_mE11D912E8954AB448C21081560A6FBB9D974B7F3 ();
// 0x000002B4 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForPosition::get_keepWaiting()
extern void WaitForPosition_get_keepWaiting_m78A4DAE5D866FA838D2A843113F275E169B03A30 ();
// 0x000002B5 System.Void DG.Tweening.DOTweenCYInstruction_WaitForPosition::.ctor(DG.Tweening.Tween,System.Single)
extern void WaitForPosition__ctor_mB0DA06151DE0C3690CCF8A3117189FA2800CB3FC ();
// 0x000002B6 System.Boolean DG.Tweening.DOTweenCYInstruction_WaitForStart::get_keepWaiting()
extern void WaitForStart_get_keepWaiting_m218C9D65E9141136A148DD636B155272308A9F5E ();
// 0x000002B7 System.Void DG.Tweening.DOTweenCYInstruction_WaitForStart::.ctor(DG.Tweening.Tween)
extern void WaitForStart__ctor_m9088C849D74DBB8D2E12FA05AD5A2E17AA48E77B ();
// 0x000002B8 System.Void DG.Tweening.DOTweenModuleUtils_Physics::SetOrientationOnPath(DG.Tweening.Plugins.Options.PathOptions,DG.Tweening.Tween,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Physics_SetOrientationOnPath_m8BE0D531D59E8E8D9F092FB6C9F881C98BDE429B ();
// 0x000002B9 System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody2D(UnityEngine.Component)
extern void Physics_HasRigidbody2D_mA2B48BE3FC3BB297B8227F0ECA26BAE6B49732A8 ();
// 0x000002BA System.Boolean DG.Tweening.DOTweenModuleUtils_Physics::HasRigidbody(UnityEngine.Component)
extern void Physics_HasRigidbody_m5C3BC90BFBC15B8787E33728CBC9FD4DBAF10DE6 ();
// 0x000002BB DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,DG.Tweening.Plugins.Core.PathCore.Path,DG.Tweening.Plugins.Options.PathOptions> DG.Tweening.DOTweenModuleUtils_Physics::CreateDOTweenPathTween(UnityEngine.MonoBehaviour,System.Boolean,System.Boolean,DG.Tweening.Plugins.Core.PathCore.Path,System.Single,DG.Tweening.PathMode)
extern void Physics_CreateDOTweenPathTween_m5026DCBFA244A145B5076E485DA4643EFA331B42 ();
static Il2CppMethodPointer s_methodPointers[699] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Bomb_Awake_m1871A2508F99AE9459F2D221FC24DA1FAB62A8CD,
	Bomb_OnEnable_mFDDF049AEE907CF72659E5C2B0F2CE6CAF963626,
	Bomb_OnDisable_mF16F07FC94D14254F3A7CFA1D07D838C85245F9D,
	Bomb_Destroy_m0B9F24232D52CB0B0A54E6E4779E7B8AE5961731,
	Bomb_Explode_mC997F2D084663D2CEBBF89C58EF1341330B0272A,
	Bomb_PlayVfx_m37C81E56D30A7193C404CF82B1B4CA229FA2F7B9,
	Bomb__ctor_m168276948FC869A7BA91027858FABF173B76EFD7,
	Bomb_U3CExplodeU3Eb__8_0_mB9059AFD8CE8BA54C5B61F8047BF329FF2433A72,
	BombSpawner_OnEnable_m58F76EEB5EAB4DFA95326CBD7710159AD494E1AB,
	BombSpawner_OnDisable_mA9AFEF6A2D531D0C49F4588D9836B66120855776,
	BombSpawner_Destroy_m7095A5DC7036ECFF10D3B132D698AB3FF2EE1D31,
	BombSpawner_Start_mEEF4FD796E6F125D8FA6BD431C31D046CDBBF2A8,
	BombSpawner_OnLevelUp_mD77BE132A1513F6D31470389BA0CC01B8F4E1188,
	BombSpawner_RestBomb_m297B7C66792EB033E3E951D47C79B9B60FA59585,
	BombSpawner_ShowBomb_mEE720FB9CCDCE4A03C598141F51A09F0DAF36899,
	BombSpawner__ctor_mE300FC2E154A29A155E45AEB87AEF64E847F3747,
	CollisionEventSender_OnCollisionEnter2D_m50C6C8F61004CD2982CB3050A86B1F879AB9F9DF,
	CollisionEventSender_OnTriggerEnter2D_mF5BDD747F0EFF340174C87D7535372FB5834393D,
	CollisionEventSender__ctor_m091C8963FAFDF08FE8863D2DDEA6F31354172807,
	CountDown_OnEnable_m31671BAB6EBFD5655F551DD3442390EC6F71C50D,
	CountDown_Awake_m71BF2CCC7110F3FB236A15B42160B290801CBCEF,
	CountDown_OnDisable_mC08463E83AA77DD1B8306A2087D779C39238EE6B,
	CountDown_Destroy_m4FB44DBF6A73BB67E9DEC52807C78A0BBD50C050,
	CountDown_Update_m6926FE9AF0240BD9DA967D4196B5226CD9310D6B,
	CountDown_OnStartCountDown_m835FE8CD623FDC0721DA5613200ECF6E24199CD0,
	CountDown_onCountDown_m3E373D82E09193C859D3BD29627006CBBB626AC4,
	CountDown__ctor_mAC752453639C81ADEA1772600F23B37BE6CB92F8,
	CountDown_U3CUpdateU3Eb__7_0_m5595728916741855A9745D8FB950499DD52494B1,
	CountDown_U3COnStartCountDownU3Eb__8_0_m6A24F2E2A100657F88470FE3A4E1BE09712B4BBC,
	EventSender_onSendEvent_m19AC0E9199ED02F0127A325F282D1D9DF8843955,
	EventSender__ctor_m42541AEBF045C22F8CCACC48788C2C92E035C8FB,
	GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260,
	GameController_Awake_m0F7A2663599EE488BA03E9010D12DF3D9EF623BE,
	GameController_OnEnable_mBEE9B7EF36A93A7A5F894132B82125FF79DF25B3,
	GameController_OnDisable_m82B20B33A46E9AA09B73EB4207D5262755B809D6,
	GameController_Destroy_m0D53E77855F55548A52B7CD8D90BDBAA30EBE6D3,
	GameController_InitializeGameUI_m2B4A1DEF8AD71C97E996578E3E190B5871D901E5,
	GameController_InitializeLevelData_m9F21D6859D3EB3055B33350B89008E0C7A2AD72F,
	GameController_PopulateLevelData_mA521856D1E39DC0AB18F000E034B45530D644852,
	GameController_ResetGame_mEF25CCAA937DBCDEF25CE02BA08AA7ADA5E47C11,
	GameController_ResetLevel_m5A74E8E43186E5877F8BACED00826E8F921D3068,
	GameController_OnClickedPlay_mD010769ECE4714958A7E4BC3A1B4FB483ADB512A,
	GameController_Playing_m3398723576EE27851C4E79F9B4434F18DE0B6738,
	GameController_LevelUp_m11B9DF1271A342AA3A427135CE4EF37AC8EA61D7,
	GameController_OnCollisionWithSameObject_m77FC37EFF7F54F8D2FFDCFBF231558F4D3325CD9,
	GameController_GameOver_m902C548A5DD5F90EA64A70B00806086D6CEB37F4,
	GameController_HasGiftbox_m48FD038BB7FB93BCB24EC6C1C2BA3FDB01CA3224,
	GameController_OnGiftboxOpened_m20A26BC5F36F698ADCAC426C15F460DCE7AC9431,
	GameController_PlayAgain_m6DA61EBDF5EFCD8532E143B10B8C753CD5A29196,
	GameController_Pause_mB4B24208C38F0D4F7D61553CA988AA6411A9ED08,
	GameController__ctor_m839A9CFB9635B009C1DA139BEAD0E38467E57464,
	GameFlow_Start_mA899CB26EFEAEC2D5F8C7EB63FE38F8BC8573ABC,
	GameFlow_Update_mA8C4D304821E21FC8804ED00B89DA8E13006B7F7,
	GameFlow__ctor_m64C71C093D36011B93F9D1A3C4F509DF3A398826,
	GameOverAdapter_OnEnable_m61B20D2EB7D1769A044DECCEDF4200ABBAF8A142,
	GameOverAdapter_OnDisable_m6604153AEED3ECD75501DEEFDED6D4D7D40306B5,
	GameOverAdapter_Destroy_m12CF85C551CF14E51DEC8FFA79391BD13BAE2BCD,
	GameOverAdapter_OnGameOverShow_mF25B7A0948CE93CB946092E08D8F95B870FB912F,
	GameOverAdapter_OnShowAnimFinished_mF03C61AC85B01BEB9B8F902BEFFD1F5C36A8CFE0,
	GameOverAdapter_OnGameOverHide_mECF9C4267E0A7ED9B7D4FC99131B4AD9EE88B1E3,
	GameOverAdapter_OnHideAnimFinished_mF20D487144070F92327305E8EE8AB609F0BBDF0C,
	GameOverAdapter_OnPlayAgain_mFEF339E32CA0C940466033FDA68F31F57FE2A899,
	GameOverAdapter__ctor_m35D65EAD6A6C50339BD1877E73DC365949F8D92E,
	GameOverAdapter_U3COnGameOverShowU3Eb__7_0_mDCEE3FB117EBB4590A0689D15FF0AE4DBD3E3766,
	GameOverAdapter_U3COnGameOverHideU3Eb__9_0_mA39944598DE9C6FBC460770C39E962507F217733,
	Giftbox_Awake_mC9BD872C12F3782D026B92185EC80F8C25766444,
	Giftbox_OnEnable_m45210D230F061A186B50FA251B44535794B29491,
	Giftbox_OnDisable_m806B7A223695B468DA31A7DD02FDFB9A04949FF4,
	Giftbox_Destroy_mE7B945818C92C8EB57BBC2B3C70A1A1280CF77AD,
	Giftbox_Explode_m41346615A7EC8315FDDBD1E00DF2FCBCF2B9DE59,
	Giftbox_PlayVfx_m78DE09B993C4BDCE407E96C2C78BB7A5667355F8,
	Giftbox__ctor_m8208F54535430BF8889162BB812F60C3A63ED984,
	Giftbox_U3CExplodeU3Eb__8_0_m27C785A2CABF099C717CC9D92A3E2114EFB1EB99,
	GiftboxAdapter_OnEnable_m1C32282370A08A3E8374F2DD9379B48FA75A294C,
	GiftboxAdapter_OnDestroy_m137B3A7822C8AC9AF3F2498C590DA7B438661AFF,
	GiftboxAdapter_OnDisable_m73AF9C8D1236B0983B759FDC21EBDADEF86F37E2,
	GiftboxAdapter_OnShowGiftbox_m057086E330A4EF83DA1C389C970C9C37FF32E375,
	GiftboxAdapter_OnShowDone_m2DD77B01BBD5B77B25F974BFC8478B4CF4130028,
	GiftboxAdapter_PlayAnimation_mB51EF0A81B31AA59EE3371657854818A9BB74D09,
	GiftboxAdapter_OnPlayAgain_m41B5677E42AF1B12887DCC99D144C984E75D9FF9,
	GiftboxAdapter_OnHideDone_m1D36FECF732C2739F66E28DECC19E2C7A7214DE8,
	GiftboxAdapter__ctor_m9EBC7A946E98CD6DE49F869674FB77828091FBAA,
	GiftboxAdapter_U3COnShowGiftboxU3Eb__7_0_m129453A9165FEB7BA16F85B6E76317FFE47CF913,
	GiftboxAdapter_U3COnPlayAgainU3Eb__10_0_mD3F4BD4ECDEC5790272C66116B73F5F0BCE43427,
	GiftboxAnimController_OnGiftboxOpen_m73F98F6355B5EBC526A2B928F6615FC98C7889EC,
	GiftboxAnimController__ctor_m62B2504289DC8C9182190BDC9AECB909E0211CE7,
	GiftboxSpawner_OnEnable_m8ED48FEB1E5C6C8E5C6BD19E15CF6175DDF67D0D,
	GiftboxSpawner_OnDisable_m894A2924CEF3EB938276894711BC88D7E091E2A2,
	GiftboxSpawner_Destroy_m097521601779A280F011A23CB1EF978F6F1EED84,
	GiftboxSpawner_Start_mDDF44B2C47E80562D99346B0E47D12CEC8838FBD,
	GiftboxSpawner_OnLevelUp_m33B8194C6F5E66E021F0EA72FFB6CEAC8865CB7F,
	GiftboxSpawner_ResetGiftbox_mF5DAD423BCEAF08D1D22A06C2A3EDC7F834C42FE,
	GiftboxSpawner_ShowGiftbox_m4CCEBB85A9E876F37861BC45AABE73036AC4174E,
	GiftboxSpawner__ctor_m62E1A6372FCADB968F1B1C659537A009215272C0,
	HudController_Start_m5FE57FFC9DA2F8C2BFA6D2E256002DC69CA2A5F2,
	HudController_tempChecker_m6AEB042BB7CBDF53CF40316D99EBD8582C55A765,
	HudController__ctor_m6F98E6BFB55B45B88A369616792A4A28C1C69468,
	KnifeCollisionAdapter_OnEnable_mAD449D64AACA564373D1C7C4ECB8D5524651A81E,
	KnifeCollisionAdapter_OnDisable_m1B68E0FE3711E99966726269CB1AA21CFB2E7887,
	KnifeCollisionAdapter_Awake_mA64C4040DA63F421865AE9A46DFDCBF1F2F90598,
	KnifeCollisionAdapter_Update_mC5C1FA055367A1A7869396A1D197BC6F4F566042,
	KnifeCollisionAdapter_OnCollisionEnter2D_m395C6284DBA5B8904E36C10ED8DBB7D2004E47A3,
	KnifeCollisionAdapter_ResetObjectData_m1E35748C19942D1907E281324D0556A2B345465D,
	KnifeCollisionAdapter__ctor_m4D19C9284948D82A5E8EF17AE86AD50853CE2ACD,
	LevelAdapter_OnEnable_mCFC251B22019AAC984F4520B184C687D3D53D3C8,
	LevelAdapter_Awake_m979C8882060DD183D14C284E82FA51C9E6E2B3A5,
	LevelAdapter_OnDisable_m3918646A9E04689180DE65A35B5167452F1D4F2F,
	LevelAdapter_Destroy_m06207A298080E7D8EA2D92F9BD0B7B0BA0C3A338,
	LevelAdapter_UpdateLevel_mFAC90A0E015E2A716025D0B41C709E06ED0B954B,
	LevelAdapter_OnInitializeLevel_m9D96796E9D0448F676FA1645A584AFACC73F37E8,
	LevelAdapter_OnLevelUp_m2859F1DED36666F4F1E27990987D492DAFD32E93,
	LevelAdapter__ctor_mE8DCE26C277D780F688A14A099F74CC3BC606385,
	ObjectRotation_Awake_m10F6A8E16522E5C6771172FB63ECC10BF85D0001,
	ObjectRotation_OnEnable_mEE3B8203BD701A3113264BEEC3F8CF6376BE6B6A,
	ObjectRotation_OnDisable_m9FEC2CC5A4479FB24DD1E5A35F1334D7FF7EC2C2,
	ObjectRotation_Destroy_m2CDC8A5EA540F0B6086D598A16ED65C839295D56,
	ObjectRotation_InitializeRotation_mA33BB1810E53A8834E9E205578E7536BBAE02C68,
	ObjectRotation_StartRotation_m846A5CE791C63F80E37609EDB4FCFCA2A81F3149,
	ObjectRotation_StopRotation_m0CBCCFFB5D1D401CA0E0F49F40F49A5D852D6FEE,
	ObjectRotation_OnGameOver_m69FAA1033081771819BFD921B72D251E5DFAB924,
	ObjectRotation__ctor_mCF31CD9B1DBDAE89C8591004917C3413348B9CE8,
	ObjectSpawner_Awake_m24368C54B5CFF7B69B312D4E005AF0307E5FEA53,
	ObjectSpawner_OnEnable_m906149C0704DF14D48F204C0A36CD1BB807CA105,
	ObjectSpawner_Start_m9E9806FBFDF0D7443C886E6D3B429CFF2B70B2C2,
	ObjectSpawner_SpawnGameObject_mA301986D0B43BB3407E92CC2623513E438A42C1E,
	ObjectSpawner_DespawnGameobject_mC093504EABC7F2130EB0E2B7B1F9DE7937A6981E,
	ObjectSpawner_DespawnGameobject_m9F9B3D79A941AAD539DC5E60FCF7A5C0728EBFE5,
	ObjectSpawner_DespawnAll_m41D675E384010AC38EE710B282B790BDFFF1276C,
	ObjectSpawner__ctor_m37C806347D6F834F55235541B95E441FCC6A0C6E,
	OverlayAdapter_OnEnable_mB6C36227A730F8879F1A0F5B31C07BE20D4FBA5E,
	OverlayAdapter_Awake_mD801FE3812595FD5B26377A57FAD6870CDFF1CDB,
	OverlayAdapter_OnDisable_m4BFC0C940506F21885CD296A132ADA440FFEBC9A,
	OverlayAdapter_Destroy_mFD9A80902BD322FCC1A6DE500D4C147B4C091B65,
	OverlayAdapter_OnHideOverlay_m25878225CBC1F1C0A1710E9EC95A61AA47AF3A29,
	OverlayAdapter_OnShowOverlay_mE68F13F1738CDC724FB44A28E14CAFD142250E3F,
	OverlayAdapter__ctor_mE75BF2A5B416F53EC0181692CC2FCE500E14A87F,
	PlayAdapter_OnEnable_m24D8499589071E5E289596482DE2995014728A6D,
	PlayAdapter_Awake_m2BCEF0C8C32ECFE2F491697E7F65AB17C359DF8B,
	PlayAdapter_OnDisable_mCB7369C6BF89438DBE1866F67135F2C39A0A19A3,
	PlayAdapter_Destroy_m2E6DB281B5C9B6C4A598B6CCF832659B0AC1CBAF,
	PlayAdapter_OnShowPlayButton_m78A9B16C74AD5382CFAE69E068326DEA40990E60,
	PlayAdapter_OnHidePlay_m2497A9CED229B9D0C1D9989E83F5AB76D3E9D045,
	PlayAdapter_OnPLay_mBE36A58F51071B415A2D96A4E74614CF739C92A6,
	PlayAdapter__ctor_mC294EE8C8466D0930C4A913A2E3926E1C3AA80C6,
	PlayAdapter_U3COnHidePlayU3Eb__10_0_mB10EFC20B252AB96507C39299B711B56540068C8,
	PlayAgainAdapter_Awake_mB54DFCF4EFB38C61336AF4B3CB4DE1FBE6FD6928,
	PlayAgainAdapter_OnEnable_m5CC5F1BD08E176F27921D6DD066FF21DAA4ADC3E,
	PlayAgainAdapter_OnDisable_m9034EE25962CD28E6D75D4597760BC7DA03500BA,
	PlayAgainAdapter_OnDestroy_m6CBBC3EC4E6DED1626195D72A90AC9AFE4473BE8,
	PlayAgainAdapter_OnShowPlayAgain_mF24CF10D4BE9347B49AD5490CEC63679F9A9D135,
	PlayAgainAdapter_OnShowDone_m2FEEFF66D83AED02E66F4D0EC88D179B11E32093,
	PlayAgainAdapter_OnHide_m088D138EA451338986EFBB5F67AF4BD6B830CF03,
	PlayAgainAdapter_onHodeDone_m14F18B577772092FDB3BC6928692CC3A7388D9E9,
	PlayAgainAdapter_PlayAgain_m0900DB1C365367AD0753B92C7E27A7F9D89DB068,
	PlayAgainAdapter__ctor_mBA49D442B2D5B458A9525DDF44DBD7B97F3F650F,
	PlayAgainAdapter_U3COnShowPlayAgainU3Eb__7_0_m76E9E1FE8BBDAD7EE7880D0AC53B289C115DB3EE,
	PlayAgainAdapter_U3COnHideU3Eb__9_0_mE8DA0045B8DE87D1B7C8A6B0797C4963B6C55A01,
	Score_get_Instance_m439146EDC19D68D79ED5BF39A4FD794FAB3E1A01,
	Score_set_Instance_m8866BA5DA4C1E9A562E94E40C5E3CCD1C0B2852E,
	Score_Awake_m481CF8ED9C2A4383D06EEEEE40EA8A0D6D4E85AA,
	Score_OnEnable_m2A91D9BA7577AA26EAB7A552EAC70519D27D3C73,
	Score_OnDisable_m103F900910779688AAFF297606EFFBA37ED8851C,
	Score_Destroy_m69193D269D15C985896A6A56467A18276CC05C21,
	Score_get_ScoreValue_m822B6EFA0A9B250A38D90D517D71E81711958DB0,
	Score_Start_m1821AC2C6C0505E9EEFA5DD52733BE5C3037AD9E,
	Score_AddScore_mA192249C9863A59333D2046823539B564EECA29B,
	Score_ResetScore_mBB6FC67BBB10F4958FF57CCFA276E3B365D3C9B0,
	Score_OnShowScore_m776FE3AAA2F29564DC0BC54CA4D57AE57358DB28,
	Score__ctor_mE2F9D741565800994F2DFAF0C4036F5E7ADA4D1F,
	Level_get_Score_mE465427199E91854BF31F6621777EF82065CF8D9,
	Level_get_StickSprite_mE373850BEBD58058FE3362C5979ADE0194E4EB65,
	Level_get_ObjectCount_mC9B7EC462ADFD82F060F01462F7BC3DC15011A15,
	Level_get_RotatingSprite_m6D628B0174976A0EEC43F92B280AC7B43B037657,
	Level_get_RotationSpeed_m1059ED3728D41F342942E52E5FED998177D1EF09,
	Level_get_RotationDuration_mE38F35F23C9742DFBDCE903300742F584087F7B6,
	Level_get_ShowGiftBox_m8D6E103DC4C8036E53E3B6112CC7E7FC652063D5,
	Level_get_ShowBomb_m276D85440A3028E2A7924B96B6F47A7B25FD641F,
	Level_get_NumberOfBomb_mCBB4CF3E07275AF86F1CDC1FBA4B3FAE29775F4B,
	Level_get_NumberOfGiftbox_m079FA9943B3BFAD750A87A1D27454F4883530B2B,
	Level__ctor_m7BAC6CA179FB0475A672991C18A04BC93712F4ED,
	Levels__ctor_m71996D6A3C681A73B1FCDC87D15AC87A2DDCB7AC,
	Spawner_Spawn_mFCA22A3A7C3F13BAEB54F0D4B2AE47767567623C,
	Spawner_SpawnAll_m3F2B25BEC54AC571796D9CA2A8BE1B3678490C6B,
	Spawner_DespawnGameObject_mA7B74EA001CC03EFC12758578F3AD30DED48FCD2,
	Spawner_DespawnAll_mC3431725EE43767357684B0200EFC1EECA0DC619,
	Spawner_Despawn_m457F724CCCB86DC3D2B90424CEB185BE43D96CFA,
	Spawner_Despawn_m505D7FBF55A8D47D5134ED17AA6F286F755759D9,
	Spawner__ctor_mA37BC0F6624E147B76FC192F6E53162998BCA0A5,
	StickPreviewSpawner_Awake_mDEFB97F229EF73245BFDE75B32AFF9E705071933,
	StickPreviewSpawner_OnEnable_m2FDEC0217872F4B9EC0BA07E9E02B268CC63C669,
	StickPreviewSpawner_OnDisable_m46401F78BEE6FBA11631D4F01404ABB226FC8855,
	StickPreviewSpawner_Destroy_m510D6F5D305A491111E6C2494A252C28D516F413,
	StickPreviewSpawner_Start_m7170740E6E965ED8B2CA85609DD6EE2217329CC2,
	StickPreviewSpawner_InitializePreview_m7A58F4DBBD494017628AA40942511C44BFE996E3,
	StickPreviewSpawner_ResetPreview_mBBB6CFA7197046262F41BCE1240E3C3E15791147,
	StickPreviewSpawner_SpawnPreview_m333A0BB118050429A99B61177838C2FC347E7BA9,
	StickPreviewSpawner_DespawnObject_mEBD17A5156B5307C728469E7DD7C40F29A20D4FF,
	StickPreviewSpawner_OnLevelUp_m1F134AD5828A575F11580AEAE3E3C21843967382,
	StickPreviewSpawner_OnGameOver_m4F9829D7A2AAE37A1E6940D5D99FDC2A10A3D085,
	StickPreviewSpawner__ctor_mE5EC46757F61DF2CA1600BC2D0277D6E564DB33A,
	StickSpawnerController_Awake_m8A2D63B8E0FA427613448292E3EA8C2802BDB12E,
	StickSpawnerController_OnEnable_mE9D04035778E0C8710CA867B3FF9A146492BC0D6,
	StickSpawnerController_OnDisable_m28F26EDBF34053A8C23CA5A5AE63E431DACDF3A3,
	StickSpawnerController_Destroy_m41F4186E3F6D8038284E4E6073F0E339B815166B,
	StickSpawnerController_SpawnStick_mEFC59DBB0AA159E9042DB2AFCE99664D3A5C37E0,
	StickSpawnerController_ResetStickValue_mDFB37286D63524B52924F2FDF021715C6D3A9627,
	StickSpawnerController_InitializeStick_m8CA884EE10A1E7451667D89216134284CDBB13C0,
	StickSpawnerController_OnLevelUp_m6162E75E3DC8790182E5B8D5B02204DFC351F7B2,
	StickSpawnerController_OnGameOver_m53776CDFDC4A824AB99B564056CA632A8D327BA5,
	StickSpawnerController__ctor_m9B4666F424C3C8C8EE8DFD42DFEEA28164071F95,
	Timer_Awake_mDBBA7C85E96EDCBE97C62856B37EB1C3A640554F,
	Timer_OnEnable_m3528CB33DE1F38EDE73BF26C9AFE3A4C2B449728,
	Timer_OnDisable_mE3BBA6A32D085B9C5CCC04ED0C36E99BCD33E443,
	Timer_Destroy_m6CF3B81FA52B7074071BB977EED750F4FA068D1E,
	Timer_Start_mEFDCF17412717C132D11A41F14C8F9E96A4B78E4,
	Timer_Update_mDC9C24926E911E84E153DA4BFCC1689E82D07566,
	Timer_DeductTime_m5C15BF2417742A6601F3D21A4978BE726D416085,
	Timer_ResetTimer_m3C539ACD37BC8733D3DBCF702BEB00C1C2EFEA14,
	Timer_SetTimerDisplayValue_m1390495D4996C4CEE5C222F2D3A3FD024F720E93,
	Timer_OnInitializeTimer_m2B1BABB6BCFA242738B509303FB540F8606AD5F5,
	Timer__ctor_m05F02A43855C43EEF0FE3171A63A895A95D9322C,
	Timer_U3COnInitializeTimerU3Eb__16_0_mE2BEB80520227E11F97A155E6C1972DB620F7C42,
	TitleAdapter_OnEnable_mA1AF3760E2D50C46B68672DD2333248799E7A7CD,
	TitleAdapter_Awake_m4D3D30CEFCAFA963FDD27027EE7BC74DDAD8B6F9,
	TitleAdapter_OnDisable_m0D3C4D4022F7DD94AE4F22DA4E0D563524F96612,
	TitleAdapter_Destroy_mE498EBB70349645DD6EF6A029EDDE6639BE4090A,
	TitleAdapter_OnPlay_m24D71D825BB406C9B780F9208AE09D52AB29B361,
	TitleAdapter__ctor_m1E2374792E4B46CE8ABAAE833FE8DD6D70397678,
	TitleAdapter_U3COnPlayU3Eb__6_0_mB213EA7140D3DC1ABE33AE60E81E81BCE509B4EA,
	MessageAdapter_OnMouseDown_m72FE753ED5E52D5FBFB7C335FD73D65982CCBAA1,
	MessageAdapter__ctor_m4EB5A62CB7F18947835DE7019DF888352E3A4A65,
	MessageTestHandler_OnEnable_m2E47AB4795ADD9C845AB943D33D7BC9A3715C54D,
	MessageTestHandler_Awake_mBB8519C50A6265C9451398CE5B0D2F6839B4C191,
	MessageTestHandler_OnDisable_m8044EE6BADFEC27CC783C7F9442E9C2B78774FFB,
	MessageTestHandler_Destroy_mA66E042933400BCC0F3AA154EA844B41AE2FAD91,
	MessageTestHandler_OnReceivedReactNativeMessage_m579776235CABC0AF77FB51CB639FBA8DADF76764,
	MessageTestHandler_SendMessageToReactNative_mE0C241CAC952C4FCF3FC384AFF0076B27D9C1511,
	MessageTestHandler_VerificationMessage_m1F7E7EEEB402333FF89F9F82E65F97D68BA9E63D,
	MessageTestHandler_Verified_m3217C0BDC199A1A471541CF6E37549F81B383028,
	MessageTestHandler_QuitGame_m4D0A49391B042E390B76BBDF91365772D8191AFC,
	MessageTestHandler_OnQuitUnity_m462BAF7A1E88E4C5CDB7813B5BA818CFCE18C3BD,
	MessageTestHandler__ctor_mA305B3C314B7ECE70F64756228FD1A36EA7953F5,
	MessageTestHandler_U3CSendMessageToReactNativeU3Eb__9_0_mD0B69CD43F8B07FB9882EECAD97A5B06809A2006,
	MessageTestHandler_U3CVerificationMessageU3Eb__10_0_m3CA40F7B81EE14688F9E6CA3D84BD1BA45142E51,
	JsonNetSample_Start_mC580188F2BDC0DA97BA536FCC61C2972D0215D42,
	JsonNetSample_WriteLine_mCFF21EB23B15C700A84D0C8FB80117AF81252AC3,
	JsonNetSample_TestJson_mA1E9139B73CDBBEF5EB43E0C23ADD53C97B3A836,
	JsonNetSample_SerailizeJson_mC1C78EAC1B88AE45BF34C98DFCE601ACE3E18984,
	JsonNetSample_DeserializeJson_m25A0086832CAD4216DEFABA6332DD9B6F4E7C30A,
	JsonNetSample_LinqToJson_mACABD21931966F2DFA717B3AF9552D91B4436EF7,
	JsonNetSample_JsonPath_mEBA2D157E494E22CC74857C90B9CB7982B655B0C,
	JsonNetSample__ctor_m5C88D5BD2710CF7D33C50DC06DC562CB17FACE46,
	Rotate_Awake_m98272D8BD36400F05C40890A9F7E6AAA35AB7515,
	Rotate_onDestroy_m36332C66DECAA5FF23044FB5D6873B7E38AA129A,
	Rotate_onMessage_m2EB175A859194F332EBA43D1F532207B84EA915C,
	Rotate_OnMouseDown_mA2F15CCF41D63C626D70DA7338B4146EC13B9988,
	Rotate_Update_m7EECA0DFF07C6BCA773478F66A949EE038D28330,
	Rotate__ctor_mC70374AA84BC2CA04CA039D1B56F5AF1B95D42CF,
	MessageHandler_Deserialize_mA9693BA8D2C9FD955FEA9DD8E48D55E6584BB8DE,
	NULL,
	MessageHandler__ctor_mD413687341FACD003432FE926B76CC1DE2E78EFC,
	MessageHandler_send_m0BF4951456C3CA50EEF785A982A6F10D92BD0C5E,
	UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45,
	UnityMessageManager_onUnityMessage_mC3031C4585567E720F536DB4EE28B0C20AB8A7C4,
	UnityMessageManager_generateId_m39045BEBA86C4467AD5549DFA0DDCDF9A7A9CB89,
	UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16,
	UnityMessageManager_set_Instance_m25A5E4940172A7950BA2EED339C161A0C74B05AE,
	UnityMessageManager_add_OnMessage_m7B623FDD1EB187BC9593C933D5E01766FC8DB344,
	UnityMessageManager_remove_OnMessage_mE747C20C6838E183F2C60960D2C697D7997588F4,
	UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA,
	UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E,
	UnityMessageManager__cctor_mDD0387729BD393CF289CB737BC70BF82CFAC885B,
	UnityMessageManager_Awake_mCBF7274D38E90447257210355EEBED184FF0C035,
	UnityMessageManager_SendMessageToRN_m7FA689191F69A7BD1CAF2218D61EE8BCE156B250,
	UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E,
	UnityMessageManager_onMessage_mD1704644A461EFD311809BB58F82D36898713485,
	UnityMessageManager_onRNMessage_m47BA244DB7A9E9A2486007E2F5217A03FFFC1AF8,
	UnityMessageManager__ctor_mFB3651A2237E489ED8746D5B8FC5B5AB95FE7CE7,
	Verification_Awake_m4CD0EE2E0009E58A617FA08FA6D449DF1B2E226D,
	Verification_OnEnable_mE50987A371FD54F5292E20F154AC2A490D011107,
	Verification_OnDisable_m18806AEBCF408EB9266CE92A75C8F7F9407BF21B,
	Verification_Destroy_m8607EFAAF4C6E7D28461C5B10AFED955AD49C446,
	Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534,
	Verification_Start_m724EA6CB3B87E138FBBF99835797BCB0BAE3EAB9,
	Verification_VerificationMessage_m3C94AE37F59AB2F5AF0B18A9D95BAD98B6188E1F,
	Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC,
	Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C,
	Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309,
	Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318,
	Verification__ctor_mF7CEB04FD552F04F0FEE9512769E3571FE91E663,
	Verification_U3CVerificationMessageU3Eb__11_0_mF013FC22CF55ECA6FE3AF190613E520947622178,
	DOTweenModuleAudio_DOFade_m39CB8EF508F3CF05B29E84981A2A88CC5956D1A0,
	DOTweenModuleAudio_DOPitch_m7F93B8BD0AC634A3F9C4744C0C6966929194C74B,
	DOTweenModuleAudio_DOSetFloat_m935C7ABBCB182FB62B3DE35EEB19B9475827C175,
	DOTweenModuleAudio_DOComplete_m99FA15157545779778E7635788E648CA14A5F298,
	DOTweenModuleAudio_DOKill_m7D939E2BEF59494626A19583D90F10E00E405AC5,
	DOTweenModuleAudio_DOFlip_mB770E32FEA8C7992DD40A29014AAB7D9D8E869E9,
	DOTweenModuleAudio_DOGoto_m877E17C8F971BDAD1BC2C766068C4976B553716F,
	DOTweenModuleAudio_DOPause_m88B2EF1067BAC84AFA75245DC89A73A2E6D26AC6,
	DOTweenModuleAudio_DOPlay_m3614FB2415D70C906BFE9B0C7E32C8DA26270936,
	DOTweenModuleAudio_DOPlayBackwards_m97A8444C91B77C69D57581350BCFF78A0DA9BEDA,
	DOTweenModuleAudio_DOPlayForward_m4C6F66E6B3DD5B2A09518BA9F89659A4F7F23F27,
	DOTweenModuleAudio_DORestart_m0E909925A1AEDCC87B9D39EF62D241BB61531A77,
	DOTweenModuleAudio_DORewind_m537136DC6FB152AD3B3C4789E5B48951168EE41B,
	DOTweenModuleAudio_DOSmoothRewind_mC4E761C3D34EDA9F3FDE8AD0E088560ECF7D0984,
	DOTweenModuleAudio_DOTogglePause_mD4E4C4AF868EE5F42E2B258809C4461FE0ED1A20,
	DOTweenModulePhysics_DOMove_mA271F3426E3EF7AA8DCF9CA457B4CA7943879AAB,
	DOTweenModulePhysics_DOMoveX_mF6099EC3DBFDE8E813B1D41701221024593E21AC,
	DOTweenModulePhysics_DOMoveY_mD323825CCFD8229556C4FF55A2B86ADC7DADBC74,
	DOTweenModulePhysics_DOMoveZ_mC77389BF46767B1F72DABC8201AE84B727253279,
	DOTweenModulePhysics_DORotate_mFD933B3DF2A7767A345F1335644AAFE1B078D7B7,
	DOTweenModulePhysics_DOLookAt_m231F5DC2C8DD72117C4CB668B146EC17A66E6069,
	DOTweenModulePhysics_DOJump_m45253F124D36FB2A26B5DE8B5EEA2CCCDE773B15,
	DOTweenModulePhysics_DOPath_m798F6F69BB6E19915A2DC9D234A6972DFED348D0,
	DOTweenModulePhysics_DOLocalPath_mFB9AEB7FBA37EB06B7D48287309430C4E54B7343,
	DOTweenModulePhysics_DOPath_m195A56635B845275DEBC57F3847100DCC05A8821,
	DOTweenModulePhysics_DOLocalPath_m89AEF519ECCA560058C4149C448FC3D10CCA5078,
	DOTweenModulePhysics2D_DOMove_mCD3E097FA2B1499BF713027E0AAEBFFA94770D71,
	DOTweenModulePhysics2D_DOMoveX_m483FD0BD2030141F8809A812C1640C66C7E9B9E2,
	DOTweenModulePhysics2D_DOMoveY_mC8760E855D826585531D9CBB492E6AA5B39A52AC,
	DOTweenModulePhysics2D_DORotate_m0176C1D02F44023C8C404A82930656D153C87126,
	DOTweenModulePhysics2D_DOJump_mDADF6CC5B3A4C0664CB346FB97566158311E51B2,
	DOTweenModuleSprite_DOColor_mF881B290086523415B55BF72B83146541C2732D7,
	DOTweenModuleSprite_DOFade_m99BCBC9FD737A6D3354ABFE7C8A2EC98EE0D7AA8,
	DOTweenModuleSprite_DOGradientColor_m57175DC68A81F1142793E83BC4DFD3C0E7457AB8,
	DOTweenModuleSprite_DOBlendableColor_m57100BAC9DF79F3179A0F17A685DA553728586CC,
	DOTweenModuleUI_DOFade_mAD40AF255234B54E5BA8F81219C9C30E520EFB10,
	DOTweenModuleUI_DOColor_m224983D24DA5CC099E78D8FA2C4FC159F6385CA6,
	DOTweenModuleUI_DOFade_mD3FD501A6776914AE93FF9B9722A9EAD4B2DBAF9,
	DOTweenModuleUI_DOColor_m60BCBDB46E4B4E67BE9D9984CAEDA8065AC3F050,
	DOTweenModuleUI_DOFade_mF285AA1CEFC18A119A640DFEC3FC8216E92C7407,
	DOTweenModuleUI_DOFillAmount_mEFF5F991C07ADB2611F22051613B7ACEF66B9E4A,
	DOTweenModuleUI_DOGradientColor_m6CC119CCB3273C03076FCBDAFB9E0CBD169D2D0B,
	DOTweenModuleUI_DOFlexibleSize_mFDE51E1609ADFE1E5D1161ED9B2BB55B140DCEEE,
	DOTweenModuleUI_DOMinSize_m0BF4109021D52DE8577A8FADFC069409C0BEC366,
	DOTweenModuleUI_DOPreferredSize_mFCEDB7320442DCD6D7853C62F049BDA0185EF278,
	DOTweenModuleUI_DOColor_mB023CF7E6FFCFC847151C5AAA0CAEA00C80046E1,
	DOTweenModuleUI_DOFade_m4DB434C44B5B2906B99B8926B6FDA2CADEF8A7B7,
	DOTweenModuleUI_DOScale_m9E98A3E1A4F40F42336B7A83EDE00B4EFA25C4B5,
	DOTweenModuleUI_DOAnchorPos_mCB314AB88AABF23417E5671CB1CC2C35591920AD,
	DOTweenModuleUI_DOAnchorPosX_mC9A3B16335FD5A5157688FF499FE782E5F5C7A33,
	DOTweenModuleUI_DOAnchorPosY_m839C67C3E8C54701B7FC5EAB46B911C0DF192E97,
	DOTweenModuleUI_DOAnchorPos3D_m595AA5252C74166C89DFC53A9052E4DCCEAF1921,
	DOTweenModuleUI_DOAnchorPos3DX_m83F31312715DF4E65F5968115CEDB185A4648B8F,
	DOTweenModuleUI_DOAnchorPos3DY_mB024133ED57232447FF4EB17C861AD70A44F304A,
	DOTweenModuleUI_DOAnchorPos3DZ_m56234AB8B466C82A5488AFAE36F7153B82BE2CB9,
	DOTweenModuleUI_DOAnchorMax_m789E726166F500902973D121DE47E178BF772AB7,
	DOTweenModuleUI_DOAnchorMin_m3E6515C288D04F55674513677FE209E2727448BA,
	DOTweenModuleUI_DOPivot_m080B5E1942554C274704FE5E024EA9CDF6909492,
	DOTweenModuleUI_DOPivotX_mF68B3F4FE5D0CAAA9E1203C6A3BE2557B2365688,
	DOTweenModuleUI_DOPivotY_m2A449ECB26A50F9CA43F852CACE4B876A5094CAC,
	DOTweenModuleUI_DOSizeDelta_m1E034E8BDB38D7F4F563AB9523891D7F215AA8CF,
	DOTweenModuleUI_DOPunchAnchorPos_mF564424EB231C6B1EC4262BD235C6F4C7B954EAE,
	DOTweenModuleUI_DOShakeAnchorPos_m19C5739A636820F747EA76567409F0773A8C7CB8,
	DOTweenModuleUI_DOShakeAnchorPos_m14E79F46BF244846CD0C57D4F0ECCF44943875AE,
	DOTweenModuleUI_DOJumpAnchorPos_mF579D5EC1A7F51586A2C0409B34CC105BF480F41,
	DOTweenModuleUI_DONormalizedPos_mAF52FC4BCBDE295E6A2F3C297188469D7BE4DF56,
	DOTweenModuleUI_DOHorizontalNormalizedPos_m4DC3AE19F8B0437E7D26930A2897CC479161A134,
	DOTweenModuleUI_DOVerticalNormalizedPos_m0B870F709BF1790B6DB4C8F05A6B7ECE8CB1D231,
	DOTweenModuleUI_DOValue_m6A01D22DCB8142450DCC0C43046C2F0D5C39D00E,
	DOTweenModuleUI_DOColor_m981384A20E9E9EA3BF1B105E1850D8DCFA1D1F60,
	DOTweenModuleUI_DOFade_m0A792EC1128D0C1C77D39B0E859CFFB45A51E479,
	DOTweenModuleUI_DOText_m811BBD8FA9215A129160FC0A7D746E1A3741465F,
	DOTweenModuleUI_DOBlendableColor_m2F817AE8F4922B274AF2B1E7BFDD0E5D7C623BFC,
	DOTweenModuleUI_DOBlendableColor_m140B9C6CD1E6402158382C49918DB303E39369B7,
	DOTweenModuleUI_DOBlendableColor_m8666A89B76B8D202DC81C1AB12D8B45688212D51,
	DOTweenModuleUnityVersion_DOGradientColor_mD3871F90FCE6FE5B23E23FE12F7826E44F8A05CB,
	DOTweenModuleUnityVersion_DOGradientColor_m1AFB6E6AED804D3261D124D2D1429134B34AFD1E,
	DOTweenModuleUnityVersion_WaitForCompletion_m59773AF9A35804797C6C389A516DA63A0DBAC246,
	DOTweenModuleUnityVersion_WaitForRewind_mC6F53B8F61682B52F390434AFCEC775A4EB49F53,
	DOTweenModuleUnityVersion_WaitForKill_m871BB15AC97A221A77DEE06B90AD1EB1DD97667D,
	DOTweenModuleUnityVersion_WaitForElapsedLoops_m10EBF8473875B14ADA1C065043BE47B9C3DE99D4,
	DOTweenModuleUnityVersion_WaitForPosition_mF7210B505549168AF420CC3453D42DC85ACFE977,
	DOTweenModuleUnityVersion_WaitForStart_mC3E5A8737A87F6DD8ED2380957AE33B786DA1401,
	DOTweenModuleUnityVersion_DOOffset_m2A623BF72AA0289033B412F4D93DF5317FF620CF,
	DOTweenModuleUnityVersion_DOTiling_mE9A75D5B8CFD751EDCB4231A9FBB2EC003752D97,
	DOTweenModuleUtils_Init_mB7D252D24842502558B1DF4DEAC5B08887202DBC,
	DOTweenModuleUtils_Preserver_mE88FFEE9E520275AC0F42ACBE27D7F2265E33DBB,
	OnSendEventMessage__ctor_mC2220D53157F8BECFCC66A9CF407D4D4117252A5,
	OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390,
	OnPlayAgain_Invoke_m2EF6E655A0D3B20EAF97B230803551E1B6395B30,
	OnPlayAgain_BeginInvoke_m11EE5A9E940AA9821321221D52946B82DB372667,
	OnPlayAgain_EndInvoke_m9061547C49BDECC0A6DD5017164D0EDC2B8546DC,
	OnInitializeGameUI__ctor_m2532572CC7CE1285668873571CDCAD597FF3F12C,
	OnInitializeGameUI_Invoke_mB58EB7A06A34732F4EDC0E0DF27F4EA5409F0FE4,
	OnInitializeGameUI_BeginInvoke_mAF66EEFCFC1A29264212C5AE7E836195992AA505,
	OnInitializeGameUI_EndInvoke_mF6628DDE27B6A05D06358E300AF108CB3AFD1320,
	OnGameStart__ctor_m90DE9F22B1C9203ACF924759EAFAC019C7BB53C5,
	OnGameStart_Invoke_m92DE6668CCE4F068DDCE1CC48053F60A2ED0E792,
	OnGameStart_BeginInvoke_m0E788DFACAB2911942CA8D716E0DF748DFF8C774,
	OnGameStart_EndInvoke_m5BFDA21CA96642DBD19AE74BFF59E41155D0BE07,
	OnPlaying__ctor_m9ED87EC988E36BF3B65CE46F7E62A98278E81774,
	OnPlaying_Invoke_mFC97A0E03A72D930F91ED5645567541F72D09E48,
	OnPlaying_BeginInvoke_m3CC0DA9476A9C96D8575C22C8844F7B45DF2F080,
	OnPlaying_EndInvoke_m94191BBBE2853F05E7D2B577CCFA62EC8294798A,
	OnLevelUp__ctor_m856D0386FD83CF7E6596DDD0D1B4437FA8765596,
	OnLevelUp_Invoke_m7831D64E99A8DC18A7E805DAB1341EF30D76BC7A,
	OnLevelUp_BeginInvoke_m3C47CA2F0BD60C2577FC68CC5625E40951C8CE20,
	OnLevelUp_EndInvoke_mD429D63FC4E7AE15A0D410837A792C2F654896B9,
	OnGameOver__ctor_m567F5D197A7D21C2A9CC4040BFC0E734E1D7E68A,
	OnGameOver_Invoke_m1D82E032223044BAAAF28191266C2EEAFC7A6D54,
	OnGameOver_BeginInvoke_mACC07A55BA95350EB457298C48D7B1C69397E907,
	OnGameOver_EndInvoke_m1BE0930C2AA10E20F9E6CDB827C672B0F4793E0E,
	OnShowGiftbox__ctor_m1AB838BF8ED671A7428F4661D0677566C4CBBC93,
	OnShowGiftbox_Invoke_m03ACAB74F660F6651BA9C5B2676E0B737392344A,
	OnShowGiftbox_BeginInvoke_m34C953C2DBECBC2FEA626A3F7FC483F52AFA73A3,
	OnShowGiftbox_EndInvoke_m4ABC998A40947EFD3BCEED7F4B8D4F0DCF58A187,
	OnShowPlayAgain__ctor_mC522658A38477164950C106A67CE7E1AEA275C4F,
	OnShowPlayAgain_Invoke_m33746D8A5E540BD11C470C653EECF60BC0C4CD45,
	OnShowPlayAgain_BeginInvoke_m4873EC8B2BCBF00CD1413027BBE1266BE4BB37E2,
	OnShowPlayAgain_EndInvoke_m089882F38921D8A7F595148B6EB8BCBB39C0CC28,
	CurrentLevelData__ctor_mCB087A0A7E55A5B870D77CB3370C206049DCFAF5,
	U3CtempCheckerU3Ed__4__ctor_mFD4DC68E759D5829D60F2406E6098C424FEC6FA9,
	U3CtempCheckerU3Ed__4_System_IDisposable_Dispose_mB85A6FB2D3D1A80833F5A3678A02717004015716,
	U3CtempCheckerU3Ed__4_MoveNext_mF002131FF191BA766FCAF967ADFBE46659DADC3C,
	U3CtempCheckerU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3AEB75A4D9B4B56439B4C080B89F714B8ECCF6E9,
	U3CtempCheckerU3Ed__4_System_Collections_IEnumerator_Reset_m0C5FD4AD543F8BDB03EC71996B304B1F66A6F22D,
	U3CtempCheckerU3Ed__4_System_Collections_IEnumerator_get_Current_m6944757F318827040B06C6C25D6AFBAA2973E6C5,
	RotationElements__ctor_mD6087F05B15B78D3E379873DD823F58B67848AE4,
	U3CStartRotationU3Ed__10__ctor_m5F6F08022723161FA5D9A565E83771A3C1375CF9,
	U3CStartRotationU3Ed__10_System_IDisposable_Dispose_m02925667265D142BE6FA3375207824B0B0227FC8,
	U3CStartRotationU3Ed__10_MoveNext_m2FEBAAD64269967C9DE907E91CC39DEAE23B1034,
	U3CStartRotationU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1E69544420E2AD796426066A1D429AFBA08AF296,
	U3CStartRotationU3Ed__10_System_Collections_IEnumerator_Reset_mE692E63C0CC6C47FE40928776F416A46820B0413,
	U3CStartRotationU3Ed__10_System_Collections_IEnumerator_get_Current_m7EF025F4DF265DA6A63EC2DF94C54A4FBB548E65,
	OnClickedButtonPlay__ctor_mB83B69AB0F60138DDE17FF181BAC36312525BAFE,
	OnClickedButtonPlay_Invoke_m30DB87D7A7DD7D61B706D3A5ECAB255DF26C599D,
	OnClickedButtonPlay_BeginInvoke_m2F6FFB95B978962EA69AE2CC5B5AB4D2B87EE3B4,
	OnClickedButtonPlay_EndInvoke_mBE2BCF14FC5A440E04A6A6A2F729F88FCFABCA42,
	CreditVerified__ctor_m16320AB9A8833C93A22BB3F3DAC58D660F59798B,
	CreditVerified_Invoke_mA96515B3A615F75CBF824F2B086A2E3555DE424E,
	CreditVerified_BeginInvoke_mDF595969DBC1EF8D642D40F74EF3A6F17BDE06DC,
	CreditVerified_EndInvoke_m53B3C86CE8DC844C5931DDC3EABFEFF175A425B0,
	U3CQuitGameU3Ed__12__ctor_m9A6AEB162750BD6E3A13033FEB4CCF41FC12FD32,
	U3CQuitGameU3Ed__12_System_IDisposable_Dispose_m64D19BD20997B9FFD808751CF8F3CF4C30F93BFB,
	U3CQuitGameU3Ed__12_MoveNext_m2C3CA77482F2B1D5127DEE502EE60E5C54EE75DC,
	U3CQuitGameU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m068A5D8A334111A3DA1916EA18C18953F1867B44,
	U3CQuitGameU3Ed__12_System_Collections_IEnumerator_Reset_m70743CA285DA438A4FB80BB7E5FA6EE4AAE04C0E,
	U3CQuitGameU3Ed__12_System_Collections_IEnumerator_get_Current_m27A6ABBD9EE709849393EBB9C3BD776719329552,
	Product_Equals_m24013D863F711F93519C34453E26E808E1A0686E,
	Product_GetHashCode_m1B92DAA11F5D91C027635DEAAB04D7D0198B4E8F,
	Product__ctor_m6EFEF541383756F20A571C74D523F6DC3D5E92B8,
	CharacterListItem_get_Id_m9DE6CEC1C0F803779454F78D9AC5B21C7D865DFB,
	CharacterListItem_set_Id_m6AEA185207FC1D111A4CE0349143735B5EB19D68,
	CharacterListItem_get_Name_m680B8EB072A36960C0554346E5E914ED71A80289,
	CharacterListItem_set_Name_mA73EFDEAD55D120E39A2014F916744B6000E3F76,
	CharacterListItem_get_Level_m10E8192D961A72A0E30E5FA3252514ACA12AB484,
	CharacterListItem_set_Level_mFEDA2AB64FAB01BE4C7ABA5753B26824422E61CF,
	CharacterListItem_get_Class_m851A77EA017AAF37F02104A72694FC28B87DFF21,
	CharacterListItem_set_Class_m0895FD4A2B96780726A35D0AF2097EC3183DFA9F,
	CharacterListItem_get_Sex_mB32D24EDEBF9CB677D6FA64BFCFE049871BD095D,
	CharacterListItem_set_Sex_mF6CC4BE8BFF1464B5BC621F4357BDF1ACD93BDE6,
	CharacterListItem__ctor_m4EF5B408AB89F2EA7F49504E5B7DDEE5F1378B94,
	Movie_get_Name_mF48FCA1C552E188CACB737D0B4831295B47D967C,
	Movie_set_Name_m7B776A884B9C2CE80EE8585C9A9594C5E575C69E,
	Movie_get_Description_m6085E85663D3332133D9D49B2D1290186CEFEFCE,
	Movie_set_Description_mD08790AD3CA6F08B035BED9103478848CDEA3163,
	Movie_get_Classification_m0459DC3BB52DF4E0F0790C799E1EFF3919BE0C6C,
	Movie_set_Classification_mAB8991781F80E23F4B633D95297CD0280BCE0829,
	Movie_get_Studio_mD4A42D1F0E1E5B62DF80A419EF7C6950FD3809A1,
	Movie_set_Studio_m320053FBE6C2F94C64716CB445AF505B74D19A15,
	Movie_get_ReleaseDate_m96B6D4EB7A3F7C778BE88725BCA78EA3CD28C423,
	Movie_set_ReleaseDate_m1D0E3581E781DEE546E1AF990057263007EFBABD,
	Movie_get_ReleaseCountries_m86802D5D1C3A9546DC43BA163B92098F9418F877,
	Movie_set_ReleaseCountries_mE815700894B23EBDE170AE332386B8CD9860B346,
	Movie__ctor_m3DDE2223972B64C3B44DCB2FCB17E7AB6970510F,
	U3CU3Ec__cctor_mDA82C2EF6063BB47B299296272C713873EB938C5,
	U3CU3Ec__ctor_mBFB4BD35AF855B60B3202619E831D4FB394CFC48,
	U3CU3Ec_U3COnMouseDownU3Eb__4_0_m174DA0084BF158C76305355492BE3C1FDDAAB0B1,
	MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8,
	MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801,
	MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4,
	MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35,
	MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA,
	MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43,
	MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E,
	MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6,
	CreditVerified__ctor_m6D08EA013526D0B1C954C16044BEC3CE0B8841AD,
	CreditVerified_Invoke_mFC8A312FC083A8A4CC499D6AF6F8D111811FFA82,
	CreditVerified_BeginInvoke_m14F6CC8749546E94279573689A8862B08EAFA4A2,
	CreditVerified_EndInvoke_m3AED88BD6161023EBA8CDF74D437C950355CDCB9,
	U3CU3Ec__DisplayClass0_0__ctor_mF76AB30BEF353B65814DA53ECA69C112D9656B70,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m25F68936FD8BED0ED35D6A3A7896F1859CF0177F,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m45287E9F8890C45FE0DE97D8ED6CD1F532F3FFAD,
	U3CU3Ec__DisplayClass1_0__ctor_mD956E9EE11DE6923BE5E83A55A8AC88859FB8DD4,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__0_m363ABEC29B7CBD13F98BFDBA491A9732B2D815C9,
	U3CU3Ec__DisplayClass1_0_U3CDOPitchU3Eb__1_mDB296FE590C17FBF30708268F6DA39F2857FA456,
	U3CU3Ec__DisplayClass2_0__ctor_m15003879FD9FF8F13F6D0AC654A12FAAA16A825F,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__0_m8447CD5F920C973ADB39884E1A6D0672D8E4C21F,
	U3CU3Ec__DisplayClass2_0_U3CDOSetFloatU3Eb__1_m99540719B3B2E5D89523D7D1C31AEF039088B8F6,
	U3CU3Ec__DisplayClass0_0__ctor_m7DD41ADAF604E6C34E536D1C97A420EAF90D45C2,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_mBDD978F428BA10922F372EAB21DE234526590F98,
	U3CU3Ec__DisplayClass1_0__ctor_mE4CA69377D3128AA8557F99767C5C1E30A74A20A,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m7CA25B5D3253D1A1BB83175835E6F06B37E6D3CA,
	U3CU3Ec__DisplayClass2_0__ctor_m6B860F4710DC7C6C7AE202D892BEA160A389BA2D,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m475A4066BB6D47109DBDA7711674E92F8B26F80F,
	U3CU3Ec__DisplayClass3_0__ctor_m8E161391F70AADA55A21658A677A53AF02108AAC,
	U3CU3Ec__DisplayClass3_0_U3CDOMoveZU3Eb__0_mFAD55624F73C8C7678B40AE2B544A4E21440093C,
	U3CU3Ec__DisplayClass4_0__ctor_m41CDC9DA22960F86F434E7C8EFCC926A7EB3890D,
	U3CU3Ec__DisplayClass4_0_U3CDORotateU3Eb__0_mCA650E955F3C9B985D9781B4E88F9EC8E22B4DF9,
	U3CU3Ec__DisplayClass5_0__ctor_m2B425FFF258947715FC7BA48E8628456D77BBB19,
	U3CU3Ec__DisplayClass5_0_U3CDOLookAtU3Eb__0_mBD3D65C2B6DAE37DB5CBA1BBF01D666A2908E540,
	U3CU3Ec__DisplayClass6_0__ctor_m66CDD42C56A002D2FCE56A80438EBA75BF339B62,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__0_m04813310411D22F519E3142909FCF08E4A83DB05,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__1_m2557DCAC83C5626D57FD6C4F8082A2D6BBD1CD3D,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__2_mB47776ADD5831D4ED18EDE2DDE38A858AC92A9C9,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__3_m67EF6E7E97403AB9053D7811A551A6D052D90CFD,
	U3CU3Ec__DisplayClass6_0_U3CDOJumpU3Eb__4_m5BE335A0259370532D153F4255FDC2788D42CE02,
	U3CU3Ec__DisplayClass7_0__ctor_m0933CB4C7EDA73C137344E0F5351FB87B9957CDA,
	U3CU3Ec__DisplayClass7_0_U3CDOPathU3Eb__0_mC71BA0A22CF6F7A1ACEC09FC99AAC8B963F653F8,
	U3CU3Ec__DisplayClass8_0__ctor_mD6555E0F3FAE1FDE188641F5D0DEE804368CC438,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__0_m9C60CDEB2CFE741DF9088E45428A7881F1E7E398,
	U3CU3Ec__DisplayClass8_0_U3CDOLocalPathU3Eb__1_m19470CDE4C65D782BB85336F3A6B29D0D0E5A95B,
	U3CU3Ec__DisplayClass9_0__ctor_m0C355BA31EC155AAFFE57E885820E87E2D24B630,
	U3CU3Ec__DisplayClass9_0_U3CDOPathU3Eb__0_mF111BF5DF70FA94EDC3C3A6F0D8A299272BC3262,
	U3CU3Ec__DisplayClass10_0__ctor_m5158D03E78581B5AC83602167F431A45FD6E3D30,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__0_mBC510690793F98CC7A4CEC293AA1C1420D77B006,
	U3CU3Ec__DisplayClass10_0_U3CDOLocalPathU3Eb__1_mFECC3FFD2469DB3CFFB9B6128E1D28E0CE9A4E30,
	U3CU3Ec__DisplayClass0_0__ctor_mCD42253A116528C3ABB04E9FF155E0CD20643826,
	U3CU3Ec__DisplayClass0_0_U3CDOMoveU3Eb__0_m7AAE48D11CDA351B8BD9555E638AA2AD3B967DDF,
	U3CU3Ec__DisplayClass1_0__ctor_m2197F33D0C489B447A1492CAF86ABEE69D9E44DA,
	U3CU3Ec__DisplayClass1_0_U3CDOMoveXU3Eb__0_m6317E8FB8A070BB91F81C94666F3CDA20A2E43E5,
	U3CU3Ec__DisplayClass2_0__ctor_m68B12007BF9F693FE0D2887557688AC886D103D1,
	U3CU3Ec__DisplayClass2_0_U3CDOMoveYU3Eb__0_m4DB120064C1A7D8A47A52371A6E532DACA6C73A6,
	U3CU3Ec__DisplayClass3_0__ctor_mA0010CB3454F2432226443E92B116B90D98BF0C8,
	U3CU3Ec__DisplayClass3_0_U3CDORotateU3Eb__0_mBCBA84515A05353317F5B564D6A0AC630C3CEE7C,
	U3CU3Ec__DisplayClass4_0__ctor_m02FBF2CD2B4A617552D10D38C23B6F31FDE5F534,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__0_m1D63D0BA78EB39F89A1B1E066912FFE44287B21B,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__1_mC5F1EA7F156F0624F25EA742FCC3909966F9A7F3,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__2_mF0D4140DD5E81B476903362C39693F8B8F457FB9,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__3_m9B07E2AACF2C5AE01FF2BF681C2CB7D99007B0BD,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__4_mF948C7A5035C6296D75BC18DB7F077EE07D0510C,
	U3CU3Ec__DisplayClass4_0_U3CDOJumpU3Eb__5_m7E5499FFA14E6263A35F34565AA865D7F812EF0C,
	U3CU3Ec__DisplayClass0_0__ctor_m31B504FBEDE581BEA37F6C8D539D8BADB9C8FE04,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__0_m19FA6E77EF091166B632107C37CF96158B3CE288,
	U3CU3Ec__DisplayClass0_0_U3CDOColorU3Eb__1_m44D2693D1A6B416C6C306A7040E5B1F009B6ED3D,
	U3CU3Ec__DisplayClass1_0__ctor_mB5AA081132A07D10B45B63C3A7D8996C98FFF09A,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__0_m493943ED4559E6518634D0901230AAC5808BFEDD,
	U3CU3Ec__DisplayClass1_0_U3CDOFadeU3Eb__1_mF7579710FC82C1F20388F050DF39FEF2D0CE7EDE,
	U3CU3Ec__DisplayClass3_0__ctor_mBBED6FCE860513FB15E971A9E4A57EEAE0B35E55,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__0_mEA5E8386E195FF3054C4AC1F7C46FCFA2E3095F5,
	U3CU3Ec__DisplayClass3_0_U3CDOBlendableColorU3Eb__1_mC3182245E5D26849A9001AC53256A5856E4DC908,
	Utils_SwitchToRectTransform_m953E8B35B59142D580B1EC5A3CB48163D94FE270,
	U3CU3Ec__DisplayClass0_0__ctor_m18A8356F5FF1FB67DC8B3E62188C603519B3124B,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__0_m32C5F6AA875203D30B95F0239833E503B58A4080,
	U3CU3Ec__DisplayClass0_0_U3CDOFadeU3Eb__1_m35704DDE3B470E932AE2394E444E236EF73D0950,
	U3CU3Ec__DisplayClass1_0__ctor_m2F3D328081E4370B02A8DC65A5595FD7779319F7,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__0_mD5A08FA687B4F3D4ACDE03C5F4747ADD5A85ACE8,
	U3CU3Ec__DisplayClass1_0_U3CDOColorU3Eb__1_m17953C82468E26BC336CB919379BEA94DB403CF5,
	U3CU3Ec__DisplayClass2_0__ctor_m42ECD7DED23372FD451B099F33986255C1F05F24,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__0_m96E8793AEB0F5454A98699F2A492CD0C6A6F29D9,
	U3CU3Ec__DisplayClass2_0_U3CDOFadeU3Eb__1_m22E508875493477F1B41CE7B93A3327597C7CA64,
	U3CU3Ec__DisplayClass3_0__ctor_mE21167957C3A82E6A2212EA06099E19A61C888F5,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__0_mD60F568B3218FB775B0F2F472DEF66F12F240EB8,
	U3CU3Ec__DisplayClass3_0_U3CDOColorU3Eb__1_mF414A2392F00D79E324899FFFD8BCCA32B63113A,
	U3CU3Ec__DisplayClass4_0__ctor_mBBFBBB66427C51F249BBFF57BB9DBE3738E70DF0,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__0_m2ABDE00A2CE387D94979FF4772233C32E0433002,
	U3CU3Ec__DisplayClass4_0_U3CDOFadeU3Eb__1_m62D514A0CBEE93DDB5E11A83FFEF88F70887B006,
	U3CU3Ec__DisplayClass5_0__ctor_m9C45A8A17A10C098C69BBAF60D2639D9ED29A978,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__0_m2CFEEB05691BF628CD9723B772220FBF1B5DEFC5,
	U3CU3Ec__DisplayClass5_0_U3CDOFillAmountU3Eb__1_mC2B9793EB03BCFC06E022CB10D2AE3D4CDC2DC9B,
	U3CU3Ec__DisplayClass7_0__ctor_m5080623FCA82C0A8E5638EDE08125692828AB793,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__0_mFA007EE23041AC3DB626E202537641F7CCC10895,
	U3CU3Ec__DisplayClass7_0_U3CDOFlexibleSizeU3Eb__1_mBFC566A897CBAFAFCC74D5DBCEC136A07F1A5F9D,
	U3CU3Ec__DisplayClass8_0__ctor_m70D737AA33CF6139F30568CAA4BFAD2E9F1D66BA,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__0_m5AADB9E424374D66A929CAE4FE7C91FAAD9C1F92,
	U3CU3Ec__DisplayClass8_0_U3CDOMinSizeU3Eb__1_mF51654F8E42E61B6D410192E9C4493BC7B50B7A6,
	U3CU3Ec__DisplayClass9_0__ctor_m6054B368E828204FC04AC31B20D57ECE521DA2CE,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__0_mF3EEFDE0596A75FA4C9B5E3CBB8D27B37286B7D7,
	U3CU3Ec__DisplayClass9_0_U3CDOPreferredSizeU3Eb__1_m723F0F3AD18E54882602EF10C41A9138C2094C92,
	U3CU3Ec__DisplayClass10_0__ctor_m3C2AC3BA68FE823C5EE4ABF01CF043FB14BC77D4,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__0_mD23FC3413F649B7175F76650D99C969D080A52B6,
	U3CU3Ec__DisplayClass10_0_U3CDOColorU3Eb__1_m2A91553D7D78ABD3E8125820476F8026322DD1AD,
	U3CU3Ec__DisplayClass11_0__ctor_m922DA2833A1846F6E644BACDA5360882FE3CBD5C,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__0_m97CEC28FB18E8EC25DE68CBBC9363C1969199D92,
	U3CU3Ec__DisplayClass11_0_U3CDOFadeU3Eb__1_m7C9745929EF3497843A96CE489528E972E1696D2,
	U3CU3Ec__DisplayClass12_0__ctor_mDB084173826CEF2F054C58D71259B79FABE1AFB1,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__0_mD4ACC050241FAF74718A82BAA2FAD799B221D52F,
	U3CU3Ec__DisplayClass12_0_U3CDOScaleU3Eb__1_mB98E48B7CD64FAEDBF41990DFA5EDFB84BEAA635,
	U3CU3Ec__DisplayClass13_0__ctor_mC4153B76DCBFB2268DBBA69D56E679A477FCBE2C,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__0_mEC1BFCBC158066EE11442276BAF4904FC8167F78,
	U3CU3Ec__DisplayClass13_0_U3CDOAnchorPosU3Eb__1_mE62D5B3135C3EA0973694EDF23FEFBE0879E0431,
	U3CU3Ec__DisplayClass14_0__ctor_m179837AE08CE76F84AD7C2DE1C4536FB2BC6724E,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__0_m946B10547ACAFC05288900D6C8AE8BE29FA8E769,
	U3CU3Ec__DisplayClass14_0_U3CDOAnchorPosXU3Eb__1_mC4EA8586148CB5AE824CA16BE35F0C7E15F13927,
	U3CU3Ec__DisplayClass15_0__ctor_mD9D66D4A39054D51E9A3A43A3E47FE76D14E8E2A,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__0_mE74B0E96F68D07C8FB2B95F696DDB3F3EF90633E,
	U3CU3Ec__DisplayClass15_0_U3CDOAnchorPosYU3Eb__1_m818E4ABC9402DC49D5DC6F8042472F3D08891D95,
	U3CU3Ec__DisplayClass16_0__ctor_m266310E4E7253E8AD0DB494DC917560C7312B6E3,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__0_m54AA9D5E75139ECB7FB5D0BF9518849E9258416F,
	U3CU3Ec__DisplayClass16_0_U3CDOAnchorPos3DU3Eb__1_mE14500ADC3593F53F54C4D66F17888A87B84344E,
	U3CU3Ec__DisplayClass17_0__ctor_m6866C080BE78AEC33050A808BD28DD4FF4B7004B,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__0_m7C05989ECA103CD60B5CF731D6C7920B1FC985E1,
	U3CU3Ec__DisplayClass17_0_U3CDOAnchorPos3DXU3Eb__1_mFB2895AADD1CDF00CFBA835E56A9E13904BAD620,
	U3CU3Ec__DisplayClass18_0__ctor_mCDA7BC09C26B4712D699A1FB257E8ACC7F8B2AA6,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__0_m9D5E91C0F8BF3135CA706B2D09504A99E384E3F6,
	U3CU3Ec__DisplayClass18_0_U3CDOAnchorPos3DYU3Eb__1_mE4A2B4E589B150E943E18A30D33E95B1754E2886,
	U3CU3Ec__DisplayClass19_0__ctor_mFDD1B1264FCD4CDDB8A38EA8AD5909CC0FEF92D1,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__0_mCE069FE82ADB466CBC0DAFC700BC15C8613B138A,
	U3CU3Ec__DisplayClass19_0_U3CDOAnchorPos3DZU3Eb__1_mD34C0057C888DD82D38BE7593C1E92FF925914DA,
	U3CU3Ec__DisplayClass20_0__ctor_m5410B8E63F947DE005A20DE7E85E3CCE2D72079B,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__0_m0D36293A6361A326C0853382366377CE10796BC9,
	U3CU3Ec__DisplayClass20_0_U3CDOAnchorMaxU3Eb__1_m5729647091DBBF0D726254ABADF2377ADA0101AD,
	U3CU3Ec__DisplayClass21_0__ctor_mFB3C178CAA653C95BEE3093D575FEB85F8C7367F,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__0_mD66DD96927DDAF369C64AE446E76D6F0F03AE0F5,
	U3CU3Ec__DisplayClass21_0_U3CDOAnchorMinU3Eb__1_m7189572DD336DDE6EC85F27770F731FEEAA2FD50,
	U3CU3Ec__DisplayClass22_0__ctor_m7D1B522F01E0419DDBA693585300D0B01AF984FB,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__0_m7DC62E4EE195C844218B14EB4EF6E5AED8989D0D,
	U3CU3Ec__DisplayClass22_0_U3CDOPivotU3Eb__1_m10B5380A3AA5CEAE992081A449EE1F74EC79B15C,
	U3CU3Ec__DisplayClass23_0__ctor_m75C045C8DEAC70D81B02EAFC1EA366AE0A7AA8F5,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__0_mF72A07116DE5256A691DBAFB766E637F55C4EF45,
	U3CU3Ec__DisplayClass23_0_U3CDOPivotXU3Eb__1_m8EABFE51EA9393CC2EC7F0055F02DED35D3B9186,
	U3CU3Ec__DisplayClass24_0__ctor_m7157DCBC4845023E76ECA85BE6C3F81EFCD04239,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__0_mADD1E688B9294ECD2A7CCD79F78CA0CD68E055B4,
	U3CU3Ec__DisplayClass24_0_U3CDOPivotYU3Eb__1_m62A33BFDF3873451B14AE3EBA0791AF52D082F15,
	U3CU3Ec__DisplayClass25_0__ctor_mAE01B70E706B6F8A90EA06CCE17FC489F1FC71D2,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__0_m036AB1FD8ACAAB27669C6F82B807258A7CF95161,
	U3CU3Ec__DisplayClass25_0_U3CDOSizeDeltaU3Eb__1_m3F1A0A60B77F9C9AADC0C9BE1969FD929AEC15AE,
	U3CU3Ec__DisplayClass26_0__ctor_m143FC1CEE9576076B1C3F954E13D9F709DE22292,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__0_mA1CDB05B94AFD8D48E1B1DB4B6F2F53FDDD274EC,
	U3CU3Ec__DisplayClass26_0_U3CDOPunchAnchorPosU3Eb__1_m2F2781A19452E8F74222AABE3617555A4CEA2DC4,
	U3CU3Ec__DisplayClass27_0__ctor_m3A3C7488BD686D2417811F19408B70D93B0FA02F,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__0_m23ADE1EDABB1FF436DCB15B4D2956341A403F86F,
	U3CU3Ec__DisplayClass27_0_U3CDOShakeAnchorPosU3Eb__1_mED3FDE11B0B31284228C7D6A01C66ADFAD06D05B,
	U3CU3Ec__DisplayClass28_0__ctor_mC9FC42C7A712D81D3E1FFD069C16C866CF7F3A2C,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__0_mBD11B2BA0B8A07753156C1F9CA22B33A041F6A28,
	U3CU3Ec__DisplayClass28_0_U3CDOShakeAnchorPosU3Eb__1_m560D14C7C7B82350B44D745D7DB7AE161730A086,
	U3CU3Ec__DisplayClass29_0__ctor_mC30B5380B8DE2DCAC55DA68241D5EF1266F79909,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__0_m3336D670200C9F13D96D7F52F123920132234850,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__1_m5B9935C76D1779CA20BEC5C8434C34BDF9ACA7EF,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__2_mFAA2F463E2CED9DE0BA520D44BDC7A64ED56BC6E,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__3_m0D9E7219678950432D0E913F31C5A3E3B3D1F449,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__4_m41FE20829CE35CC646EF346A2ED2D42A8F7FFF6D,
	U3CU3Ec__DisplayClass29_0_U3CDOJumpAnchorPosU3Eb__5_m46AB05BB46F4343B5372269801C929C6BEA4E78C,
	U3CU3Ec__DisplayClass30_0__ctor_m8786A94537CDE17C192F816195A8D5095A147611,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__0_mE69AF155BCC63288298C03F1FDE8264894DC5112,
	U3CU3Ec__DisplayClass30_0_U3CDONormalizedPosU3Eb__1_m34D4384708B00929EC0936A3D9818FBB9320124A,
	U3CU3Ec__DisplayClass31_0__ctor_m430D95F36B8C311BCA555E6C9EB4E3D5B6CB52DD,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__0_m4B50B77000E1A5B0959F7EFEC780C43A2B3C21A6,
	U3CU3Ec__DisplayClass31_0_U3CDOHorizontalNormalizedPosU3Eb__1_mE79549F5C6F07819B8FCA4A3670548B94B794980,
	U3CU3Ec__DisplayClass32_0__ctor_m03AFF4E74AF489641F31A7ED7BB588DC48C86B47,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__0_mDFC279CC6199107052C6C57F7C80069AF08B727E,
	U3CU3Ec__DisplayClass32_0_U3CDOVerticalNormalizedPosU3Eb__1_m69F269F657FA4BD2821E0451B328802CA74E5E4D,
	U3CU3Ec__DisplayClass33_0__ctor_m44794063561EE5542E8D44B8047DA0EB476D9D99,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__0_m49CBBBEC21C5C4D3517ED220CE99FECCA32D8E17,
	U3CU3Ec__DisplayClass33_0_U3CDOValueU3Eb__1_mC23A71E0D945D05C253DAA3C6B29F017AC3C8B7A,
	U3CU3Ec__DisplayClass34_0__ctor_m31CDFCCAA9BC664B12E06164FE6FD3F6BCF1CE10,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__0_mC0172F2115741150E114BCEB8C6366E8DC05F0FE,
	U3CU3Ec__DisplayClass34_0_U3CDOColorU3Eb__1_mC68427660102A2FD7D41ED52EA99141A48A176E5,
	U3CU3Ec__DisplayClass35_0__ctor_m5119796185D9945B3DADE92E11D912791044F5F1,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__0_m7AEADCAF76E9378EF16998AF06C981E238317A14,
	U3CU3Ec__DisplayClass35_0_U3CDOFadeU3Eb__1_m6F73BC7514BC81F58222707B2FEA9F1775452C1B,
	U3CU3Ec__DisplayClass36_0__ctor_m266E0D445578A8CF42FF8E6E28F8DD0D5816B6C6,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__0_m8450222712B70BE65D184CE038DDA30929396E09,
	U3CU3Ec__DisplayClass36_0_U3CDOTextU3Eb__1_m5EBCE38D43719324BFF117A085F469D6C7A53A4C,
	U3CU3Ec__DisplayClass37_0__ctor_mF7EE64DF2C6519BFCE36A53802C870392DCA681E,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__0_m5C210BC6DB5E81E37C5DA7F0ED05DCEBF88A838F,
	U3CU3Ec__DisplayClass37_0_U3CDOBlendableColorU3Eb__1_m001CE2BECACB83DE0BB4252DFAEFF93D51676EDF,
	U3CU3Ec__DisplayClass38_0__ctor_m029E260AE14F07692FEC152AA854A3E7E818F5A8,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__0_mD045F88560E9C38E36C045962A24AEEE7E220146,
	U3CU3Ec__DisplayClass38_0_U3CDOBlendableColorU3Eb__1_mF9673E803B1ED13D3937EA5093B07828944B0D33,
	U3CU3Ec__DisplayClass39_0__ctor_m2D3EEE4A819B3C837A38B0F228978C5DECA02157,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__0_m769FD067F1DD156E6737065978D723626F684DD4,
	U3CU3Ec__DisplayClass39_0_U3CDOBlendableColorU3Eb__1_m9E000E92EC974F7B26E70A9DB887A7B5ECD13C4A,
	U3CU3Ec__DisplayClass8_0__ctor_mCB2FA585275686213140C65F568AF5B895DE515D,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__0_m8CB04FB886F55DF914E092EAB3E88CADA5E4BFEB,
	U3CU3Ec__DisplayClass8_0_U3CDOOffsetU3Eb__1_m70E3E83723C4C320D2E78FBB32E3C5AB9E483413,
	U3CU3Ec__DisplayClass9_0__ctor_m71E4A32D334F5A1DDFE2DB998E25634EC8C9E0D8,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__0_m45117726269A56BEFE7511E185A5EB5DC6614C08,
	U3CU3Ec__DisplayClass9_0_U3CDOTilingU3Eb__1_mEC4AEA77E8E47513A16390421DFE3814DB87DFBD,
	WaitForCompletion_get_keepWaiting_m30EF66A11D003F93F4CBF834A856E7298D5955B0,
	WaitForCompletion__ctor_m69692E16010A98F06E13B8668A2C9AC26E470727,
	WaitForRewind_get_keepWaiting_m8E68A912E510F991CC524EA2DCB6634BE7F7DF65,
	WaitForRewind__ctor_m111D38D621831BAE9D00CCC7F195DBC48A9483D4,
	WaitForKill_get_keepWaiting_m410BE82C7AEB9A7E45E5E07EEBDA1D2E2CEB50B4,
	WaitForKill__ctor_mD127E54E87A255CD19BB9539CE786DD6953C5719,
	WaitForElapsedLoops_get_keepWaiting_m6A50A75A89879252500ED70BD15C0C04CF6DC2D7,
	WaitForElapsedLoops__ctor_mE11D912E8954AB448C21081560A6FBB9D974B7F3,
	WaitForPosition_get_keepWaiting_m78A4DAE5D866FA838D2A843113F275E169B03A30,
	WaitForPosition__ctor_mB0DA06151DE0C3690CCF8A3117189FA2800CB3FC,
	WaitForStart_get_keepWaiting_m218C9D65E9141136A148DD636B155272308A9F5E,
	WaitForStart__ctor_m9088C849D74DBB8D2E12FA05AD5A2E17AA48E77B,
	Physics_SetOrientationOnPath_m8BE0D531D59E8E8D9F092FB6C9F881C98BDE429B,
	Physics_HasRigidbody2D_mA2B48BE3FC3BB297B8227F0ECA26BAE6B49732A8,
	Physics_HasRigidbody_m5C3BC90BFBC15B8787E33728CBC9FD4DBAF10DE6,
	Physics_CreateDOTweenPathTween_m5026DCBFA244A145B5076E485DA4643EFA331B42,
};
static const int32_t s_InvokerIndices[699] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	4,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	4,
	122,
	23,
	23,
	23,
	23,
	10,
	23,
	26,
	23,
	23,
	23,
	10,
	14,
	10,
	14,
	710,
	710,
	114,
	114,
	10,
	10,
	23,
	23,
	14,
	713,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	23,
	26,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	0,
	-1,
	40,
	26,
	23,
	122,
	131,
	4,
	122,
	26,
	26,
	26,
	26,
	3,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	1671,
	1671,
	1631,
	1648,
	1648,
	95,
	1650,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	95,
	1685,
	1686,
	1686,
	1686,
	1687,
	1690,
	1691,
	1695,
	1695,
	1696,
	1696,
	2019,
	1686,
	1686,
	1671,
	2020,
	1672,
	1671,
	1667,
	1672,
	1671,
	1672,
	1671,
	1672,
	1671,
	1671,
	1667,
	2019,
	2019,
	2019,
	1672,
	1671,
	1681,
	2019,
	1686,
	1686,
	1685,
	1686,
	1686,
	1686,
	2019,
	2019,
	1681,
	1671,
	1671,
	2019,
	2020,
	1693,
	2021,
	2020,
	2019,
	1686,
	1686,
	1686,
	1672,
	1671,
	2022,
	1672,
	1672,
	1672,
	1667,
	1635,
	121,
	121,
	121,
	678,
	1702,
	121,
	2024,
	2024,
	3,
	3,
	23,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	23,
	32,
	23,
	114,
	14,
	23,
	14,
	102,
	23,
	113,
	26,
	102,
	23,
	113,
	26,
	32,
	23,
	114,
	14,
	23,
	14,
	9,
	10,
	23,
	10,
	32,
	14,
	26,
	10,
	32,
	14,
	26,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1806,
	1831,
	14,
	26,
	23,
	3,
	23,
	26,
	102,
	26,
	177,
	26,
	102,
	26,
	177,
	26,
	102,
	23,
	113,
	26,
	23,
	710,
	310,
	23,
	710,
	310,
	23,
	710,
	310,
	23,
	1180,
	23,
	1180,
	23,
	1180,
	23,
	1180,
	23,
	1336,
	23,
	1336,
	23,
	1180,
	23,
	1180,
	1180,
	23,
	23,
	1180,
	23,
	1180,
	1181,
	23,
	1180,
	23,
	1180,
	1181,
	23,
	1188,
	23,
	1188,
	23,
	1188,
	23,
	710,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	2023,
	23,
	710,
	310,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	710,
	310,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1180,
	1181,
	23,
	1180,
	1181,
	23,
	1180,
	1181,
	23,
	1180,
	1181,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	1180,
	1181,
	23,
	1180,
	1181,
	23,
	1180,
	1181,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	23,
	23,
	1188,
	1203,
	23,
	710,
	310,
	23,
	710,
	310,
	23,
	710,
	310,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	14,
	26,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1132,
	1133,
	23,
	1188,
	1203,
	23,
	1188,
	1203,
	114,
	26,
	114,
	26,
	114,
	26,
	114,
	136,
	114,
	896,
	114,
	26,
	1777,
	94,
	94,
	2025,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000002, { 0, 6 } },
	{ 0x02000003, { 6, 21 } },
	{ 0x06000111, { 27, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[28] = 
{
	{ (Il2CppRGCTXDataType)2, 20357 },
	{ (Il2CppRGCTXDataType)3, 15674 },
	{ (Il2CppRGCTXDataType)2, 20358 },
	{ (Il2CppRGCTXDataType)3, 15675 },
	{ (Il2CppRGCTXDataType)3, 15676 },
	{ (Il2CppRGCTXDataType)2, 19687 },
	{ (Il2CppRGCTXDataType)2, 20359 },
	{ (Il2CppRGCTXDataType)3, 15677 },
	{ (Il2CppRGCTXDataType)2, 20360 },
	{ (Il2CppRGCTXDataType)3, 15678 },
	{ (Il2CppRGCTXDataType)3, 15679 },
	{ (Il2CppRGCTXDataType)2, 20361 },
	{ (Il2CppRGCTXDataType)3, 15680 },
	{ (Il2CppRGCTXDataType)3, 15681 },
	{ (Il2CppRGCTXDataType)2, 20362 },
	{ (Il2CppRGCTXDataType)3, 15682 },
	{ (Il2CppRGCTXDataType)3, 15683 },
	{ (Il2CppRGCTXDataType)2, 20363 },
	{ (Il2CppRGCTXDataType)3, 15684 },
	{ (Il2CppRGCTXDataType)3, 15685 },
	{ (Il2CppRGCTXDataType)3, 15686 },
	{ (Il2CppRGCTXDataType)3, 15687 },
	{ (Il2CppRGCTXDataType)3, 15688 },
	{ (Il2CppRGCTXDataType)2, 19691 },
	{ (Il2CppRGCTXDataType)2, 19692 },
	{ (Il2CppRGCTXDataType)2, 19693 },
	{ (Il2CppRGCTXDataType)2, 19694 },
	{ (Il2CppRGCTXDataType)3, 15689 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	699,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	28,
	s_rgctxValues,
	NULL,
};
