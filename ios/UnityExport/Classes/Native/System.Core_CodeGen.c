﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 ();
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 ();
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 ();
// 0x00000004 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 ();
// 0x00000005 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 ();
// 0x00000006 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D ();
// 0x00000007 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000009 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000000A System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x0000000B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000012 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000015 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000016 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000017 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000018 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000019 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001D TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000020 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000021 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000023 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000024 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000025 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000026 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000027 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000028 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000029 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x0000002A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x0000002B System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x0000002C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000002D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000002F System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000030 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000031 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000032 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x00000033 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x00000034 System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000039 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003C System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x0000003E System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x0000003F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000041 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000042 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x00000043 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x00000044 System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000047 System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000048 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000049 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x0000004A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004C System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000004D System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x0000004E System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x0000004F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000051 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x00000052 System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000053 System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x00000054 TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000055 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000056 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000057 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000058 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000059 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000005A TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000005B System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000005C System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000005D System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000005E System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005F System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000060 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x00000061 System.Boolean System.Linq.Enumerable_<UnionIterator>d__71`1::MoveNext()
// 0x00000062 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally1()
// 0x00000063 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::<>m__Finally2()
// 0x00000064 TSource System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000065 System.Void System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000066 System.Object System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000068 System.Collections.IEnumerator System.Linq.Enumerable_<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000069 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x0000006A System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x0000006B System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x0000006C System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x0000006D TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000006E System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x0000006F System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x00000070 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000071 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000072 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x00000073 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x00000074 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x00000075 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x00000076 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x00000077 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000078 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000079 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000007A System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x0000007B System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000007C System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x0000007D System.Linq.Lookup`2_Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x0000007E System.Void System.Linq.Lookup`2::Resize()
// 0x0000007F System.Void System.Linq.Lookup`2_Grouping::Add(TElement)
// 0x00000080 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2_Grouping::GetEnumerator()
// 0x00000081 System.Collections.IEnumerator System.Linq.Lookup`2_Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000082 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x00000083 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x00000084 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x00000085 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x00000086 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x00000087 System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x00000088 System.Boolean System.Linq.Lookup`2_Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000089 System.Int32 System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x0000008A System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x0000008B System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x0000008C TElement System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x0000008D System.Void System.Linq.Lookup`2_Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x0000008E System.Void System.Linq.Lookup`2_Grouping::.ctor()
// 0x0000008F System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::.ctor(System.Int32)
// 0x00000090 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x00000091 System.Boolean System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::MoveNext()
// 0x00000092 TElement System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000093 System.Void System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x00000094 System.Object System.Linq.Lookup`2_Grouping_<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x00000095 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::.ctor(System.Int32)
// 0x00000096 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x00000097 System.Boolean System.Linq.Lookup`2_<GetEnumerator>d__12::MoveNext()
// 0x00000098 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x00000099 System.Void System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x0000009A System.Object System.Linq.Lookup`2_<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x0000009B System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x0000009C System.Boolean System.Linq.Set`1::Add(TElement)
// 0x0000009D System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x0000009E System.Void System.Linq.Set`1::Resize()
// 0x0000009F System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000A0 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000A1 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000A2 System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A3 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000A4 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000A5 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A6 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000A7 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000A8 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000A9 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000AA System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000AB TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000AC System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000AD System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000AE System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000AF System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000B0 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000B1 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000B2 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000B3 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000B4 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000B5 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000B6 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000B7 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000B8 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000B9 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000BA System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000BB System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000BC System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000BD System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000BE System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000BF System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000C0 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000C1 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000C2 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000C3 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000C4 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000C5 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000C6 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C7 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000C8 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000C9 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000CA System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000CB System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000CC System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000CD System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000CE System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000CF System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000D0 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000D1 System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000D2 System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x000000D3 System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x000000D4 T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x000000D5 System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000D6 System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[214] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	Error_NotSupported_mD771E9977E8BE0B8298A582AB0BB74D1CF10900D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[214] = 
{
	0,
	0,
	4,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[67] = 
{
	{ 0x02000004, { 103, 4 } },
	{ 0x02000005, { 107, 9 } },
	{ 0x02000006, { 118, 7 } },
	{ 0x02000007, { 127, 10 } },
	{ 0x02000008, { 139, 11 } },
	{ 0x02000009, { 153, 9 } },
	{ 0x0200000A, { 165, 12 } },
	{ 0x0200000B, { 180, 1 } },
	{ 0x0200000C, { 181, 2 } },
	{ 0x0200000D, { 183, 12 } },
	{ 0x0200000E, { 195, 12 } },
	{ 0x0200000F, { 207, 6 } },
	{ 0x02000010, { 213, 2 } },
	{ 0x02000011, { 215, 4 } },
	{ 0x02000012, { 219, 3 } },
	{ 0x02000015, { 222, 17 } },
	{ 0x02000016, { 243, 5 } },
	{ 0x02000017, { 248, 1 } },
	{ 0x02000019, { 249, 8 } },
	{ 0x0200001B, { 257, 4 } },
	{ 0x0200001C, { 261, 3 } },
	{ 0x0200001D, { 266, 5 } },
	{ 0x0200001E, { 271, 7 } },
	{ 0x0200001F, { 278, 3 } },
	{ 0x02000020, { 281, 7 } },
	{ 0x02000021, { 288, 4 } },
	{ 0x02000022, { 292, 21 } },
	{ 0x02000024, { 313, 2 } },
	{ 0x06000007, { 0, 10 } },
	{ 0x06000008, { 10, 10 } },
	{ 0x06000009, { 20, 5 } },
	{ 0x0600000A, { 25, 5 } },
	{ 0x0600000B, { 30, 1 } },
	{ 0x0600000C, { 31, 2 } },
	{ 0x0600000D, { 33, 2 } },
	{ 0x0600000E, { 35, 1 } },
	{ 0x0600000F, { 36, 4 } },
	{ 0x06000010, { 40, 1 } },
	{ 0x06000011, { 41, 2 } },
	{ 0x06000012, { 43, 3 } },
	{ 0x06000013, { 46, 2 } },
	{ 0x06000014, { 48, 1 } },
	{ 0x06000015, { 49, 7 } },
	{ 0x06000016, { 56, 2 } },
	{ 0x06000017, { 58, 2 } },
	{ 0x06000018, { 60, 4 } },
	{ 0x06000019, { 64, 4 } },
	{ 0x0600001A, { 68, 4 } },
	{ 0x0600001B, { 72, 3 } },
	{ 0x0600001C, { 75, 4 } },
	{ 0x0600001D, { 79, 4 } },
	{ 0x0600001E, { 83, 3 } },
	{ 0x0600001F, { 86, 1 } },
	{ 0x06000020, { 87, 1 } },
	{ 0x06000021, { 88, 3 } },
	{ 0x06000022, { 91, 3 } },
	{ 0x06000023, { 94, 2 } },
	{ 0x06000024, { 96, 2 } },
	{ 0x06000025, { 98, 5 } },
	{ 0x06000035, { 116, 2 } },
	{ 0x0600003A, { 125, 2 } },
	{ 0x0600003F, { 137, 2 } },
	{ 0x06000045, { 150, 3 } },
	{ 0x0600004A, { 162, 3 } },
	{ 0x0600004F, { 177, 3 } },
	{ 0x06000078, { 239, 4 } },
	{ 0x060000A6, { 264, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[315] = 
{
	{ (Il2CppRGCTXDataType)2, 20188 },
	{ (Il2CppRGCTXDataType)3, 15212 },
	{ (Il2CppRGCTXDataType)2, 20189 },
	{ (Il2CppRGCTXDataType)2, 20190 },
	{ (Il2CppRGCTXDataType)3, 15213 },
	{ (Il2CppRGCTXDataType)2, 20191 },
	{ (Il2CppRGCTXDataType)2, 20192 },
	{ (Il2CppRGCTXDataType)3, 15214 },
	{ (Il2CppRGCTXDataType)2, 20193 },
	{ (Il2CppRGCTXDataType)3, 15215 },
	{ (Il2CppRGCTXDataType)2, 20194 },
	{ (Il2CppRGCTXDataType)3, 15216 },
	{ (Il2CppRGCTXDataType)2, 20195 },
	{ (Il2CppRGCTXDataType)2, 20196 },
	{ (Il2CppRGCTXDataType)3, 15217 },
	{ (Il2CppRGCTXDataType)2, 20197 },
	{ (Il2CppRGCTXDataType)2, 20198 },
	{ (Il2CppRGCTXDataType)3, 15218 },
	{ (Il2CppRGCTXDataType)2, 20199 },
	{ (Il2CppRGCTXDataType)3, 15219 },
	{ (Il2CppRGCTXDataType)2, 20200 },
	{ (Il2CppRGCTXDataType)3, 15220 },
	{ (Il2CppRGCTXDataType)3, 15221 },
	{ (Il2CppRGCTXDataType)2, 14820 },
	{ (Il2CppRGCTXDataType)3, 15222 },
	{ (Il2CppRGCTXDataType)2, 20201 },
	{ (Il2CppRGCTXDataType)3, 15223 },
	{ (Il2CppRGCTXDataType)3, 15224 },
	{ (Il2CppRGCTXDataType)2, 14827 },
	{ (Il2CppRGCTXDataType)3, 15225 },
	{ (Il2CppRGCTXDataType)3, 15226 },
	{ (Il2CppRGCTXDataType)2, 20202 },
	{ (Il2CppRGCTXDataType)3, 15227 },
	{ (Il2CppRGCTXDataType)2, 20203 },
	{ (Il2CppRGCTXDataType)3, 15228 },
	{ (Il2CppRGCTXDataType)3, 15229 },
	{ (Il2CppRGCTXDataType)3, 15230 },
	{ (Il2CppRGCTXDataType)2, 20204 },
	{ (Il2CppRGCTXDataType)2, 20205 },
	{ (Il2CppRGCTXDataType)3, 15231 },
	{ (Il2CppRGCTXDataType)3, 15232 },
	{ (Il2CppRGCTXDataType)2, 20206 },
	{ (Il2CppRGCTXDataType)3, 15233 },
	{ (Il2CppRGCTXDataType)2, 20207 },
	{ (Il2CppRGCTXDataType)3, 15234 },
	{ (Il2CppRGCTXDataType)3, 15235 },
	{ (Il2CppRGCTXDataType)2, 14863 },
	{ (Il2CppRGCTXDataType)3, 15236 },
	{ (Il2CppRGCTXDataType)3, 15237 },
	{ (Il2CppRGCTXDataType)2, 14878 },
	{ (Il2CppRGCTXDataType)3, 15238 },
	{ (Il2CppRGCTXDataType)2, 14871 },
	{ (Il2CppRGCTXDataType)2, 20208 },
	{ (Il2CppRGCTXDataType)3, 15239 },
	{ (Il2CppRGCTXDataType)3, 15240 },
	{ (Il2CppRGCTXDataType)3, 15241 },
	{ (Il2CppRGCTXDataType)2, 14879 },
	{ (Il2CppRGCTXDataType)3, 15242 },
	{ (Il2CppRGCTXDataType)2, 20209 },
	{ (Il2CppRGCTXDataType)3, 15243 },
	{ (Il2CppRGCTXDataType)2, 20210 },
	{ (Il2CppRGCTXDataType)2, 20211 },
	{ (Il2CppRGCTXDataType)2, 14883 },
	{ (Il2CppRGCTXDataType)2, 20212 },
	{ (Il2CppRGCTXDataType)2, 20213 },
	{ (Il2CppRGCTXDataType)2, 20214 },
	{ (Il2CppRGCTXDataType)2, 14885 },
	{ (Il2CppRGCTXDataType)2, 20215 },
	{ (Il2CppRGCTXDataType)2, 20216 },
	{ (Il2CppRGCTXDataType)2, 20217 },
	{ (Il2CppRGCTXDataType)2, 14887 },
	{ (Il2CppRGCTXDataType)2, 20218 },
	{ (Il2CppRGCTXDataType)2, 14889 },
	{ (Il2CppRGCTXDataType)2, 20219 },
	{ (Il2CppRGCTXDataType)3, 15244 },
	{ (Il2CppRGCTXDataType)2, 20220 },
	{ (Il2CppRGCTXDataType)2, 20221 },
	{ (Il2CppRGCTXDataType)2, 14892 },
	{ (Il2CppRGCTXDataType)2, 20222 },
	{ (Il2CppRGCTXDataType)2, 20223 },
	{ (Il2CppRGCTXDataType)2, 20224 },
	{ (Il2CppRGCTXDataType)2, 14894 },
	{ (Il2CppRGCTXDataType)2, 20225 },
	{ (Il2CppRGCTXDataType)2, 14896 },
	{ (Il2CppRGCTXDataType)2, 20226 },
	{ (Il2CppRGCTXDataType)3, 15245 },
	{ (Il2CppRGCTXDataType)2, 20227 },
	{ (Il2CppRGCTXDataType)2, 14901 },
	{ (Il2CppRGCTXDataType)2, 14903 },
	{ (Il2CppRGCTXDataType)2, 20228 },
	{ (Il2CppRGCTXDataType)3, 15246 },
	{ (Il2CppRGCTXDataType)2, 14906 },
	{ (Il2CppRGCTXDataType)2, 20229 },
	{ (Il2CppRGCTXDataType)3, 15247 },
	{ (Il2CppRGCTXDataType)2, 20230 },
	{ (Il2CppRGCTXDataType)2, 14909 },
	{ (Il2CppRGCTXDataType)2, 20231 },
	{ (Il2CppRGCTXDataType)3, 15248 },
	{ (Il2CppRGCTXDataType)3, 15249 },
	{ (Il2CppRGCTXDataType)2, 20232 },
	{ (Il2CppRGCTXDataType)2, 14913 },
	{ (Il2CppRGCTXDataType)2, 20233 },
	{ (Il2CppRGCTXDataType)2, 14915 },
	{ (Il2CppRGCTXDataType)3, 15250 },
	{ (Il2CppRGCTXDataType)3, 15251 },
	{ (Il2CppRGCTXDataType)2, 14918 },
	{ (Il2CppRGCTXDataType)3, 15252 },
	{ (Il2CppRGCTXDataType)3, 15253 },
	{ (Il2CppRGCTXDataType)2, 14930 },
	{ (Il2CppRGCTXDataType)2, 20234 },
	{ (Il2CppRGCTXDataType)3, 15254 },
	{ (Il2CppRGCTXDataType)3, 15255 },
	{ (Il2CppRGCTXDataType)2, 14932 },
	{ (Il2CppRGCTXDataType)2, 20073 },
	{ (Il2CppRGCTXDataType)3, 15256 },
	{ (Il2CppRGCTXDataType)3, 15257 },
	{ (Il2CppRGCTXDataType)2, 20235 },
	{ (Il2CppRGCTXDataType)3, 15258 },
	{ (Il2CppRGCTXDataType)3, 15259 },
	{ (Il2CppRGCTXDataType)2, 14942 },
	{ (Il2CppRGCTXDataType)2, 20236 },
	{ (Il2CppRGCTXDataType)3, 15260 },
	{ (Il2CppRGCTXDataType)3, 15261 },
	{ (Il2CppRGCTXDataType)3, 14689 },
	{ (Il2CppRGCTXDataType)3, 15262 },
	{ (Il2CppRGCTXDataType)2, 20237 },
	{ (Il2CppRGCTXDataType)3, 15263 },
	{ (Il2CppRGCTXDataType)3, 15264 },
	{ (Il2CppRGCTXDataType)2, 14954 },
	{ (Il2CppRGCTXDataType)2, 20238 },
	{ (Il2CppRGCTXDataType)3, 15265 },
	{ (Il2CppRGCTXDataType)3, 15266 },
	{ (Il2CppRGCTXDataType)3, 15267 },
	{ (Il2CppRGCTXDataType)3, 15268 },
	{ (Il2CppRGCTXDataType)3, 15269 },
	{ (Il2CppRGCTXDataType)3, 14695 },
	{ (Il2CppRGCTXDataType)3, 15270 },
	{ (Il2CppRGCTXDataType)2, 20239 },
	{ (Il2CppRGCTXDataType)3, 15271 },
	{ (Il2CppRGCTXDataType)3, 15272 },
	{ (Il2CppRGCTXDataType)2, 14967 },
	{ (Il2CppRGCTXDataType)2, 20240 },
	{ (Il2CppRGCTXDataType)3, 15273 },
	{ (Il2CppRGCTXDataType)3, 15274 },
	{ (Il2CppRGCTXDataType)2, 14969 },
	{ (Il2CppRGCTXDataType)2, 20241 },
	{ (Il2CppRGCTXDataType)3, 15275 },
	{ (Il2CppRGCTXDataType)3, 15276 },
	{ (Il2CppRGCTXDataType)2, 20242 },
	{ (Il2CppRGCTXDataType)3, 15277 },
	{ (Il2CppRGCTXDataType)3, 15278 },
	{ (Il2CppRGCTXDataType)2, 20243 },
	{ (Il2CppRGCTXDataType)3, 15279 },
	{ (Il2CppRGCTXDataType)3, 15280 },
	{ (Il2CppRGCTXDataType)2, 14984 },
	{ (Il2CppRGCTXDataType)2, 20244 },
	{ (Il2CppRGCTXDataType)3, 15281 },
	{ (Il2CppRGCTXDataType)3, 15282 },
	{ (Il2CppRGCTXDataType)3, 15283 },
	{ (Il2CppRGCTXDataType)3, 14706 },
	{ (Il2CppRGCTXDataType)2, 20245 },
	{ (Il2CppRGCTXDataType)3, 15284 },
	{ (Il2CppRGCTXDataType)3, 15285 },
	{ (Il2CppRGCTXDataType)2, 20246 },
	{ (Il2CppRGCTXDataType)3, 15286 },
	{ (Il2CppRGCTXDataType)3, 15287 },
	{ (Il2CppRGCTXDataType)2, 15000 },
	{ (Il2CppRGCTXDataType)2, 20247 },
	{ (Il2CppRGCTXDataType)3, 15288 },
	{ (Il2CppRGCTXDataType)3, 15289 },
	{ (Il2CppRGCTXDataType)3, 15290 },
	{ (Il2CppRGCTXDataType)3, 15291 },
	{ (Il2CppRGCTXDataType)3, 15292 },
	{ (Il2CppRGCTXDataType)3, 15293 },
	{ (Il2CppRGCTXDataType)3, 14712 },
	{ (Il2CppRGCTXDataType)2, 20248 },
	{ (Il2CppRGCTXDataType)3, 15294 },
	{ (Il2CppRGCTXDataType)3, 15295 },
	{ (Il2CppRGCTXDataType)2, 20249 },
	{ (Il2CppRGCTXDataType)3, 15296 },
	{ (Il2CppRGCTXDataType)3, 15297 },
	{ (Il2CppRGCTXDataType)3, 15298 },
	{ (Il2CppRGCTXDataType)3, 15299 },
	{ (Il2CppRGCTXDataType)3, 15300 },
	{ (Il2CppRGCTXDataType)3, 15301 },
	{ (Il2CppRGCTXDataType)2, 20250 },
	{ (Il2CppRGCTXDataType)2, 20251 },
	{ (Il2CppRGCTXDataType)3, 15302 },
	{ (Il2CppRGCTXDataType)2, 15035 },
	{ (Il2CppRGCTXDataType)2, 15029 },
	{ (Il2CppRGCTXDataType)3, 15303 },
	{ (Il2CppRGCTXDataType)2, 15028 },
	{ (Il2CppRGCTXDataType)2, 20252 },
	{ (Il2CppRGCTXDataType)3, 15304 },
	{ (Il2CppRGCTXDataType)3, 15305 },
	{ (Il2CppRGCTXDataType)3, 15306 },
	{ (Il2CppRGCTXDataType)3, 15307 },
	{ (Il2CppRGCTXDataType)2, 20253 },
	{ (Il2CppRGCTXDataType)3, 15308 },
	{ (Il2CppRGCTXDataType)2, 15051 },
	{ (Il2CppRGCTXDataType)2, 15043 },
	{ (Il2CppRGCTXDataType)3, 15309 },
	{ (Il2CppRGCTXDataType)3, 15310 },
	{ (Il2CppRGCTXDataType)2, 15042 },
	{ (Il2CppRGCTXDataType)2, 20254 },
	{ (Il2CppRGCTXDataType)3, 15311 },
	{ (Il2CppRGCTXDataType)3, 15312 },
	{ (Il2CppRGCTXDataType)3, 15313 },
	{ (Il2CppRGCTXDataType)2, 15055 },
	{ (Il2CppRGCTXDataType)3, 15314 },
	{ (Il2CppRGCTXDataType)2, 20255 },
	{ (Il2CppRGCTXDataType)3, 15315 },
	{ (Il2CppRGCTXDataType)3, 15316 },
	{ (Il2CppRGCTXDataType)2, 20256 },
	{ (Il2CppRGCTXDataType)2, 20257 },
	{ (Il2CppRGCTXDataType)2, 20258 },
	{ (Il2CppRGCTXDataType)3, 15317 },
	{ (Il2CppRGCTXDataType)2, 15067 },
	{ (Il2CppRGCTXDataType)3, 15318 },
	{ (Il2CppRGCTXDataType)2, 20259 },
	{ (Il2CppRGCTXDataType)3, 15319 },
	{ (Il2CppRGCTXDataType)2, 20259 },
	{ (Il2CppRGCTXDataType)2, 15095 },
	{ (Il2CppRGCTXDataType)3, 15320 },
	{ (Il2CppRGCTXDataType)3, 15321 },
	{ (Il2CppRGCTXDataType)3, 15322 },
	{ (Il2CppRGCTXDataType)3, 15323 },
	{ (Il2CppRGCTXDataType)2, 20260 },
	{ (Il2CppRGCTXDataType)2, 20261 },
	{ (Il2CppRGCTXDataType)2, 20262 },
	{ (Il2CppRGCTXDataType)3, 15324 },
	{ (Il2CppRGCTXDataType)3, 15325 },
	{ (Il2CppRGCTXDataType)2, 15091 },
	{ (Il2CppRGCTXDataType)2, 15094 },
	{ (Il2CppRGCTXDataType)3, 15326 },
	{ (Il2CppRGCTXDataType)3, 15327 },
	{ (Il2CppRGCTXDataType)2, 15098 },
	{ (Il2CppRGCTXDataType)3, 15328 },
	{ (Il2CppRGCTXDataType)2, 20263 },
	{ (Il2CppRGCTXDataType)2, 15088 },
	{ (Il2CppRGCTXDataType)2, 20264 },
	{ (Il2CppRGCTXDataType)3, 15329 },
	{ (Il2CppRGCTXDataType)3, 15330 },
	{ (Il2CppRGCTXDataType)3, 15331 },
	{ (Il2CppRGCTXDataType)2, 20265 },
	{ (Il2CppRGCTXDataType)3, 15332 },
	{ (Il2CppRGCTXDataType)3, 15333 },
	{ (Il2CppRGCTXDataType)3, 15334 },
	{ (Il2CppRGCTXDataType)2, 15113 },
	{ (Il2CppRGCTXDataType)3, 15335 },
	{ (Il2CppRGCTXDataType)2, 20266 },
	{ (Il2CppRGCTXDataType)2, 20267 },
	{ (Il2CppRGCTXDataType)3, 15336 },
	{ (Il2CppRGCTXDataType)3, 15337 },
	{ (Il2CppRGCTXDataType)2, 15134 },
	{ (Il2CppRGCTXDataType)3, 15338 },
	{ (Il2CppRGCTXDataType)2, 15135 },
	{ (Il2CppRGCTXDataType)3, 15339 },
	{ (Il2CppRGCTXDataType)2, 20268 },
	{ (Il2CppRGCTXDataType)3, 15340 },
	{ (Il2CppRGCTXDataType)3, 15341 },
	{ (Il2CppRGCTXDataType)2, 20269 },
	{ (Il2CppRGCTXDataType)3, 15342 },
	{ (Il2CppRGCTXDataType)3, 15343 },
	{ (Il2CppRGCTXDataType)2, 20270 },
	{ (Il2CppRGCTXDataType)3, 15344 },
	{ (Il2CppRGCTXDataType)2, 20271 },
	{ (Il2CppRGCTXDataType)3, 15345 },
	{ (Il2CppRGCTXDataType)3, 15346 },
	{ (Il2CppRGCTXDataType)3, 15347 },
	{ (Il2CppRGCTXDataType)2, 15170 },
	{ (Il2CppRGCTXDataType)3, 15348 },
	{ (Il2CppRGCTXDataType)2, 15178 },
	{ (Il2CppRGCTXDataType)3, 15349 },
	{ (Il2CppRGCTXDataType)2, 20272 },
	{ (Il2CppRGCTXDataType)2, 20273 },
	{ (Il2CppRGCTXDataType)3, 15350 },
	{ (Il2CppRGCTXDataType)3, 15351 },
	{ (Il2CppRGCTXDataType)3, 15352 },
	{ (Il2CppRGCTXDataType)3, 15353 },
	{ (Il2CppRGCTXDataType)3, 15354 },
	{ (Il2CppRGCTXDataType)3, 15355 },
	{ (Il2CppRGCTXDataType)2, 15194 },
	{ (Il2CppRGCTXDataType)2, 20274 },
	{ (Il2CppRGCTXDataType)3, 15356 },
	{ (Il2CppRGCTXDataType)3, 15357 },
	{ (Il2CppRGCTXDataType)2, 15198 },
	{ (Il2CppRGCTXDataType)3, 15358 },
	{ (Il2CppRGCTXDataType)2, 20275 },
	{ (Il2CppRGCTXDataType)2, 15208 },
	{ (Il2CppRGCTXDataType)2, 15206 },
	{ (Il2CppRGCTXDataType)2, 20276 },
	{ (Il2CppRGCTXDataType)3, 15359 },
	{ (Il2CppRGCTXDataType)2, 20277 },
	{ (Il2CppRGCTXDataType)3, 15360 },
	{ (Il2CppRGCTXDataType)3, 15361 },
	{ (Il2CppRGCTXDataType)3, 15362 },
	{ (Il2CppRGCTXDataType)2, 15212 },
	{ (Il2CppRGCTXDataType)3, 15363 },
	{ (Il2CppRGCTXDataType)3, 15364 },
	{ (Il2CppRGCTXDataType)2, 15215 },
	{ (Il2CppRGCTXDataType)3, 15365 },
	{ (Il2CppRGCTXDataType)1, 20278 },
	{ (Il2CppRGCTXDataType)2, 15214 },
	{ (Il2CppRGCTXDataType)3, 15366 },
	{ (Il2CppRGCTXDataType)1, 15214 },
	{ (Il2CppRGCTXDataType)1, 15212 },
	{ (Il2CppRGCTXDataType)2, 20279 },
	{ (Il2CppRGCTXDataType)2, 15214 },
	{ (Il2CppRGCTXDataType)3, 15367 },
	{ (Il2CppRGCTXDataType)3, 15368 },
	{ (Il2CppRGCTXDataType)3, 15369 },
	{ (Il2CppRGCTXDataType)2, 15213 },
	{ (Il2CppRGCTXDataType)3, 15370 },
	{ (Il2CppRGCTXDataType)2, 15226 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	214,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	67,
	s_rgctxIndices,
	315,
	s_rgctxValues,
	NULL,
};
