﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericVirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct GenericInterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj, T1 p1)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// DG.Tweening.Core.DOGetter`1<UnityEngine.Vector3>
struct DOGetter_1_t6A6DB6A183AD6D7F4B2F3656C4AB9326994D8CA4;
// DG.Tweening.Core.DOSetter`1<UnityEngine.Vector3>
struct DOSetter_1_t528C4470268C88CABD1BFDF039B98D72E012AF46;
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B;
// DG.Tweening.EaseFunction
struct EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691;
// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678;
// DG.Tweening.Sequence
struct Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2;
// DG.Tweening.TweenCallback
struct TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83;
// DG.Tweening.TweenCallback`1<System.Int32>
struct TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1;
// GameController
struct GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3;
// GameController/CurrentLevelData
struct CurrentLevelData_tE484B76B20083ADBD407E6CEAD859634F191F518;
// GameController/OnGameOver
struct OnGameOver_t076BD5C9FC5BA0655BA9845C8153DD1B72C995EE;
// GameController/OnGameStart
struct OnGameStart_t3BCD32C4083A49DF55CE8B92FE83464FBB1C4353;
// GameController/OnInitializeGameUI
struct OnInitializeGameUI_tB65E856A433F3B0EF1922D1F56B23B45D0664314;
// GameController/OnLevelUp
struct OnLevelUp_t52A7FC3E266DE444FA52B33423F2991AC4FDA92F;
// GameController/OnPlayAgain
struct OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611;
// GameController/OnPlaying
struct OnPlaying_t5B0880EE1DA7CB41A933143A651E58CBC338BF1B;
// GameController/OnShowGiftbox
struct OnShowGiftbox_t770AC8537873656001D91D013912642C3E50751B;
// GameController/OnShowPlayAgain
struct OnShowPlayAgain_t697DAB214BF0D0CC20A314594A1E4F1046C82118;
// Levels
struct Levels_tD642EA54B7651EAF0091AE53B3E74D05EAB7EC7B;
// MessageHandler
struct MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840;
// Newtonsoft.Json.Linq.JObject
struct JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B;
// Newtonsoft.Json.Linq.JToken
struct JToken_tCCEF558996D47101E43F6436A874C249291581AA;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage>
struct Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8;
// System.Delegate
struct Delegate_t;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TMP_Text
struct TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityMessage
struct UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4;
// UnityMessageManager
struct UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F;
// UnityMessageManager/MessageDelegate
struct MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E;
// UnityMessageManager/MessageHandlerDelegate
struct MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101;
// Verification
struct Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA;
// Verification/CreditVerified
struct CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral3618FB1497957BF530D16396449F5D7D96A59856;
IL2CPP_EXTERN_C String_t* _stringLiteral65279E1ECF00D0D1177A5B3B458FD187A1D6135E;
IL2CPP_EXTERN_C String_t* _stringLiteralC69F263D9117BC3A30FE1031BED6E325F7B2FEBF;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_m96AF6FDC6BC35E18F78A5510ECA18A01BA312B57_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TweenSettingsExtensions_SetEase_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_mD0FE09A1EFC67CFB28555BE2C84249EB301DB47F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Verification_U3CVerificationMessageU3Eb__11_0_mF013FC22CF55ECA6FE3AF190613E520947622178_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260AssemblyU2DCSharp1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16AssemblyU2DCSharp1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_Awake_m4CD0EE2E0009E58A617FA08FA6D449DF1B2E226D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_Destroy_m8607EFAAF4C6E7D28461C5B10AFED955AD49C446_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_OnDisable_m18806AEBCF408EB9266CE92A75C8F7F9407BF21B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_OnEnable_mE50987A371FD54F5292E20F154AC2A490D011107_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_VerificationMessage_m3C94AE37F59AB2F5AF0B18A9D95BAD98B6188E1F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// MessageHandler
struct  MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840  : public RuntimeObject
{
public:
	// System.Int32 MessageHandler::id
	int32_t ___id_0;
	// System.String MessageHandler::seq
	String_t* ___seq_1;
	// System.String MessageHandler::name
	String_t* ___name_2;
	// Newtonsoft.Json.Linq.JToken MessageHandler::data
	JToken_tCCEF558996D47101E43F6436A874C249291581AA * ___data_3;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___id_0)); }
	inline int32_t get_id_0() const { return ___id_0; }
	inline int32_t* get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(int32_t value)
	{
		___id_0 = value;
	}

	inline static int32_t get_offset_of_seq_1() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___seq_1)); }
	inline String_t* get_seq_1() const { return ___seq_1; }
	inline String_t** get_address_of_seq_1() { return &___seq_1; }
	inline void set_seq_1(String_t* value)
	{
		___seq_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___seq_1), (void*)value);
	}

	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_2), (void*)value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840, ___data_3)); }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA * get_data_3() const { return ___data_3; }
	inline JToken_tCCEF558996D47101E43F6436A874C249291581AA ** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(JToken_tCCEF558996D47101E43F6436A874C249291581AA * value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_3), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// UnityMessage
struct  UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4  : public RuntimeObject
{
public:
	// System.String UnityMessage::name
	String_t* ___name_0;
	// Newtonsoft.Json.Linq.JObject UnityMessage::data
	JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * ___data_1;
	// System.Action`1<System.Object> UnityMessage::callBack
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___callBack_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___data_1)); }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * get_data_1() const { return ___data_1; }
	inline JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(JObject_t2CA4A682579D239DA86B57BCE30CED02C7B8E30B * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_1), (void*)value);
	}

	inline static int32_t get_offset_of_callBack_2() { return static_cast<int32_t>(offsetof(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4, ___callBack_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_callBack_2() const { return ___callBack_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_callBack_2() { return &___callBack_2; }
	inline void set_callBack_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___callBack_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___callBack_2), (void*)value);
	}
};


// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// DG.Tweening.AxisConstraint
struct  AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A 
{
public:
	// System.Int32 DG.Tweening.AxisConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AxisConstraint_t6929580E28515C207FCC805981F73EC49354E48A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Core.Enums.SpecialStartupMode
struct  SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49 
{
public:
	// System.Int32 DG.Tweening.Core.Enums.SpecialStartupMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialStartupMode_tE272D2FEC46F4A5FC68E9A99E0093494AA4E0E49, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.Ease
struct  Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E 
{
public:
	// System.Int32 DG.Tweening.Ease::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Ease_tC8B7BB21FCD1947DD4A1DD8025D4E9C6CC01DA0E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.LoopType
struct  LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A 
{
public:
	// System.Int32 DG.Tweening.LoopType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopType_t1F864F717700724AC772E79CCF167C3C44D6EE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.TweenType
struct  TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F 
{
public:
	// System.Int32 DG.Tweening.TweenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TweenType_t13FCD3DFD7DD8AE49EA4A7BA70695D3936EAF71F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DG.Tweening.UpdateType
struct  UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63 
{
public:
	// System.Int32 DG.Tweening.UpdateType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UpdateType_t0EBAB258B7D72D4959A3835E554B7958CAFDAF63, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GameController_GameStatus
struct  GameStatus_t9229A65A94E51787DF8541D7FC168A240D96B81B 
{
public:
	// System.Int32 GameController_GameStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameStatus_t9229A65A94E51787DF8541D7FC168A240D96B81B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.Core.ABSSequentiable
struct  ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757  : public RuntimeObject
{
public:
	// DG.Tweening.TweenType DG.Tweening.Core.ABSSequentiable::tweenType
	int32_t ___tweenType_0;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedPosition
	float ___sequencedPosition_1;
	// System.Single DG.Tweening.Core.ABSSequentiable::sequencedEndPosition
	float ___sequencedEndPosition_2;
	// DG.Tweening.TweenCallback DG.Tweening.Core.ABSSequentiable::onStart
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onStart_3;

public:
	inline static int32_t get_offset_of_tweenType_0() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___tweenType_0)); }
	inline int32_t get_tweenType_0() const { return ___tweenType_0; }
	inline int32_t* get_address_of_tweenType_0() { return &___tweenType_0; }
	inline void set_tweenType_0(int32_t value)
	{
		___tweenType_0 = value;
	}

	inline static int32_t get_offset_of_sequencedPosition_1() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___sequencedPosition_1)); }
	inline float get_sequencedPosition_1() const { return ___sequencedPosition_1; }
	inline float* get_address_of_sequencedPosition_1() { return &___sequencedPosition_1; }
	inline void set_sequencedPosition_1(float value)
	{
		___sequencedPosition_1 = value;
	}

	inline static int32_t get_offset_of_sequencedEndPosition_2() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___sequencedEndPosition_2)); }
	inline float get_sequencedEndPosition_2() const { return ___sequencedEndPosition_2; }
	inline float* get_address_of_sequencedEndPosition_2() { return &___sequencedEndPosition_2; }
	inline void set_sequencedEndPosition_2(float value)
	{
		___sequencedEndPosition_2 = value;
	}

	inline static int32_t get_offset_of_onStart_3() { return static_cast<int32_t>(offsetof(ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757, ___onStart_3)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onStart_3() const { return ___onStart_3; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onStart_3() { return &___onStart_3; }
	inline void set_onStart_3(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onStart_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStart_3), (void*)value);
	}
};


// DG.Tweening.Plugins.Options.VectorOptions
struct  VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322 
{
public:
	// DG.Tweening.AxisConstraint DG.Tweening.Plugins.Options.VectorOptions::axisConstraint
	int32_t ___axisConstraint_0;
	// System.Boolean DG.Tweening.Plugins.Options.VectorOptions::snapping
	bool ___snapping_1;

public:
	inline static int32_t get_offset_of_axisConstraint_0() { return static_cast<int32_t>(offsetof(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322, ___axisConstraint_0)); }
	inline int32_t get_axisConstraint_0() const { return ___axisConstraint_0; }
	inline int32_t* get_address_of_axisConstraint_0() { return &___axisConstraint_0; }
	inline void set_axisConstraint_0(int32_t value)
	{
		___axisConstraint_0 = value;
	}

	inline static int32_t get_offset_of_snapping_1() { return static_cast<int32_t>(offsetof(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322, ___snapping_1)); }
	inline bool get_snapping_1() const { return ___snapping_1; }
	inline bool* get_address_of_snapping_1() { return &___snapping_1; }
	inline void set_snapping_1(bool value)
	{
		___snapping_1 = value;
	}
};

// Native definition for P/Invoke marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_marshaled_pinvoke
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};
// Native definition for COM marshalling of DG.Tweening.Plugins.Options.VectorOptions
struct VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322_marshaled_com
{
	int32_t ___axisConstraint_0;
	int32_t ___snapping_1;
};

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// DG.Tweening.Tween
struct  Tween_t119487E0AB84EF563521F1043116BDBAE68AC437  : public ABSSequentiable_tDA1366907669973CC0BB553EF4159D45FC46A757
{
public:
	// System.Single DG.Tweening.Tween::timeScale
	float ___timeScale_4;
	// System.Boolean DG.Tweening.Tween::isBackwards
	bool ___isBackwards_5;
	// System.Object DG.Tweening.Tween::id
	RuntimeObject * ___id_6;
	// System.String DG.Tweening.Tween::stringId
	String_t* ___stringId_7;
	// System.Int32 DG.Tweening.Tween::intId
	int32_t ___intId_8;
	// System.Object DG.Tweening.Tween::target
	RuntimeObject * ___target_9;
	// DG.Tweening.UpdateType DG.Tweening.Tween::updateType
	int32_t ___updateType_10;
	// System.Boolean DG.Tweening.Tween::isIndependentUpdate
	bool ___isIndependentUpdate_11;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPlay
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onPlay_12;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onPause
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onPause_13;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onRewind
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onRewind_14;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onUpdate
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onUpdate_15;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onStepComplete
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onStepComplete_16;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onComplete
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onComplete_17;
	// DG.Tweening.TweenCallback DG.Tweening.Tween::onKill
	TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * ___onKill_18;
	// DG.Tweening.TweenCallback`1<System.Int32> DG.Tweening.Tween::onWaypointChange
	TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * ___onWaypointChange_19;
	// System.Boolean DG.Tweening.Tween::isFrom
	bool ___isFrom_20;
	// System.Boolean DG.Tweening.Tween::isBlendable
	bool ___isBlendable_21;
	// System.Boolean DG.Tweening.Tween::isRecyclable
	bool ___isRecyclable_22;
	// System.Boolean DG.Tweening.Tween::isSpeedBased
	bool ___isSpeedBased_23;
	// System.Boolean DG.Tweening.Tween::autoKill
	bool ___autoKill_24;
	// System.Single DG.Tweening.Tween::duration
	float ___duration_25;
	// System.Int32 DG.Tweening.Tween::loops
	int32_t ___loops_26;
	// DG.Tweening.LoopType DG.Tweening.Tween::loopType
	int32_t ___loopType_27;
	// System.Single DG.Tweening.Tween::delay
	float ___delay_28;
	// System.Boolean DG.Tweening.Tween::<isRelative>k__BackingField
	bool ___U3CisRelativeU3Ek__BackingField_29;
	// DG.Tweening.Ease DG.Tweening.Tween::easeType
	int32_t ___easeType_30;
	// DG.Tweening.EaseFunction DG.Tweening.Tween::customEase
	EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * ___customEase_31;
	// System.Single DG.Tweening.Tween::easeOvershootOrAmplitude
	float ___easeOvershootOrAmplitude_32;
	// System.Single DG.Tweening.Tween::easePeriod
	float ___easePeriod_33;
	// System.Type DG.Tweening.Tween::typeofT1
	Type_t * ___typeofT1_34;
	// System.Type DG.Tweening.Tween::typeofT2
	Type_t * ___typeofT2_35;
	// System.Type DG.Tweening.Tween::typeofTPlugOptions
	Type_t * ___typeofTPlugOptions_36;
	// System.Boolean DG.Tweening.Tween::<active>k__BackingField
	bool ___U3CactiveU3Ek__BackingField_37;
	// System.Boolean DG.Tweening.Tween::isSequenced
	bool ___isSequenced_38;
	// DG.Tweening.Sequence DG.Tweening.Tween::sequenceParent
	Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * ___sequenceParent_39;
	// System.Int32 DG.Tweening.Tween::activeId
	int32_t ___activeId_40;
	// DG.Tweening.Core.Enums.SpecialStartupMode DG.Tweening.Tween::specialStartupMode
	int32_t ___specialStartupMode_41;
	// System.Boolean DG.Tweening.Tween::creationLocked
	bool ___creationLocked_42;
	// System.Boolean DG.Tweening.Tween::startupDone
	bool ___startupDone_43;
	// System.Boolean DG.Tweening.Tween::<playedOnce>k__BackingField
	bool ___U3CplayedOnceU3Ek__BackingField_44;
	// System.Single DG.Tweening.Tween::<position>k__BackingField
	float ___U3CpositionU3Ek__BackingField_45;
	// System.Single DG.Tweening.Tween::fullDuration
	float ___fullDuration_46;
	// System.Int32 DG.Tweening.Tween::completedLoops
	int32_t ___completedLoops_47;
	// System.Boolean DG.Tweening.Tween::isPlaying
	bool ___isPlaying_48;
	// System.Boolean DG.Tweening.Tween::isComplete
	bool ___isComplete_49;
	// System.Single DG.Tweening.Tween::elapsedDelay
	float ___elapsedDelay_50;
	// System.Boolean DG.Tweening.Tween::delayComplete
	bool ___delayComplete_51;
	// System.Int32 DG.Tweening.Tween::miscInt
	int32_t ___miscInt_52;

public:
	inline static int32_t get_offset_of_timeScale_4() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___timeScale_4)); }
	inline float get_timeScale_4() const { return ___timeScale_4; }
	inline float* get_address_of_timeScale_4() { return &___timeScale_4; }
	inline void set_timeScale_4(float value)
	{
		___timeScale_4 = value;
	}

	inline static int32_t get_offset_of_isBackwards_5() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isBackwards_5)); }
	inline bool get_isBackwards_5() const { return ___isBackwards_5; }
	inline bool* get_address_of_isBackwards_5() { return &___isBackwards_5; }
	inline void set_isBackwards_5(bool value)
	{
		___isBackwards_5 = value;
	}

	inline static int32_t get_offset_of_id_6() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___id_6)); }
	inline RuntimeObject * get_id_6() const { return ___id_6; }
	inline RuntimeObject ** get_address_of_id_6() { return &___id_6; }
	inline void set_id_6(RuntimeObject * value)
	{
		___id_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_6), (void*)value);
	}

	inline static int32_t get_offset_of_stringId_7() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___stringId_7)); }
	inline String_t* get_stringId_7() const { return ___stringId_7; }
	inline String_t** get_address_of_stringId_7() { return &___stringId_7; }
	inline void set_stringId_7(String_t* value)
	{
		___stringId_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stringId_7), (void*)value);
	}

	inline static int32_t get_offset_of_intId_8() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___intId_8)); }
	inline int32_t get_intId_8() const { return ___intId_8; }
	inline int32_t* get_address_of_intId_8() { return &___intId_8; }
	inline void set_intId_8(int32_t value)
	{
		___intId_8 = value;
	}

	inline static int32_t get_offset_of_target_9() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___target_9)); }
	inline RuntimeObject * get_target_9() const { return ___target_9; }
	inline RuntimeObject ** get_address_of_target_9() { return &___target_9; }
	inline void set_target_9(RuntimeObject * value)
	{
		___target_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_9), (void*)value);
	}

	inline static int32_t get_offset_of_updateType_10() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___updateType_10)); }
	inline int32_t get_updateType_10() const { return ___updateType_10; }
	inline int32_t* get_address_of_updateType_10() { return &___updateType_10; }
	inline void set_updateType_10(int32_t value)
	{
		___updateType_10 = value;
	}

	inline static int32_t get_offset_of_isIndependentUpdate_11() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isIndependentUpdate_11)); }
	inline bool get_isIndependentUpdate_11() const { return ___isIndependentUpdate_11; }
	inline bool* get_address_of_isIndependentUpdate_11() { return &___isIndependentUpdate_11; }
	inline void set_isIndependentUpdate_11(bool value)
	{
		___isIndependentUpdate_11 = value;
	}

	inline static int32_t get_offset_of_onPlay_12() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onPlay_12)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onPlay_12() const { return ___onPlay_12; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onPlay_12() { return &___onPlay_12; }
	inline void set_onPlay_12(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onPlay_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlay_12), (void*)value);
	}

	inline static int32_t get_offset_of_onPause_13() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onPause_13)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onPause_13() const { return ___onPause_13; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onPause_13() { return &___onPause_13; }
	inline void set_onPause_13(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onPause_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPause_13), (void*)value);
	}

	inline static int32_t get_offset_of_onRewind_14() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onRewind_14)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onRewind_14() const { return ___onRewind_14; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onRewind_14() { return &___onRewind_14; }
	inline void set_onRewind_14(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onRewind_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onRewind_14), (void*)value);
	}

	inline static int32_t get_offset_of_onUpdate_15() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onUpdate_15)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onUpdate_15() const { return ___onUpdate_15; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onUpdate_15() { return &___onUpdate_15; }
	inline void set_onUpdate_15(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onUpdate_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onUpdate_15), (void*)value);
	}

	inline static int32_t get_offset_of_onStepComplete_16() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onStepComplete_16)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onStepComplete_16() const { return ___onStepComplete_16; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onStepComplete_16() { return &___onStepComplete_16; }
	inline void set_onStepComplete_16(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onStepComplete_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onStepComplete_16), (void*)value);
	}

	inline static int32_t get_offset_of_onComplete_17() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onComplete_17)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onComplete_17() const { return ___onComplete_17; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onComplete_17() { return &___onComplete_17; }
	inline void set_onComplete_17(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onComplete_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onComplete_17), (void*)value);
	}

	inline static int32_t get_offset_of_onKill_18() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onKill_18)); }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * get_onKill_18() const { return ___onKill_18; }
	inline TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 ** get_address_of_onKill_18() { return &___onKill_18; }
	inline void set_onKill_18(TweenCallback_tD2CEE8D219857E8D988DBEC4D2DE19FA5FCB3E83 * value)
	{
		___onKill_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onKill_18), (void*)value);
	}

	inline static int32_t get_offset_of_onWaypointChange_19() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___onWaypointChange_19)); }
	inline TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * get_onWaypointChange_19() const { return ___onWaypointChange_19; }
	inline TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 ** get_address_of_onWaypointChange_19() { return &___onWaypointChange_19; }
	inline void set_onWaypointChange_19(TweenCallback_1_t46CB97F053453C4F41F8AE6213792813E455DDE1 * value)
	{
		___onWaypointChange_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onWaypointChange_19), (void*)value);
	}

	inline static int32_t get_offset_of_isFrom_20() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isFrom_20)); }
	inline bool get_isFrom_20() const { return ___isFrom_20; }
	inline bool* get_address_of_isFrom_20() { return &___isFrom_20; }
	inline void set_isFrom_20(bool value)
	{
		___isFrom_20 = value;
	}

	inline static int32_t get_offset_of_isBlendable_21() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isBlendable_21)); }
	inline bool get_isBlendable_21() const { return ___isBlendable_21; }
	inline bool* get_address_of_isBlendable_21() { return &___isBlendable_21; }
	inline void set_isBlendable_21(bool value)
	{
		___isBlendable_21 = value;
	}

	inline static int32_t get_offset_of_isRecyclable_22() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isRecyclable_22)); }
	inline bool get_isRecyclable_22() const { return ___isRecyclable_22; }
	inline bool* get_address_of_isRecyclable_22() { return &___isRecyclable_22; }
	inline void set_isRecyclable_22(bool value)
	{
		___isRecyclable_22 = value;
	}

	inline static int32_t get_offset_of_isSpeedBased_23() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isSpeedBased_23)); }
	inline bool get_isSpeedBased_23() const { return ___isSpeedBased_23; }
	inline bool* get_address_of_isSpeedBased_23() { return &___isSpeedBased_23; }
	inline void set_isSpeedBased_23(bool value)
	{
		___isSpeedBased_23 = value;
	}

	inline static int32_t get_offset_of_autoKill_24() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___autoKill_24)); }
	inline bool get_autoKill_24() const { return ___autoKill_24; }
	inline bool* get_address_of_autoKill_24() { return &___autoKill_24; }
	inline void set_autoKill_24(bool value)
	{
		___autoKill_24 = value;
	}

	inline static int32_t get_offset_of_duration_25() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___duration_25)); }
	inline float get_duration_25() const { return ___duration_25; }
	inline float* get_address_of_duration_25() { return &___duration_25; }
	inline void set_duration_25(float value)
	{
		___duration_25 = value;
	}

	inline static int32_t get_offset_of_loops_26() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___loops_26)); }
	inline int32_t get_loops_26() const { return ___loops_26; }
	inline int32_t* get_address_of_loops_26() { return &___loops_26; }
	inline void set_loops_26(int32_t value)
	{
		___loops_26 = value;
	}

	inline static int32_t get_offset_of_loopType_27() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___loopType_27)); }
	inline int32_t get_loopType_27() const { return ___loopType_27; }
	inline int32_t* get_address_of_loopType_27() { return &___loopType_27; }
	inline void set_loopType_27(int32_t value)
	{
		___loopType_27 = value;
	}

	inline static int32_t get_offset_of_delay_28() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___delay_28)); }
	inline float get_delay_28() const { return ___delay_28; }
	inline float* get_address_of_delay_28() { return &___delay_28; }
	inline void set_delay_28(float value)
	{
		___delay_28 = value;
	}

	inline static int32_t get_offset_of_U3CisRelativeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___U3CisRelativeU3Ek__BackingField_29)); }
	inline bool get_U3CisRelativeU3Ek__BackingField_29() const { return ___U3CisRelativeU3Ek__BackingField_29; }
	inline bool* get_address_of_U3CisRelativeU3Ek__BackingField_29() { return &___U3CisRelativeU3Ek__BackingField_29; }
	inline void set_U3CisRelativeU3Ek__BackingField_29(bool value)
	{
		___U3CisRelativeU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_easeType_30() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easeType_30)); }
	inline int32_t get_easeType_30() const { return ___easeType_30; }
	inline int32_t* get_address_of_easeType_30() { return &___easeType_30; }
	inline void set_easeType_30(int32_t value)
	{
		___easeType_30 = value;
	}

	inline static int32_t get_offset_of_customEase_31() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___customEase_31)); }
	inline EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * get_customEase_31() const { return ___customEase_31; }
	inline EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 ** get_address_of_customEase_31() { return &___customEase_31; }
	inline void set_customEase_31(EaseFunction_tAC315FE3B057AC8DA87991C785F2595F3B761691 * value)
	{
		___customEase_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___customEase_31), (void*)value);
	}

	inline static int32_t get_offset_of_easeOvershootOrAmplitude_32() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easeOvershootOrAmplitude_32)); }
	inline float get_easeOvershootOrAmplitude_32() const { return ___easeOvershootOrAmplitude_32; }
	inline float* get_address_of_easeOvershootOrAmplitude_32() { return &___easeOvershootOrAmplitude_32; }
	inline void set_easeOvershootOrAmplitude_32(float value)
	{
		___easeOvershootOrAmplitude_32 = value;
	}

	inline static int32_t get_offset_of_easePeriod_33() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___easePeriod_33)); }
	inline float get_easePeriod_33() const { return ___easePeriod_33; }
	inline float* get_address_of_easePeriod_33() { return &___easePeriod_33; }
	inline void set_easePeriod_33(float value)
	{
		___easePeriod_33 = value;
	}

	inline static int32_t get_offset_of_typeofT1_34() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofT1_34)); }
	inline Type_t * get_typeofT1_34() const { return ___typeofT1_34; }
	inline Type_t ** get_address_of_typeofT1_34() { return &___typeofT1_34; }
	inline void set_typeofT1_34(Type_t * value)
	{
		___typeofT1_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT1_34), (void*)value);
	}

	inline static int32_t get_offset_of_typeofT2_35() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofT2_35)); }
	inline Type_t * get_typeofT2_35() const { return ___typeofT2_35; }
	inline Type_t ** get_address_of_typeofT2_35() { return &___typeofT2_35; }
	inline void set_typeofT2_35(Type_t * value)
	{
		___typeofT2_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofT2_35), (void*)value);
	}

	inline static int32_t get_offset_of_typeofTPlugOptions_36() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___typeofTPlugOptions_36)); }
	inline Type_t * get_typeofTPlugOptions_36() const { return ___typeofTPlugOptions_36; }
	inline Type_t ** get_address_of_typeofTPlugOptions_36() { return &___typeofTPlugOptions_36; }
	inline void set_typeofTPlugOptions_36(Type_t * value)
	{
		___typeofTPlugOptions_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___typeofTPlugOptions_36), (void*)value);
	}

	inline static int32_t get_offset_of_U3CactiveU3Ek__BackingField_37() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___U3CactiveU3Ek__BackingField_37)); }
	inline bool get_U3CactiveU3Ek__BackingField_37() const { return ___U3CactiveU3Ek__BackingField_37; }
	inline bool* get_address_of_U3CactiveU3Ek__BackingField_37() { return &___U3CactiveU3Ek__BackingField_37; }
	inline void set_U3CactiveU3Ek__BackingField_37(bool value)
	{
		___U3CactiveU3Ek__BackingField_37 = value;
	}

	inline static int32_t get_offset_of_isSequenced_38() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isSequenced_38)); }
	inline bool get_isSequenced_38() const { return ___isSequenced_38; }
	inline bool* get_address_of_isSequenced_38() { return &___isSequenced_38; }
	inline void set_isSequenced_38(bool value)
	{
		___isSequenced_38 = value;
	}

	inline static int32_t get_offset_of_sequenceParent_39() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___sequenceParent_39)); }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * get_sequenceParent_39() const { return ___sequenceParent_39; }
	inline Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 ** get_address_of_sequenceParent_39() { return &___sequenceParent_39; }
	inline void set_sequenceParent_39(Sequence_tA6FA099F465933113D131DDC0EECB6A19A27BCB2 * value)
	{
		___sequenceParent_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sequenceParent_39), (void*)value);
	}

	inline static int32_t get_offset_of_activeId_40() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___activeId_40)); }
	inline int32_t get_activeId_40() const { return ___activeId_40; }
	inline int32_t* get_address_of_activeId_40() { return &___activeId_40; }
	inline void set_activeId_40(int32_t value)
	{
		___activeId_40 = value;
	}

	inline static int32_t get_offset_of_specialStartupMode_41() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___specialStartupMode_41)); }
	inline int32_t get_specialStartupMode_41() const { return ___specialStartupMode_41; }
	inline int32_t* get_address_of_specialStartupMode_41() { return &___specialStartupMode_41; }
	inline void set_specialStartupMode_41(int32_t value)
	{
		___specialStartupMode_41 = value;
	}

	inline static int32_t get_offset_of_creationLocked_42() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___creationLocked_42)); }
	inline bool get_creationLocked_42() const { return ___creationLocked_42; }
	inline bool* get_address_of_creationLocked_42() { return &___creationLocked_42; }
	inline void set_creationLocked_42(bool value)
	{
		___creationLocked_42 = value;
	}

	inline static int32_t get_offset_of_startupDone_43() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___startupDone_43)); }
	inline bool get_startupDone_43() const { return ___startupDone_43; }
	inline bool* get_address_of_startupDone_43() { return &___startupDone_43; }
	inline void set_startupDone_43(bool value)
	{
		___startupDone_43 = value;
	}

	inline static int32_t get_offset_of_U3CplayedOnceU3Ek__BackingField_44() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___U3CplayedOnceU3Ek__BackingField_44)); }
	inline bool get_U3CplayedOnceU3Ek__BackingField_44() const { return ___U3CplayedOnceU3Ek__BackingField_44; }
	inline bool* get_address_of_U3CplayedOnceU3Ek__BackingField_44() { return &___U3CplayedOnceU3Ek__BackingField_44; }
	inline void set_U3CplayedOnceU3Ek__BackingField_44(bool value)
	{
		___U3CplayedOnceU3Ek__BackingField_44 = value;
	}

	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_45() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___U3CpositionU3Ek__BackingField_45)); }
	inline float get_U3CpositionU3Ek__BackingField_45() const { return ___U3CpositionU3Ek__BackingField_45; }
	inline float* get_address_of_U3CpositionU3Ek__BackingField_45() { return &___U3CpositionU3Ek__BackingField_45; }
	inline void set_U3CpositionU3Ek__BackingField_45(float value)
	{
		___U3CpositionU3Ek__BackingField_45 = value;
	}

	inline static int32_t get_offset_of_fullDuration_46() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___fullDuration_46)); }
	inline float get_fullDuration_46() const { return ___fullDuration_46; }
	inline float* get_address_of_fullDuration_46() { return &___fullDuration_46; }
	inline void set_fullDuration_46(float value)
	{
		___fullDuration_46 = value;
	}

	inline static int32_t get_offset_of_completedLoops_47() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___completedLoops_47)); }
	inline int32_t get_completedLoops_47() const { return ___completedLoops_47; }
	inline int32_t* get_address_of_completedLoops_47() { return &___completedLoops_47; }
	inline void set_completedLoops_47(int32_t value)
	{
		___completedLoops_47 = value;
	}

	inline static int32_t get_offset_of_isPlaying_48() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isPlaying_48)); }
	inline bool get_isPlaying_48() const { return ___isPlaying_48; }
	inline bool* get_address_of_isPlaying_48() { return &___isPlaying_48; }
	inline void set_isPlaying_48(bool value)
	{
		___isPlaying_48 = value;
	}

	inline static int32_t get_offset_of_isComplete_49() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___isComplete_49)); }
	inline bool get_isComplete_49() const { return ___isComplete_49; }
	inline bool* get_address_of_isComplete_49() { return &___isComplete_49; }
	inline void set_isComplete_49(bool value)
	{
		___isComplete_49 = value;
	}

	inline static int32_t get_offset_of_elapsedDelay_50() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___elapsedDelay_50)); }
	inline float get_elapsedDelay_50() const { return ___elapsedDelay_50; }
	inline float* get_address_of_elapsedDelay_50() { return &___elapsedDelay_50; }
	inline void set_elapsedDelay_50(float value)
	{
		___elapsedDelay_50 = value;
	}

	inline static int32_t get_offset_of_delayComplete_51() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___delayComplete_51)); }
	inline bool get_delayComplete_51() const { return ___delayComplete_51; }
	inline bool* get_address_of_delayComplete_51() { return &___delayComplete_51; }
	inline void set_delayComplete_51(bool value)
	{
		___delayComplete_51 = value;
	}

	inline static int32_t get_offset_of_miscInt_52() { return static_cast<int32_t>(offsetof(Tween_t119487E0AB84EF563521F1043116BDBAE68AC437, ___miscInt_52)); }
	inline int32_t get_miscInt_52() const { return ___miscInt_52; }
	inline int32_t* get_address_of_miscInt_52() { return &___miscInt_52; }
	inline void set_miscInt_52(int32_t value)
	{
		___miscInt_52 = value;
	}
};


// GameController_OnPlayAgain
struct  OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`1<System.Object>
struct  Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0  : public MulticastDelegate_t
{
public:

public:
};


// System.AsyncCallback
struct  AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityMessageManager_MessageDelegate
struct  MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E  : public MulticastDelegate_t
{
public:

public:
};


// UnityMessageManager_MessageHandlerDelegate
struct  MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101  : public MulticastDelegate_t
{
public:

public:
};


// Verification_CreditVerified
struct  CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74  : public MulticastDelegate_t
{
public:

public:
};


// DG.Tweening.Tweener
struct  Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8  : public Tween_t119487E0AB84EF563521F1043116BDBAE68AC437
{
public:
	// System.Boolean DG.Tweening.Tweener::hasManuallySetStartValue
	bool ___hasManuallySetStartValue_53;
	// System.Boolean DG.Tweening.Tweener::isFromAllowed
	bool ___isFromAllowed_54;

public:
	inline static int32_t get_offset_of_hasManuallySetStartValue_53() { return static_cast<int32_t>(offsetof(Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8, ___hasManuallySetStartValue_53)); }
	inline bool get_hasManuallySetStartValue_53() const { return ___hasManuallySetStartValue_53; }
	inline bool* get_address_of_hasManuallySetStartValue_53() { return &___hasManuallySetStartValue_53; }
	inline void set_hasManuallySetStartValue_53(bool value)
	{
		___hasManuallySetStartValue_53 = value;
	}

	inline static int32_t get_offset_of_isFromAllowed_54() { return static_cast<int32_t>(offsetof(Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8, ___isFromAllowed_54)); }
	inline bool get_isFromAllowed_54() const { return ___isFromAllowed_54; }
	inline bool* get_address_of_isFromAllowed_54() { return &___isFromAllowed_54; }
	inline void set_isFromAllowed_54(bool value)
	{
		___isFromAllowed_54 = value;
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>
struct  TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B  : public Tweener_t9B2A5E94EE6D11F7607E58AE4E37186FF63587C8
{
public:
	// T2 DG.Tweening.Core.TweenerCore`3::startValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startValue_55;
	// T2 DG.Tweening.Core.TweenerCore`3::endValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endValue_56;
	// T2 DG.Tweening.Core.TweenerCore`3::changeValue
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___changeValue_57;
	// TPlugOptions DG.Tweening.Core.TweenerCore`3::plugOptions
	VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322  ___plugOptions_58;
	// DG.Tweening.Core.DOGetter`1<T1> DG.Tweening.Core.TweenerCore`3::getter
	DOGetter_1_t6A6DB6A183AD6D7F4B2F3656C4AB9326994D8CA4 * ___getter_59;
	// DG.Tweening.Core.DOSetter`1<T1> DG.Tweening.Core.TweenerCore`3::setter
	DOSetter_1_t528C4470268C88CABD1BFDF039B98D72E012AF46 * ___setter_60;
	// DG.Tweening.Plugins.Core.ABSTweenPlugin`3<T1,T2,TPlugOptions> DG.Tweening.Core.TweenerCore`3::tweenPlugin
	ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678 * ___tweenPlugin_61;

public:
	inline static int32_t get_offset_of_startValue_55() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___startValue_55)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startValue_55() const { return ___startValue_55; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startValue_55() { return &___startValue_55; }
	inline void set_startValue_55(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startValue_55 = value;
	}

	inline static int32_t get_offset_of_endValue_56() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___endValue_56)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_endValue_56() const { return ___endValue_56; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_endValue_56() { return &___endValue_56; }
	inline void set_endValue_56(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___endValue_56 = value;
	}

	inline static int32_t get_offset_of_changeValue_57() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___changeValue_57)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_changeValue_57() const { return ___changeValue_57; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_changeValue_57() { return &___changeValue_57; }
	inline void set_changeValue_57(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___changeValue_57 = value;
	}

	inline static int32_t get_offset_of_plugOptions_58() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___plugOptions_58)); }
	inline VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322  get_plugOptions_58() const { return ___plugOptions_58; }
	inline VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322 * get_address_of_plugOptions_58() { return &___plugOptions_58; }
	inline void set_plugOptions_58(VectorOptions_t385A0AFDBE150CC9F61EA0DC03FEA860BA5EA322  value)
	{
		___plugOptions_58 = value;
	}

	inline static int32_t get_offset_of_getter_59() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___getter_59)); }
	inline DOGetter_1_t6A6DB6A183AD6D7F4B2F3656C4AB9326994D8CA4 * get_getter_59() const { return ___getter_59; }
	inline DOGetter_1_t6A6DB6A183AD6D7F4B2F3656C4AB9326994D8CA4 ** get_address_of_getter_59() { return &___getter_59; }
	inline void set_getter_59(DOGetter_1_t6A6DB6A183AD6D7F4B2F3656C4AB9326994D8CA4 * value)
	{
		___getter_59 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___getter_59), (void*)value);
	}

	inline static int32_t get_offset_of_setter_60() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___setter_60)); }
	inline DOSetter_1_t528C4470268C88CABD1BFDF039B98D72E012AF46 * get_setter_60() const { return ___setter_60; }
	inline DOSetter_1_t528C4470268C88CABD1BFDF039B98D72E012AF46 ** get_address_of_setter_60() { return &___setter_60; }
	inline void set_setter_60(DOSetter_1_t528C4470268C88CABD1BFDF039B98D72E012AF46 * value)
	{
		___setter_60 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___setter_60), (void*)value);
	}

	inline static int32_t get_offset_of_tweenPlugin_61() { return static_cast<int32_t>(offsetof(TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B, ___tweenPlugin_61)); }
	inline ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678 * get_tweenPlugin_61() const { return ___tweenPlugin_61; }
	inline ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678 ** get_address_of_tweenPlugin_61() { return &___tweenPlugin_61; }
	inline void set_tweenPlugin_61(ABSTweenPlugin_3_t50462951CA2C372D1A9BC7C38C6ECC1810944678 * value)
	{
		___tweenPlugin_61 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tweenPlugin_61), (void*)value);
	}
};


// GameController
struct  GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 GameController::credit
	int32_t ___credit_4;
	// System.Boolean GameController::hasGiftBox
	bool ___hasGiftBox_5;
	// Levels GameController::levels
	Levels_tD642EA54B7651EAF0091AE53B3E74D05EAB7EC7B * ___levels_6;
	// System.Int32 GameController::currentLevelCount
	int32_t ___currentLevelCount_7;
	// GameController_GameStatus GameController::gameStatus
	int32_t ___gameStatus_8;
	// GameController_CurrentLevelData GameController::currentLevelData
	CurrentLevelData_tE484B76B20083ADBD407E6CEAD859634F191F518 * ___currentLevelData_9;

public:
	inline static int32_t get_offset_of_credit_4() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___credit_4)); }
	inline int32_t get_credit_4() const { return ___credit_4; }
	inline int32_t* get_address_of_credit_4() { return &___credit_4; }
	inline void set_credit_4(int32_t value)
	{
		___credit_4 = value;
	}

	inline static int32_t get_offset_of_hasGiftBox_5() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___hasGiftBox_5)); }
	inline bool get_hasGiftBox_5() const { return ___hasGiftBox_5; }
	inline bool* get_address_of_hasGiftBox_5() { return &___hasGiftBox_5; }
	inline void set_hasGiftBox_5(bool value)
	{
		___hasGiftBox_5 = value;
	}

	inline static int32_t get_offset_of_levels_6() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___levels_6)); }
	inline Levels_tD642EA54B7651EAF0091AE53B3E74D05EAB7EC7B * get_levels_6() const { return ___levels_6; }
	inline Levels_tD642EA54B7651EAF0091AE53B3E74D05EAB7EC7B ** get_address_of_levels_6() { return &___levels_6; }
	inline void set_levels_6(Levels_tD642EA54B7651EAF0091AE53B3E74D05EAB7EC7B * value)
	{
		___levels_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___levels_6), (void*)value);
	}

	inline static int32_t get_offset_of_currentLevelCount_7() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___currentLevelCount_7)); }
	inline int32_t get_currentLevelCount_7() const { return ___currentLevelCount_7; }
	inline int32_t* get_address_of_currentLevelCount_7() { return &___currentLevelCount_7; }
	inline void set_currentLevelCount_7(int32_t value)
	{
		___currentLevelCount_7 = value;
	}

	inline static int32_t get_offset_of_gameStatus_8() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___gameStatus_8)); }
	inline int32_t get_gameStatus_8() const { return ___gameStatus_8; }
	inline int32_t* get_address_of_gameStatus_8() { return &___gameStatus_8; }
	inline void set_gameStatus_8(int32_t value)
	{
		___gameStatus_8 = value;
	}

	inline static int32_t get_offset_of_currentLevelData_9() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3, ___currentLevelData_9)); }
	inline CurrentLevelData_tE484B76B20083ADBD407E6CEAD859634F191F518 * get_currentLevelData_9() const { return ___currentLevelData_9; }
	inline CurrentLevelData_tE484B76B20083ADBD407E6CEAD859634F191F518 ** get_address_of_currentLevelData_9() { return &___currentLevelData_9; }
	inline void set_currentLevelData_9(CurrentLevelData_tE484B76B20083ADBD407E6CEAD859634F191F518 * value)
	{
		___currentLevelData_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentLevelData_9), (void*)value);
	}
};

struct GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields
{
public:
	// GameController_OnPlayAgain GameController::onPlayAgain
	OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * ___onPlayAgain_10;
	// GameController_OnInitializeGameUI GameController::onInitializeGameUI
	OnInitializeGameUI_tB65E856A433F3B0EF1922D1F56B23B45D0664314 * ___onInitializeGameUI_11;
	// GameController_OnGameStart GameController::onGameStart
	OnGameStart_t3BCD32C4083A49DF55CE8B92FE83464FBB1C4353 * ___onGameStart_12;
	// GameController_OnPlaying GameController::onPlaying
	OnPlaying_t5B0880EE1DA7CB41A933143A651E58CBC338BF1B * ___onPlaying_13;
	// GameController_OnLevelUp GameController::onLevelUp
	OnLevelUp_t52A7FC3E266DE444FA52B33423F2991AC4FDA92F * ___onLevelUp_14;
	// GameController_OnGameOver GameController::onGameOver
	OnGameOver_t076BD5C9FC5BA0655BA9845C8153DD1B72C995EE * ___onGameOver_15;
	// GameController_OnShowGiftbox GameController::onShowGiftbox
	OnShowGiftbox_t770AC8537873656001D91D013912642C3E50751B * ___onShowGiftbox_16;
	// GameController_OnShowPlayAgain GameController::onShowPlayAgain
	OnShowPlayAgain_t697DAB214BF0D0CC20A314594A1E4F1046C82118 * ___onShowPlayAgain_17;
	// GameController GameController::_instance
	GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * ____instance_18;

public:
	inline static int32_t get_offset_of_onPlayAgain_10() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onPlayAgain_10)); }
	inline OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * get_onPlayAgain_10() const { return ___onPlayAgain_10; }
	inline OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 ** get_address_of_onPlayAgain_10() { return &___onPlayAgain_10; }
	inline void set_onPlayAgain_10(OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * value)
	{
		___onPlayAgain_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlayAgain_10), (void*)value);
	}

	inline static int32_t get_offset_of_onInitializeGameUI_11() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onInitializeGameUI_11)); }
	inline OnInitializeGameUI_tB65E856A433F3B0EF1922D1F56B23B45D0664314 * get_onInitializeGameUI_11() const { return ___onInitializeGameUI_11; }
	inline OnInitializeGameUI_tB65E856A433F3B0EF1922D1F56B23B45D0664314 ** get_address_of_onInitializeGameUI_11() { return &___onInitializeGameUI_11; }
	inline void set_onInitializeGameUI_11(OnInitializeGameUI_tB65E856A433F3B0EF1922D1F56B23B45D0664314 * value)
	{
		___onInitializeGameUI_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onInitializeGameUI_11), (void*)value);
	}

	inline static int32_t get_offset_of_onGameStart_12() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onGameStart_12)); }
	inline OnGameStart_t3BCD32C4083A49DF55CE8B92FE83464FBB1C4353 * get_onGameStart_12() const { return ___onGameStart_12; }
	inline OnGameStart_t3BCD32C4083A49DF55CE8B92FE83464FBB1C4353 ** get_address_of_onGameStart_12() { return &___onGameStart_12; }
	inline void set_onGameStart_12(OnGameStart_t3BCD32C4083A49DF55CE8B92FE83464FBB1C4353 * value)
	{
		___onGameStart_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onGameStart_12), (void*)value);
	}

	inline static int32_t get_offset_of_onPlaying_13() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onPlaying_13)); }
	inline OnPlaying_t5B0880EE1DA7CB41A933143A651E58CBC338BF1B * get_onPlaying_13() const { return ___onPlaying_13; }
	inline OnPlaying_t5B0880EE1DA7CB41A933143A651E58CBC338BF1B ** get_address_of_onPlaying_13() { return &___onPlaying_13; }
	inline void set_onPlaying_13(OnPlaying_t5B0880EE1DA7CB41A933143A651E58CBC338BF1B * value)
	{
		___onPlaying_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPlaying_13), (void*)value);
	}

	inline static int32_t get_offset_of_onLevelUp_14() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onLevelUp_14)); }
	inline OnLevelUp_t52A7FC3E266DE444FA52B33423F2991AC4FDA92F * get_onLevelUp_14() const { return ___onLevelUp_14; }
	inline OnLevelUp_t52A7FC3E266DE444FA52B33423F2991AC4FDA92F ** get_address_of_onLevelUp_14() { return &___onLevelUp_14; }
	inline void set_onLevelUp_14(OnLevelUp_t52A7FC3E266DE444FA52B33423F2991AC4FDA92F * value)
	{
		___onLevelUp_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onLevelUp_14), (void*)value);
	}

	inline static int32_t get_offset_of_onGameOver_15() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onGameOver_15)); }
	inline OnGameOver_t076BD5C9FC5BA0655BA9845C8153DD1B72C995EE * get_onGameOver_15() const { return ___onGameOver_15; }
	inline OnGameOver_t076BD5C9FC5BA0655BA9845C8153DD1B72C995EE ** get_address_of_onGameOver_15() { return &___onGameOver_15; }
	inline void set_onGameOver_15(OnGameOver_t076BD5C9FC5BA0655BA9845C8153DD1B72C995EE * value)
	{
		___onGameOver_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onGameOver_15), (void*)value);
	}

	inline static int32_t get_offset_of_onShowGiftbox_16() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onShowGiftbox_16)); }
	inline OnShowGiftbox_t770AC8537873656001D91D013912642C3E50751B * get_onShowGiftbox_16() const { return ___onShowGiftbox_16; }
	inline OnShowGiftbox_t770AC8537873656001D91D013912642C3E50751B ** get_address_of_onShowGiftbox_16() { return &___onShowGiftbox_16; }
	inline void set_onShowGiftbox_16(OnShowGiftbox_t770AC8537873656001D91D013912642C3E50751B * value)
	{
		___onShowGiftbox_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onShowGiftbox_16), (void*)value);
	}

	inline static int32_t get_offset_of_onShowPlayAgain_17() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ___onShowPlayAgain_17)); }
	inline OnShowPlayAgain_t697DAB214BF0D0CC20A314594A1E4F1046C82118 * get_onShowPlayAgain_17() const { return ___onShowPlayAgain_17; }
	inline OnShowPlayAgain_t697DAB214BF0D0CC20A314594A1E4F1046C82118 ** get_address_of_onShowPlayAgain_17() { return &___onShowPlayAgain_17; }
	inline void set_onShowPlayAgain_17(OnShowPlayAgain_t697DAB214BF0D0CC20A314594A1E4F1046C82118 * value)
	{
		___onShowPlayAgain_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onShowPlayAgain_17), (void*)value);
	}

	inline static int32_t get_offset_of__instance_18() { return static_cast<int32_t>(offsetof(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields, ____instance_18)); }
	inline GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * get__instance_18() const { return ____instance_18; }
	inline GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 ** get_address_of__instance_18() { return &____instance_18; }
	inline void set__instance_18(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * value)
	{
		____instance_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____instance_18), (void*)value);
	}
};


// UnityMessageManager
struct  UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityMessageManager_MessageDelegate UnityMessageManager::OnMessage
	MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * ___OnMessage_7;
	// UnityMessageManager_MessageHandlerDelegate UnityMessageManager::OnRNMessage
	MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___OnRNMessage_8;
	// System.Collections.Generic.Dictionary`2<System.Int32,UnityMessage> UnityMessageManager::waitCallbackMessageMap
	Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * ___waitCallbackMessageMap_9;

public:
	inline static int32_t get_offset_of_OnMessage_7() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___OnMessage_7)); }
	inline MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * get_OnMessage_7() const { return ___OnMessage_7; }
	inline MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E ** get_address_of_OnMessage_7() { return &___OnMessage_7; }
	inline void set_OnMessage_7(MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * value)
	{
		___OnMessage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnMessage_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnRNMessage_8() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___OnRNMessage_8)); }
	inline MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * get_OnRNMessage_8() const { return ___OnRNMessage_8; }
	inline MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 ** get_address_of_OnRNMessage_8() { return &___OnRNMessage_8; }
	inline void set_OnRNMessage_8(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * value)
	{
		___OnRNMessage_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnRNMessage_8), (void*)value);
	}

	inline static int32_t get_offset_of_waitCallbackMessageMap_9() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F, ___waitCallbackMessageMap_9)); }
	inline Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * get_waitCallbackMessageMap_9() const { return ___waitCallbackMessageMap_9; }
	inline Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 ** get_address_of_waitCallbackMessageMap_9() { return &___waitCallbackMessageMap_9; }
	inline void set_waitCallbackMessageMap_9(Dictionary_2_t7DC8BC690F69BD7893ABAC6770BA4286B9A971C8 * value)
	{
		___waitCallbackMessageMap_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___waitCallbackMessageMap_9), (void*)value);
	}
};

struct UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields
{
public:
	// System.Int32 UnityMessageManager::ID
	int32_t ___ID_5;
	// UnityMessageManager UnityMessageManager::<Instance>k__BackingField
	UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * ___U3CInstanceU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_ID_5() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields, ___ID_5)); }
	inline int32_t get_ID_5() const { return ___ID_5; }
	inline int32_t* get_address_of_ID_5() { return &___ID_5; }
	inline void set_ID_5(int32_t value)
	{
		___ID_5 = value;
	}

	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields, ___U3CInstanceU3Ek__BackingField_6)); }
	inline UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * get_U3CInstanceU3Ek__BackingField_6() const { return ___U3CInstanceU3Ek__BackingField_6; }
	inline UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F ** get_address_of_U3CInstanceU3Ek__BackingField_6() { return &___U3CInstanceU3Ek__BackingField_6; }
	inline void set_U3CInstanceU3Ek__BackingField_6(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * value)
	{
		___U3CInstanceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CInstanceU3Ek__BackingField_6), (void*)value);
	}
};


// Verification
struct  Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TMP_Text Verification::verif_text
	TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * ___verif_text_4;
	// UnityEngine.GameObject Verification::verifContainer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___verifContainer_5;
	// UnityEngine.GameObject Verification::loading
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___loading_6;

public:
	inline static int32_t get_offset_of_verif_text_4() { return static_cast<int32_t>(offsetof(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA, ___verif_text_4)); }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * get_verif_text_4() const { return ___verif_text_4; }
	inline TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 ** get_address_of_verif_text_4() { return &___verif_text_4; }
	inline void set_verif_text_4(TMP_Text_t7BA5B6522651EBED2D8E2C92CBE3F17C14075CE7 * value)
	{
		___verif_text_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___verif_text_4), (void*)value);
	}

	inline static int32_t get_offset_of_verifContainer_5() { return static_cast<int32_t>(offsetof(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA, ___verifContainer_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_verifContainer_5() const { return ___verifContainer_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_verifContainer_5() { return &___verifContainer_5; }
	inline void set_verifContainer_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___verifContainer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___verifContainer_5), (void*)value);
	}

	inline static int32_t get_offset_of_loading_6() { return static_cast<int32_t>(offsetof(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA, ___loading_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_loading_6() const { return ___loading_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_loading_6() { return &___loading_6; }
	inline void set_loading_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___loading_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loading_6), (void*)value);
	}
};

struct Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_StaticFields
{
public:
	// Verification_CreditVerified Verification::creditVerified
	CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * ___creditVerified_7;

public:
	inline static int32_t get_offset_of_creditVerified_7() { return static_cast<int32_t>(offsetof(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_StaticFields, ___creditVerified_7)); }
	inline CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * get_creditVerified_7() const { return ___creditVerified_7; }
	inline CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 ** get_address_of_creditVerified_7() { return &___creditVerified_7; }
	inline void set_creditVerified_7(CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * value)
	{
		___creditVerified_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___creditVerified_7), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t * m_Items[1];

public:
	inline Delegate_t * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Delegate_t * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<System.Object>(!!0,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetDelay_TisRuntimeObject_m4C7D291E25A270CD45697A6BCD708B09D0298A2D_gshared (RuntimeObject * ___t0, float ___delay1, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<System.Object>(!!0,DG.Tweening.Ease)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * TweenSettingsExtensions_SetEase_TisRuntimeObject_m1CE41997D3274EBE885B839BC2912EBD7CBD4A8E_gshared (RuntimeObject * ___t0, int32_t ___ease1, const RuntimeMethod* method);

// UnityMessageManager UnityMessageManager::get_Instance()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline (const RuntimeMethod* method);
// System.Void UnityMessageManager/MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityMessageManager::add_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method);
// System.Void GameController/OnPlayAgain::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390 (OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Delegate System.Delegate::Combine(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1 (Delegate_t * ___a0, Delegate_t * ___b1, const RuntimeMethod* method);
// System.Void UnityMessageManager::remove_OnRNMessage(UnityMessageManager/MessageHandlerDelegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * ___value0, const RuntimeMethod* method);
// System.Delegate System.Delegate::Remove(System.Delegate,System.Delegate)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Delegate_t * Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D (Delegate_t * ___source0, Delegate_t * ___value1, const RuntimeMethod* method);
// GameController GameController::get_Instance()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260_inline (const RuntimeMethod* method);
// System.Void Verification::ShowVerificationPopUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method);
// System.Void UnityMessage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45 (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510 (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityMessageManager::SendMessageToRN(UnityMessage)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E (UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * __this, UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * ___message0, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void Verification::HideVerificationPopUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method);
// System.Void Verification/CreditVerified::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreditVerified_Invoke_mFC8A312FC083A8A4CC499D6AF6F8D111811FFA82 (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04 (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C (GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556 (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * __this, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1 (Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions> DG.Tweening.ShortcutExtensions::DOScale(UnityEngine.Transform,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * ShortcutExtensions_DOScale_m2954FD20A561ECF99B91AA617430F3379704CCCB (Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target0, Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___endValue1, float ___duration2, const RuntimeMethod* method);
// !!0 DG.Tweening.TweenSettingsExtensions::SetDelay<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,System.Single)
inline TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_m96AF6FDC6BC35E18F78A5510ECA18A01BA312B57 (TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * ___t0, float ___delay1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * (*) (TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B *, float, const RuntimeMethod*))TweenSettingsExtensions_SetDelay_TisRuntimeObject_m4C7D291E25A270CD45697A6BCD708B09D0298A2D_gshared)(___t0, ___delay1, method);
}
// !!0 DG.Tweening.TweenSettingsExtensions::SetEase<DG.Tweening.Core.TweenerCore`3<UnityEngine.Vector3,UnityEngine.Vector3,DG.Tweening.Plugins.Options.VectorOptions>>(!!0,DG.Tweening.Ease)
inline TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * TweenSettingsExtensions_SetEase_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_mD0FE09A1EFC67CFB28555BE2C84249EB301DB47F (TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * ___t0, int32_t ___ease1, const RuntimeMethod* method)
{
	return ((  TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * (*) (TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B *, int32_t, const RuntimeMethod*))TweenSettingsExtensions_SetEase_TisRuntimeObject_m1CE41997D3274EBE885B839BC2912EBD7CBD4A8E_gshared)(___t0, ___ease1, method);
}
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.Void Verification::Verified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)(char*);
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Marshaling of parameter '___message0' to native representation
	char* ____message0_marshaled = NULL;
	____message0_marshaled = il2cpp_codegen_marshal_string(___message0);

	// Native function invocation
	il2cppPInvokeFunc(____message0_marshaled);

	// Marshaling cleanup of parameter '___message0' native representation
	il2cpp_codegen_marshal_free(____message0_marshaled);
	____message0_marshaled = NULL;

}
// System.Void UnityMessageManager_MessageDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate__ctor_m3EA48B06536871149F1A43C905528166D49F48C8 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityMessageManager_MessageDelegate::Invoke(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate_Invoke_m2D199E1633D3EFBEA727164B8A3F7888E6A4F801 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___message0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___message0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___message0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (String_t*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___message0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
					else
						GenericVirtActionInvoker1< String_t* >::Invoke(targetMethod, targetThis, ___message0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___message0);
					else
						VirtActionInvoker1< String_t* >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___message0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, String_t*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___message0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UnityMessageManager_MessageDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MessageDelegate_BeginInvoke_m89C256246D24A40498952C82CDD615B3507E4BA4 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, String_t* ___message0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___message0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityMessageManager_MessageDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageDelegate_EndInvoke_m778326112BD3D26CB9DE0FFC335E9A1A9353BC35 (MessageDelegate_tEC5B4973B81B5B0805A2F28582EC515D5475232E * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityMessageManager_MessageHandlerDelegate::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void UnityMessageManager_MessageHandlerDelegate::Invoke(MessageHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate_Invoke_mF982647CDFA5782B9ABAF328FF4070492A364B43 (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___handler0, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 1)
			{
				// open
				typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, targetMethod);
			}
		}
		else if (___parameterCount != 1)
		{
			// open
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, ___handler0);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, ___handler0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), ___handler0);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), ___handler0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (targetThis == NULL)
				{
					typedef void (*FunctionPointerType) (MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(___handler0, targetMethod);
				}
				else if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(targetMethod, targetThis, ___handler0);
					else
						GenericVirtActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(targetMethod, targetThis, ___handler0);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis, ___handler0);
					else
						VirtActionInvoker1< MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * >::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis, ___handler0);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 *, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, ___handler0, targetMethod);
			}
		}
	}
}
// System.IAsyncResult UnityMessageManager_MessageHandlerDelegate::BeginInvoke(MessageHandler,System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* MessageHandlerDelegate_BeginInvoke_mFDBB4C951CAC28A4F0E08666050BB3F266FA927E (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___handler0, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback1, RuntimeObject * ___object2, const RuntimeMethod* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___handler0;
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback1, (RuntimeObject*)___object2);
}
// System.Void UnityMessageManager_MessageHandlerDelegate::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MessageHandlerDelegate_EndInvoke_m9BC3850452E21DEBBF15C7598D811D4D744633A6 (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Verification::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_Awake_m4CD0EE2E0009E58A617FA08FA6D449DF1B2E226D (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_Awake_m4CD0EE2E0009E58A617FA08FA6D449DF1B2E226D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA(L_0, L_1, /*hidden argument*/NULL);
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_2 = ((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->get_onPlayAgain_10();
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_3 = (OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)il2cpp_codegen_object_new(OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var);
		OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390(L_3, __this, (intptr_t)((intptr_t)Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309_RuntimeMethod_var), /*hidden argument*/NULL);
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->set_onPlayAgain_10(((OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)CastclassSealed((RuntimeObject*)L_4, OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Verification::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_OnEnable_mE50987A371FD54F5292E20F154AC2A490D011107 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_OnEnable_mE50987A371FD54F5292E20F154AC2A490D011107_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_add_OnRNMessage_mD1D6F59672DB48353711FBD7CC5A1C5573FF33FA(L_0, L_1, /*hidden argument*/NULL);
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_2 = ((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->get_onPlayAgain_10();
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_3 = (OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)il2cpp_codegen_object_new(OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var);
		OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390(L_3, __this, (intptr_t)((intptr_t)Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309_RuntimeMethod_var), /*hidden argument*/NULL);
		Delegate_t * L_4 = Delegate_Combine_mC25D2F7DECAFBA6D9A2F9EBA8A77063F0658ECF1(L_2, L_3, /*hidden argument*/NULL);
		((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->set_onPlayAgain_10(((OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)CastclassSealed((RuntimeObject*)L_4, OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Verification::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_OnDisable_m18806AEBCF408EB9266CE92A75C8F7F9407BF21B (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_OnDisable_m18806AEBCF408EB9266CE92A75C8F7F9407BF21B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E(L_0, L_1, /*hidden argument*/NULL);
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_2 = ((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->get_onPlayAgain_10();
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_3 = (OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)il2cpp_codegen_object_new(OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var);
		OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390(L_3, __this, (intptr_t)((intptr_t)Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309_RuntimeMethod_var), /*hidden argument*/NULL);
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->set_onPlayAgain_10(((OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)CastclassSealed((RuntimeObject*)L_4, OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Verification::Destroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_Destroy_m8607EFAAF4C6E7D28461C5B10AFED955AD49C446 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_Destroy_m8607EFAAF4C6E7D28461C5B10AFED955AD49C446_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 * L_1 = (MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101 *)il2cpp_codegen_object_new(MessageHandlerDelegate_t6F53AFA81E390D93C9E7F2C31E3CD65FCBF84101_il2cpp_TypeInfo_var);
		MessageHandlerDelegate__ctor_m58386047CB442F144728F0A568B26FDDDB0EE6EA(L_1, __this, (intptr_t)((intptr_t)Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		UnityMessageManager_remove_OnRNMessage_m12B8512E35DC119F3800C8C8A05442F9C0A19B5E(L_0, L_1, /*hidden argument*/NULL);
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_2 = ((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->get_onPlayAgain_10();
		OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 * L_3 = (OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)il2cpp_codegen_object_new(OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var);
		OnPlayAgain__ctor_m367BBDED33B19F7AC50F8858D41117CAEFA74390(L_3, __this, (intptr_t)((intptr_t)Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309_RuntimeMethod_var), /*hidden argument*/NULL);
		Delegate_t * L_4 = Delegate_Remove_m0B0DB7D1B3AF96B71AFAA72BA0EFE32FBBC2932D(L_2, L_3, /*hidden argument*/NULL);
		((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->set_onPlayAgain_10(((OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611 *)CastclassSealed((RuntimeObject*)L_4, OnPlayAgain_tC54EE3C9C69359652641548EE0AB1E29D670B611_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void Verification::OnReceivedReactNativeMessage(MessageHandler)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_OnReceivedReactNativeMessage_mCC87EC70B7F76D2B3721274BF29DD4F7A0BBE534 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, MessageHandler_t229E80D372663A5098E3DBDCB563A36576A49840 * ___message0, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Void Verification::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_Start_m724EA6CB3B87E138FBBF99835797BCB0BAE3EAB9 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	{
		GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * L_0 = GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_credit_4();
		if (L_1)
		{
			goto IL_0012;
		}
	}
	{
		Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318(__this, /*hidden argument*/NULL);
	}

IL_0012:
	{
		return;
	}
}
// System.Void Verification::VerificationMessage()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_VerificationMessage_m3C94AE37F59AB2F5AF0B18A9D95BAD98B6188E1F (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_VerificationMessage_m3C94AE37F59AB2F5AF0B18A9D95BAD98B6188E1F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline(/*hidden argument*/NULL);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_1 = (UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 *)il2cpp_codegen_object_new(UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4_il2cpp_TypeInfo_var);
		UnityMessage__ctor_m76E68BAF631BE07953E806CA26C480BF23B2ED45(L_1, /*hidden argument*/NULL);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_2 = L_1;
		NullCheck(L_2);
		L_2->set_name_0(_stringLiteral65279E1ECF00D0D1177A5B3B458FD187A1D6135E);
		UnityMessage_t4E02735CFD842E0810B789FC36B22142221BFEB4 * L_3 = L_2;
		Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * L_4 = (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 *)il2cpp_codegen_object_new(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0_il2cpp_TypeInfo_var);
		Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510(L_4, __this, (intptr_t)((intptr_t)Verification_U3CVerificationMessageU3Eb__11_0_mF013FC22CF55ECA6FE3AF190613E520947622178_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_RuntimeMethod_var);
		NullCheck(L_3);
		L_3->set_callBack_2(L_4);
		NullCheck(L_0);
		UnityMessageManager_SendMessageToRN_m810379DB93ACA8FB4F37914CA49BA677BDE94E3E(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Verification::Verified()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteralC69F263D9117BC3A30FE1031BED6E325F7B2FEBF, /*hidden argument*/NULL);
		Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C(__this, /*hidden argument*/NULL);
		GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * L_0 = GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260_inline(/*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_credit_4(1);
		CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * L_1 = ((Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_StaticFields*)il2cpp_codegen_static_fields_for(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_il2cpp_TypeInfo_var))->get_creditVerified_7();
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * L_2 = ((Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_StaticFields*)il2cpp_codegen_static_fields_for(Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA_il2cpp_TypeInfo_var))->get_creditVerified_7();
		NullCheck(L_2);
		CreditVerified_Invoke_mFC8A312FC083A8A4CC499D6AF6F8D111811FFA82(L_2, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void Verification::HideVerificationPopUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_HideVerificationPopUp_m7E0B68506F7E3A52AB51A5000621733A73A2938C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_Log_m4B7C70BAFD477C6BDB59C88A0934F0B018D03708(_stringLiteral3618FB1497957BF530D16396449F5D7D96A59856, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_loading_6();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_verifContainer_5();
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)0, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_verifContainer_5();
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_il2cpp_TypeInfo_var);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4 = Vector3_get_zero_m3CDDCAE94581DF3BB16C4B40A100E28E9C6649C2(/*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m7ED1A6E5A87CD1D483515B99D6D3121FB92B0556(L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Verification::OnPlayAgain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_OnPlayAgain_mA756FC56D2F70CE52E28341FAA923C5DE390F309 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	{
		Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Verification::ShowVerificationPopUp()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Verification_ShowVerificationPopUp_mC04047DDE24975E88E1CCFAE75BE8897FA118318_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_0 = __this->get_loading_6();
		NullCheck(L_0);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_0, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_1 = __this->get_verifContainer_5();
		NullCheck(L_1);
		GameObject_SetActive_m25A39F6D9FB68C51F13313F9804E85ACC937BC04(L_1, (bool)1, /*hidden argument*/NULL);
		GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * L_2 = __this->get_verifContainer_5();
		NullCheck(L_2);
		Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * L_3 = GameObject_get_transform_mA5C38857137F137CB96C69FAA624199EB1C2FB2C(L_2, /*hidden argument*/NULL);
		Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m08F61F548AA5836D8789843ACB4A81E4963D2EE1((&L_4), (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * L_5 = ShortcutExtensions_DOScale_m2954FD20A561ECF99B91AA617430F3379704CCCB(L_3, L_4, (0.3f), /*hidden argument*/NULL);
		TweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B * L_6 = TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_m96AF6FDC6BC35E18F78A5510ECA18A01BA312B57(L_5, (0.2f), /*hidden argument*/TweenSettingsExtensions_SetDelay_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_m96AF6FDC6BC35E18F78A5510ECA18A01BA312B57_RuntimeMethod_var);
		TweenSettingsExtensions_SetEase_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_mD0FE09A1EFC67CFB28555BE2C84249EB301DB47F(L_6, ((int32_t)23), /*hidden argument*/TweenSettingsExtensions_SetEase_TisTweenerCore_3_t104C232CA5A703780F416D4E8F2D71E42FE9C46B_mD0FE09A1EFC67CFB28555BE2C84249EB301DB47F_RuntimeMethod_var);
		return;
	}
}
// System.Void Verification::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification__ctor_mF7CEB04FD552F04F0FEE9512769E3571FE91E663 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Verification::<VerificationMessage>b__11_0(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Verification_U3CVerificationMessageU3Eb__11_0_mF013FC22CF55ECA6FE3AF190613E520947622178 (Verification_t1B36E5174F8FDC4B841655BFCB46416900B28FBA * __this, RuntimeObject * ___data0, const RuntimeMethod* method)
{
	{
		Verification_Verified_m920CE3F940E61198B950021DBF1152CCC103D9DC(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
IL2CPP_EXTERN_C  void DelegatePInvokeWrapper_CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Verification_CreditVerified::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreditVerified__ctor_m6D08EA013526D0B1C954C16044BEC3CE0B8841AD (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Verification_CreditVerified::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreditVerified_Invoke_mFC8A312FC083A8A4CC499D6AF6F8D111811FFA82 (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, const RuntimeMethod* method)
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* delegateArrayToInvoke = __this->get_delegates_11();
	Delegate_t** delegatesToInvoke;
	il2cpp_array_size_t length;
	if (delegateArrayToInvoke != NULL)
	{
		length = delegateArrayToInvoke->max_length;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(delegateArrayToInvoke->GetAddressAtUnchecked(0));
	}
	else
	{
		length = 1;
		delegatesToInvoke = reinterpret_cast<Delegate_t**>(&__this);
	}

	for (il2cpp_array_size_t i = 0; i < length; i++)
	{
		Delegate_t* currentDelegate = delegatesToInvoke[i];
		Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
		RuntimeObject* targetThis = currentDelegate->get_m_target_2();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
		if (!il2cpp_codegen_method_is_virtual(targetMethod))
		{
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		}
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		int ___parameterCount = il2cpp_codegen_method_parameter_count(targetMethod);
		if (___methodIsStatic)
		{
			if (___parameterCount == 0)
			{
				// open
				typedef void (*FunctionPointerType) (const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetMethod);
			}
			else
			{
				// closed
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
		else
		{
			// closed
			if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
			{
				if (il2cpp_codegen_method_is_generic_instance(targetMethod))
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
					else
						GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
				}
				else
				{
					if (il2cpp_codegen_method_is_interface_method(targetMethod))
						InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
					else
						VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
				}
			}
			else
			{
				typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
				((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
			}
		}
	}
}
// System.IAsyncResult Verification_CreditVerified::BeginInvoke(System.AsyncCallback,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* CreditVerified_BeginInvoke_m14F6CC8749546E94279573689A8862B08EAFA4A2 (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Verification_CreditVerified::EndInvoke(System.IAsyncResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreditVerified_EndInvoke_m3AED88BD6161023EBA8CDF74D437C950355CDCB9 (CreditVerified_t9B07AD8E0A29502F2A59DE726D44FFB769F1CD74 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UnityMessageManager_get_Instance_m3571F06844422C9D5B2ADE29D69AE7B9E63ACF16AssemblyU2DCSharp1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var);
		UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F * L_0 = ((UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_StaticFields*)il2cpp_codegen_static_fields_for(UnityMessageManager_t84F521C4D6478829BA5A1B35425A87A5C915D22F_il2cpp_TypeInfo_var))->get_U3CInstanceU3Ek__BackingField_6();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GameController_get_Instance_m09158B7BBCCFF92421E94BF6D92041BEAD10D260AssemblyU2DCSharp1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3 * L_0 = ((GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_StaticFields*)il2cpp_codegen_static_fields_for(GameController_t8BB0D3CE8992C95BED5FB508588730F0A74009A3_il2cpp_TypeInfo_var))->get__instance_18();
		return L_0;
	}
}
