﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TitleAdapter : MonoBehaviour
{

    public GameObject title;
    public Ease onHideEase;

    private void OnEnable()
    {
        PlayAdapter.onClickedPlay += OnPlay;
    }

    private void Awake()
    {
        //PlayAdapter.onClickedPlay += OnPlay;
    }

    private void OnDisable()
    {
        PlayAdapter.onClickedPlay -= OnPlay;
    }
    private void Destroy()
    {
        PlayAdapter.onClickedPlay -= OnPlay;
    }

    private void OnPlay()
    {
        title.transform.DOScale(Vector3.zero, 0.1f).SetEase(onHideEase).OnComplete(() => title.SetActive(false));
    }
}
