﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bomb : MonoBehaviour
{
    public ParticleSystem vfx;
    public Ease explosionEase;
    public GameObject bombSprite;

    // Private Varialbes
    Vector3 origsize;

    void Awake()
    {
        origsize = new Vector3();
        origsize = bombSprite.transform.localScale ;
        //GameController.onInitializeGameUI += OnShowScore;
    }

    private void OnEnable()
    {
        //GameController.onInitializeGameUI += OnShowScore;
        bombSprite.transform.localScale = origsize;
        bombSprite.SetActive(true);

    }

    private void OnDisable()
    {
        //GameController.onInitializeGameUI -= OnShowScore;
        //bombSprite.transform.localScale = origsize;
    }
    private void Destroy()
    {
        //GameController.onInitializeGameUI -= OnShowScore;
        //bombSprite.transform.localScale = origsize;
    }
    public void Explode()
    {
        bombSprite.transform.DOScale(new Vector3(0.37f, 0.37f, 0.37f), 0.13f).SetEase(explosionEase).OnComplete(() => PlayVfx());
    }

    void PlayVfx()
    {
        vfx.Play();
        bombSprite.SetActive(false);
    }
}
