﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GiftboxSpawner : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    public ObjectSpawner giftboxSpawner;
    [SerializeField]
    public List<Transform> giftboxPos;

    //Private Variables
    private int giftboxCounts = 0;
    private List<Transform> _giftboxPos;

    private void OnEnable()
    {
        GameController.onGameStart += OnLevelUp;
        GameController.onLevelUp += OnLevelUp;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
    }

    private void Destroy()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
    }

    private void Start()
    {
        _giftboxPos = new List<Transform>();

        for(int i = 0; i < giftboxPos.Count; i++)
            _giftboxPos.Add(giftboxPos[i]);
    }

    void OnLevelUp()
    {
        ResetGiftbox();

        ShowGiftbox();
    }

    void ResetGiftbox()
    {
        _giftboxPos.Clear();

        _giftboxPos = new List<Transform>();

        for(int i = 0; i < giftboxPos.Count; i++)
            _giftboxPos.Add(giftboxPos[i]);

        giftboxSpawner.DespawnAll();
    }

    void ShowGiftbox()
    {
        Debug.Log("GameController.Instance.currentLevelData.showGiftBox: " + GameController.Instance.currentLevelData.showGiftBox);
        if(!GameController.Instance.currentLevelData.showGiftBox)
            return;

        giftboxCounts = GameController.Instance.currentLevelData.numberOfGiftbox;

        for(int i = 0; i < giftboxCounts; i++)
        {
            int randomPos = Random.Range(0, (_giftboxPos.Count));
            giftboxSpawner.prefabPosition = _giftboxPos[randomPos].position;
            giftboxSpawner.useRotation = true;
            //Quaternion rot = _bombPos[randomPos].localRotation;
            giftboxSpawner.prefabRotation = _giftboxPos[randomPos].eulerAngles;
            _giftboxPos.Remove(_giftboxPos[randomPos]);
            giftboxSpawner.SpawnGameObject();
        }
    }
}
