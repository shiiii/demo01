﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Score : MonoBehaviour
{
    [SerializeField]
    public TMP_Text score;
    [SerializeField]
    public int scoreValue;
    public Ease onShowEase;

    public static Score Instance
    {
        get; private set;
    }

    void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        } else
        {
            Destroy(gameObject);
        }

        //GameController.onInitializeGameUI += OnShowScore;
    }

    private void OnEnable()
    {
        GameController.onInitializeGameUI += OnShowScore;
    }

    private void OnDisable()
    {
        GameController.onInitializeGameUI -= OnShowScore;
    }
    private void Destroy()
    {
        GameController.onInitializeGameUI -= OnShowScore;
    }

    public int ScoreValue
    {
        get {
            return scoreValue;
        }
    }

    public void Start()
    {
        //ResetScore();
        score.text = "";
    }

    // this methos is called at the inspector
    public void AddScore(string _value)
    {
        int value = int.Parse(_value);
        scoreValue += value;
        score.text = scoreValue.ToString();
    }

    public void ResetScore()
    {
        Debug.Log("RESET SCORE");
        scoreValue = 0;
        score.text = scoreValue.ToString();
    }

    void OnShowScore()
    {
        score.gameObject.SetActive(true);
        score.transform.DOScale(Vector3.one, 0.2f).SetEase(onShowEase);
        scoreValue = 0;
        score.text = scoreValue.ToString();
    }
}
