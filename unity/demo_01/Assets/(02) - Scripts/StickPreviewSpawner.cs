﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickPreviewSpawner : MonoBehaviour
{
    // Public Variables
    public ObjectSpawner previewSpawner;
    public int numberOfPreviewObejctToSpawn;
    [SerializeField]
    private List<Vector3> previewPosition;

    public void Awake()
    {
        if(previewSpawner == null)
            previewSpawner = gameObject.GetComponent<ObjectSpawner>();

        //GameController.onGameStart += OnLevelUp;
    }


    private void OnEnable()
    {
        GameController.onGameStart += OnLevelUp;
        GameController.onLevelUp += OnLevelUp;
        GameController.onGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onGameOver -= OnGameOver;
    }
    private void Destroy()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onGameOver -= OnGameOver;
    }

    public void Start()
    {
        //InitializePreview();
    }

    public void InitializePreview()
    {
        Debug.Log("InitializePreview");
        previewSpawner.DespawnAll();
    }

    public void ResetPreview()
    {
        Debug.Log("ResetPreview");
        numberOfPreviewObejctToSpawn = GameController.Instance.currentLevelData.objectCount;
        Debug.Log("numberOfPreviewObejctToSpawn : " + numberOfPreviewObejctToSpawn);

        for(int i = 0; i < numberOfPreviewObejctToSpawn; i++)
        {
            previewSpawner.prefabPosition = previewPosition[i];
            previewSpawner.SpawnGameObject();
        }
    }

    public void SpawnPreview()
    {
        previewSpawner.SpawnGameObject();
    }

    public void DespawnObject()
    {
        previewSpawner.DespawnGameobject();
    }

    private void OnLevelUp()
    {
        ResetPreview();
    }

    private void OnGameOver()
    {
        previewSpawner.DespawnAll();
    }
}
