﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CountDown : MonoBehaviour
{
    public Text counter;

    bool startCounter = false;
    float _counter;
    private void OnEnable()
    {
        GameController.onGameStart += OnStartCountDown;
    }

    private void Awake()
    {
        //counter.enabled = false;
        _counter = 3f;
        counter.transform.localScale = Vector3.zero;

        GameController.onGameStart += OnStartCountDown;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= OnStartCountDown;
    }
    private void Destroy()
    {
        GameController.onGameStart -= OnStartCountDown;
    }

    private void Update()
    {
        if(startCounter)
        {
            _counter -= Time.deltaTime;
            counter.text = Mathf.RoundToInt(_counter).ToString();

            if(_counter <= 0)
            {
                counter.text = "0";
                _counter = 0;
                startCounter = false;
                counter.transform.DOScale(Vector3.zero, 0.2f).OnComplete(() => counter.enabled = false);

                GameController.Instance.Playing();
            }
        }
    }

    void OnStartCountDown()
    {
        //counter.enabled = true;
        counter.text = "3";

        counter.transform.DOScale(Vector3.one, 0.2f).SetDelay(0.1f).OnComplete(() => onCountDown()); 
    }

    void onCountDown()
    {
        startCounter = true;
    }
}
