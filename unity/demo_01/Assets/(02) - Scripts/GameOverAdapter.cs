﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class GameOverAdapter : MonoBehaviour
{
    public GameObject gameOver;
    public TMP_Text gameOverText;
    public Ease onShowEase;
    public Ease OnHideEase;

    private void OnEnable()
    {
        GameController.onGameOver += OnGameOverShow;
        GameController.onPlayAgain += OnPlayAgain;
    }

    private void OnDisable()
    {
        GameController.onGameOver -= OnGameOverShow;
        GameController.onPlayAgain -= OnPlayAgain;
        gameOver.SetActive(false);
    }
    private void Destroy()
    {
        GameController.onGameOver -= OnGameOverShow;
        GameController.onPlayAgain -= OnPlayAgain;
        gameOver.SetActive(false);
    }

    void OnGameOverShow()
    {
        gameOver.SetActive(true);
        gameOver.transform.DOScale(Vector3.one, 0.2f).SetDelay(0.2f).SetEase(onShowEase).OnComplete(() => OnShowAnimFinished());
    }

    void OnShowAnimFinished()
    {

    }

    void OnGameOverHide()
    {
        gameOver.transform.DOScale(Vector3.zero, 0.2f).SetEase(OnHideEase).OnComplete(() => OnHideAnimFinished());
    }

    void OnHideAnimFinished()
    {
        gameOver.SetActive(false);
    }

    void OnPlayAgain()
    {
        OnGameOverHide();
    }
}
