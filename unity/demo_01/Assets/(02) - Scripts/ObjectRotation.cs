﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectRotation : MonoBehaviour
{
    [System.Serializable]
    private class RotationElements
    {
        public float speed;
        public float duration;
    }

    [SerializeField]
    private RotationElements rotationPattern;
    [SerializeField]
    private WheelJoint2D wheelJoint2D;
    [SerializeField]
    private JointMotor2D motor;
    [SerializeField]
    private bool enableRotation;

    private void Awake()
    {
        wheelJoint2D = GetComponent<WheelJoint2D>();
        motor = new JointMotor2D();
        GameController.onGameStart += InitializeRotation;
        GameController.onLevelUp += InitializeRotation;
        GameController.onGameOver += InitializeRotation;
    }
    private void OnEnable()
    {
        GameController.onGameStart += InitializeRotation;
        GameController.onLevelUp += InitializeRotation;
        GameController.onGameOver += InitializeRotation;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= InitializeRotation;
        GameController.onLevelUp += InitializeRotation;
        GameController.onGameOver -= InitializeRotation;
    }
    private void Destroy()
    {
        GameController.onGameStart -= InitializeRotation;
        GameController.onLevelUp += InitializeRotation;
        GameController.onGameOver -= InitializeRotation;
    }

    public void InitializeRotation()
    {
        //StopCoroutine("StartRotation");
        //enableRotation = false;

        rotationPattern.duration = GameController.Instance.currentLevelData.rotationDuration;
        rotationPattern.speed = GameController.Instance.currentLevelData.rotationSpeed;
        enableRotation = true;
        StartCoroutine("StartRotation");
    }

    private IEnumerator StartRotation()
    {
        //int rotationIndex = 0;
        while(enableRotation)
        {
            yield return new WaitForFixedUpdate();

            motor.motorSpeed = rotationPattern.speed;
            motor.maxMotorTorque = 10000;

            wheelJoint2D.motor = motor;

            //yield return new WaitForSeconds(rotationPattern.duration);

            //rotationIndex = 0;
            //rotationIndex = rotationIndex < rotationPattern.Length ? rotationIndex : 0;
        }
    }

    public void StopRotation()
    {
        //enableRotation = false;
        // StopCoroutine("StartRotation");
        motor.motorSpeed = 300f;
        motor.maxMotorTorque = 10000f;

        wheelJoint2D.motor = motor;
        Debug.Log("STOP ROTATION");
        enableRotation = false;
        StartCoroutine("StartRotation");
    }
    //StartCoroutine("StartRotation");


    void OnGameOver()
    {
        StopRotation();
    }


}
