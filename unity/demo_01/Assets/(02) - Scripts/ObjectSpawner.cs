﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSpawner : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    private GameObject prefab;
    [SerializeField]
    private int numberOfPrefabToSpawn;
    [SerializeField]
    private bool spawnAtStart;

    public Vector3 prefabPosition;
    public bool useRotation = false;
    public Vector3 prefabRotation;

    private Spawner spawner;

    private void Awake()
    {
        spawner = this.gameObject.GetComponent<Spawner>();
    }

    public void OnEnable()
    {
        spawner.SpawnAll(prefab, numberOfPrefabToSpawn, this.transform);
    }

    private void Start()
    {

        if(spawnAtStart)
            SpawnGameObject();
    }

    public void SpawnGameObject()
    {
        GameObject obj = spawner.Spawn();
        obj.transform.position = new Vector3(prefabPosition.x, prefabPosition.y, prefabPosition.z);
        if(!useRotation)
            return;
        obj.transform.eulerAngles = new Vector3(prefabRotation.x, prefabRotation.y, prefabRotation.z);
    }

    public void DespawnGameobject()
    {
        spawner.Despawn();
    }

    public void DespawnGameobject(GameObject obj)
    {
        spawner.DespawnGameObject(obj);
    }

    public void DespawnAll()
    {
        spawner.DespawnAll();
    }
}
