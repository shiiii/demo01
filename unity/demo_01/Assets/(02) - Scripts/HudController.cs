﻿using System.Collections;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class HudController : MonoBehaviour
{
    // Public Variables

    [SerializeField]
    private GameObject overlay;

    [SerializeField]
    private GameObject start;

    [SerializeField]
    private GameObject play;

    //[SerializeField]
    //private GameObject scoreHolder;
    //[SerializeField]
    //private GameObject gameOverHolder;
    //[SerializeField]
    //private GameObject TapToStart;
    //[SerializeField]
    //private Button startButton;


    //// Private Variables
    //private bool isActive = false;
    //private void Awake()
    //{
    //    scoreHolder.SetActive(false);
    //    gameOverHolder.SetActive(false);
    //    TapToStart.SetActive(false);
    //    startButton.interactable = false;
    //}

    public void Start()
    {
        //OnStart();
    }

    IEnumerator tempChecker()
    {

        yield return new WaitForSeconds(0.3f);
        
        // TODO : temporarily set verif notification

    }

    //public void OnStart()
    //{
    //    onShowHud(0.3f, 0);
    //    gameOverHolder.transform.localScale = new Vector3(0.4f, 0.4f, 0.4f);

    //    gameOverHolder.SetActive(true);

    //    gameOverHolder.GetComponent<TMP_Text>().text = "PLAY";
    //    gameOverHolder.GetComponent<TMP_Text>().color = Color.green;

    //    gameOverHolder.transform.DOScale(1, 0.25f).SetDelay(0.4f)
    //        .SetEase(Ease.Flash).OnComplete(() => startButton.interactable = true);

    //}

    ///*public void StartGame()
    //{
    //    onStartGame();
    //}

    //private void onStartGame()
    //{
    //    GameController.Instance.InitializeLevel();
    //}*/

    //public void onShowHud(float duration, float delay = 0)
    //{
    //    overlay.transform.DOLocalMove(Vector3.zero, duration).SetEase(Ease.InOutBounce).SetDelay(delay);
    //}

    //public void onHideHud(float delay = 0f)
    //{
    //    overlay.transform.DOLocalMove(new Vector3(0f, 2300f, 0f), 0.3f)
    //        .SetEase(Ease.InFlash).SetDelay(delay);

    //    gameOverHolder.transform.DOScale(0f, 0.1f).SetEase(Ease.Linear).SetDelay(0.5f);

    //    isActive = false;

    //    GameController.Instance.gameStatus = GameController.GameStatus.Playing;
    //    startButton.interactable = false;
    //}

    //public void OnButtonClick()
    //{
    //    if(GameController.Instance.gameStatus != GameController.GameStatus.GameOver)
    //    {
    //        startButton.interactable = false;

    //        gameOverHolder.GetComponent<TMP_Text>().text = "";

    //        gameOverHolder.transform.DOScale(0f, 0.1f).SetEase(Ease.Linear);

    //        scoreHolder.SetActive(false);
    //        TapToStart.SetActive(false);
    //    }
    //}


    //// This methos is called at the GameController inspector UnityEvent invoke.
    //public void onShowLevelHud()
    //{
    //    Debug.Log("onShowLevelHud");
    //    if (!isActive)
    //        onShowHud(0.05f, 0.2f);

    //    GameController.Instance.gameStatus = GameController.GameStatus.Pause;

    //    gameOverHolder.transform.DOScale(1, 0.3f)
    //      .SetEase(Ease.Linear).OnComplete(() => onHideLevelHud());

    //    gameOverHolder.GetComponent<TMP_Text>().text = "LEVEL\n" + GameController.Instance.currentLevelCount;
    //    gameOverHolder.GetComponent<TMP_Text>().color = Color.white;

    //}

    //private void onHideLevelHud()
    //{
    //    onHideHud(0.3f);

    //    GameController.Instance.LevelUp();
    //}

    //public void GameOver()
    //{
    //    if(GameController.Instance.gameStatus != GameController.GameStatus.GameOver)
    //        return;

    //    if(!isActive)
    //        onShowHud(0.03f);

    //    scoreHolder.SetActive(true);
    //    GameObject _score = scoreHolder.transform.Find("score").gameObject;
    //    _score.GetComponent<TMP_Text>().text = Score.Instance.ScoreValue.ToString();

    //    gameOverHolder.GetComponent<TMP_Text>().text = "Game Over!";
    //    gameOverHolder.GetComponent<TMP_Text>().color = Color.red;
    //    gameOverHolder.SetActive(true);

    //    gameOverHolder.transform.DOScale(1f, 0.5f).SetEase(Ease.Linear).SetDelay(0.8f).OnComplete( () => OnGameOverPopup());

    //}

    //private void OnGameOverPopup()
    //{
    //    TapToStart.SetActive(true);
    //    startButton.interactable = true;
    //}

}
