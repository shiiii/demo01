﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    // Private Variables
    private List<GameObject> objectPrefabs;

    public GameObject Spawn()
    {
        for(int i = 0; i < objectPrefabs.Count; i++)
        {
            if(!objectPrefabs[i].activeInHierarchy)
            {
                objectPrefabs[i].SetActive(true);
                return objectPrefabs[i];
            }
        }

        return null;
    }

    public void SpawnAll(GameObject prefab, int amount, Transform parent)
    {
        objectPrefabs = new List<GameObject>();
        for(int i = 0; i < amount; i++)
        {
            GameObject obj = Instantiate(prefab, parent);
            obj.SetActive(false);
            objectPrefabs.Add(obj);
        }
    }

    public void DespawnGameObject(GameObject obj)
    {
        for(int i = 0; i < objectPrefabs.Count; i++)
        {
            if(obj == objectPrefabs[i])
            {
                Despawn(obj);
                return;
            }
        }
    }

    public void DespawnAll()
    {
        Debug.Log("Despawn All count : " + objectPrefabs.Count);
        for(int i = 0; i < objectPrefabs.Count; i++)
        {
            Despawn(objectPrefabs[i]);
        }
    }

    private void Despawn(GameObject obj)
    {
        if(this.gameObject.activeInHierarchy)
        {
            obj.transform.SetParent(this.gameObject.transform);
            obj.transform.position = this.transform.position;
        }

        obj.SetActive(false);
    }

    public void Despawn()
    {
        Debug.Log("Despawn");
        for(int i = 0; i < objectPrefabs.Count; i++)
        {
            if(objectPrefabs[i].activeInHierarchy)
            {
                Debug.Log("Despawn this object : " + objectPrefabs[i].name);
                objectPrefabs[i].transform.SetParent(this.gameObject.transform);
                objectPrefabs[i].transform.position = this.transform.position;
                objectPrefabs[i].SetActive(false);
                return;
            }
        }
    }
}
