﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class PlayAgainAdapter : MonoBehaviour
{
    public GameObject playAgain;
    public Ease showAnim;


    // Private Variables
    private Tweener tween;
    private void Awake()
    {
        GameController.onGameOver += OnShowPlayAgain;
        GameController.onShowPlayAgain += OnShowPlayAgain;
        playAgain.transform.localScale = Vector3.zero;
        playAgain.SetActive(true);
    }

    private void OnEnable()
    {
        GameController.onGameOver += OnShowPlayAgain;
        GameController.onShowPlayAgain += OnShowPlayAgain;
        playAgain.transform.localScale = Vector3.zero;
        playAgain.SetActive(true);
    }

    private void OnDisable()
    {
        GameController.onGameOver -= OnShowPlayAgain;
        GameController.onShowPlayAgain -= OnShowPlayAgain;
        playAgain.SetActive(false);
    }

    private void OnDestroy()
    {
        GameController.onGameOver -= OnShowPlayAgain;
        GameController.onShowPlayAgain -= OnShowPlayAgain;
        playAgain.SetActive(false);
    }


    void OnShowPlayAgain()
    {
        if(GameController.Instance.hasGiftBox)
            return;

        playAgain.SetActive(true);
        playAgain.GetComponentInChildren<Button>().interactable = false;
        tween = playAgain.transform.DOScale(Vector3.one, 0.3f).SetDelay(0.3f).SetEase(showAnim).OnComplete(() => OnShowDone());
    }

    void OnShowDone()
    {
        if(!tween.IsComplete())
            return;

        playAgain.GetComponentInChildren<Button>().interactable = true;

    }

    void OnHide()
    {
        tween = playAgain.transform.DOScale(new Vector3(1.2f, 1.2f, 1.2f), 0.2f).SetDelay(0.3f).SetEase(showAnim).OnComplete(() => onHodeDone());
    }

    void onHodeDone()
    {
        if(!tween.IsComplete())
            return;

        playAgain.SetActive(false);

    }

    public void PlayAgain()
    {
        OnHide();
        GameController.Instance.PlayAgain();
    }
}
