﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class MessageTestHandler : MonoBehaviour {

    public TMP_Text display;

    // Private Variables
    private string _data = "";


    public delegate void CreditVerified();
    public static CreditVerified creditVerified;

    private void OnEnable()
    {
        UnityMessageManager.Instance.OnRNMessage += OnReceivedReactNativeMessage;
        display.text = "";
    }

    private void Awake()
    {
        UnityMessageManager.Instance.OnRNMessage += OnReceivedReactNativeMessage;
    }

    private void OnDisable()
    {
        UnityMessageManager.Instance.OnRNMessage -= OnReceivedReactNativeMessage;
    }
    private void Destroy()
    {
        UnityMessageManager.Instance.OnRNMessage -= OnReceivedReactNativeMessage;
    }

    public void OnReceivedReactNativeMessage(MessageHandler message)
    {
        var _message = message.getData<string>();

         display.text = _message + "...";
        //message.send(new {
        //    CallbackTest = "I am Unity callback, Message is receive!"
        //});

        //StartCoroutine(QuitGame());
    }

    public void SendMessageToReactNative()
    {
        UnityMessageManager.Instance.SendMessageToRN(new UnityMessage() {
            name = "verification",
            callBack = (data) => { Verified(); }
        });
    }

    public void VerificationMessage()
    {
        UnityMessageManager.Instance.SendMessageToRN(new UnityMessage() {
            name = "verification",
            callBack = (data) => { Verified(); }
        });
    }

    void Verified()
    {
        display.text = "verified";
        if(creditVerified != null)
            creditVerified.Invoke();
    }

    IEnumerator QuitGame()
    {
        yield return new WaitForSeconds(1f);

        OnQuitUnity();
    }

    // TODO : 
    public void OnQuitUnity()
    {
#if UNITY_ANDROID
        AndroidJavaClass playerClass =
        new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject activity =
        playerClass.GetStatic<AndroidJavaObject>("currentActivity");

        AndroidJavaObject activityUtils =
        new AndroidJavaObject("com.google.unity.ActivityUtils", activity);
        activityUtils.Call("finish");

        //System.Diagnostics.Process.GetCurrentProcess().Kill();
        // Add Application.quit();
        //Application.Quit();
#endif
    }

}
