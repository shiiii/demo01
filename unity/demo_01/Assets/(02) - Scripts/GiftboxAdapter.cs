﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class GiftboxAdapter : MonoBehaviour
{
    public GameObject giftboxholder;
    public Ease showAnim;

    // Private Variables
    Animator anim;
    private Tween tween;

    private void OnEnable()
    {
        GameController.onGameOver += OnShowGiftbox;
        GameController.onPlayAgain += OnPlayAgain;

        anim = giftboxholder.GetComponent<Animator>();

        //show = new Tweener();
    }

    private void OnDestroy()
    {
        GameController.onGameOver -= OnShowGiftbox;
        GameController.onPlayAgain -= OnPlayAgain;
    }

    private void OnDisable()
    {
        GameController.onGameOver -= OnShowGiftbox;
        GameController.onPlayAgain -= OnPlayAgain;
        anim.SetTrigger("exit");
    }

    void OnShowGiftbox()
    {
        if(!GameController.Instance.hasGiftBox)
            return;

        giftboxholder.SetActive(true);
        tween = giftboxholder.transform.DOScale(Vector3.one, 0.3f).SetEase(showAnim).SetDelay(0.2f).OnComplete(() => OnShowDone());

        Debug.Log("gifbox shown");
    }

    void OnShowDone()
    {
        if(!tween.IsComplete())
            return;

        anim.SetTrigger("play");
    }

    public void PlayAnimation()
    {
        anim.SetTrigger("explode");
    }

    void OnPlayAgain()
    {
        Debug.Log("OnPlayAgain gift box adapter");
        tween = giftboxholder.transform.DOScale(Vector3.zero, 0.3f).SetEase(showAnim).OnComplete(() => OnHideDone());
    }

    void OnHideDone()
    {
        if(!tween.IsComplete())
            return;

        giftboxholder.SetActive(false);

        tween.Rewind();

    }


}
