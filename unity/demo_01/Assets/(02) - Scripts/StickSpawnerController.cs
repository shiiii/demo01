﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickSpawnerController : MonoBehaviour
{
    [SerializeField]
    private int numberOfStickToSpawn;
    [SerializeField]
    private ObjectSpawner objectSpawner;
    [SerializeField]
    private GameObject rotatingObject;

    private int counter = 0;

    private void Awake()
    {
        if(objectSpawner == null)
            objectSpawner = GetComponent<ObjectSpawner>();

        //GameController.onGameStart += OnLevelUp;
        //GameController.onLevelUp += OnLevelUp;
    }

    private void OnEnable()
    {
        GameController.onGameStart += OnLevelUp;
        GameController.onLevelUp += OnLevelUp;
        GameController.onGameOver += OnGameOver;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onGameOver -= OnGameOver;
    }


    private void Destroy()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onGameOver -= OnGameOver;
    }

    public void SpawnStick()
    {
        if(counter > 0)
        {
            objectSpawner.SpawnGameObject();
            counter--;
        } else
        {
            GameController.Instance.LevelUp();
        }
    }

    public void ResetStickValue()
    {
        numberOfStickToSpawn = GameController.Instance.currentLevelData.objectCount;
        counter = numberOfStickToSpawn;

        SpawnStick();
    }

    private void InitializeStick()
    {
        objectSpawner.DespawnAll();
    }

    private void OnLevelUp()
    {
        objectSpawner.DespawnAll();

        ResetStickValue();
    }

    private void OnGameOver()
    {
        InitializeStick();
    }
}
