﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Level")]
public class Level : ScriptableObject
{
    [SerializeField]
    private int score;
    [SerializeField]
    private Sprite stickSprite;
    [SerializeField]
    private int objectCount;
    [SerializeField]
    private Sprite rotatingSprite;
    [SerializeField]
    private float rotationSpeed;
    [SerializeField]
    private float rotationDuration;
    [SerializeField]
    private bool showGiftBox;
    [SerializeField]
    private int numberOfGiftbox;
    [SerializeField]
    private bool showBomb;
    [SerializeField]
    private int numberOfBombs;

    public int Score
    {
        get {
            return score;
        }
    }


    public Sprite StickSprite
    {
        get{
            return stickSprite;
        }
    }

    public int ObjectCount
    {
        get {
            return objectCount;
        }
    }

    public Sprite RotatingSprite
    {
        get {
            return rotatingSprite;
        }
    }

    public float RotationSpeed
    {
        get {
            return rotationSpeed;
        }
    }

    public float RotationDuration
    {
        get {
            return rotationDuration;
        }
    }

    public bool ShowGiftBox
    {
        get {
            return showGiftBox;
        }
    }

    public bool ShowBomb
    {
        get {
            return showBomb;
        }
    }

    public int NumberOfBomb
    {
        get {
            return numberOfBombs;
        }
    }

    public int NumberOfGiftbox
    {
        get {
            return numberOfGiftbox;
        }
    }
}
