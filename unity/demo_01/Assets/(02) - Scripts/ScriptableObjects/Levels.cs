﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Levels")]
public class Levels : ScriptableObject
{
    public Level[] level;
}
