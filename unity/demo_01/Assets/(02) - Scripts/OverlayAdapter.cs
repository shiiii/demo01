﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class OverlayAdapter : MonoBehaviour
{
    public Image overlay;

    private void OnEnable()
    {
        GameController.onPlaying += OnHideOverlay;
        GameController.onGameOver += OnShowOverlay;
            ;
    }

    private void Awake()
    {
        GameController.onPlaying += OnHideOverlay;
    }

    private void OnDisable()
    {
        GameController.onPlaying -= OnHideOverlay;
    }
    private void Destroy()
    {
        GameController.onPlaying -= OnHideOverlay;
    }

    void OnHideOverlay()
    {
        overlay.enabled = false;
    }

    void OnShowOverlay()
    {
        overlay.enabled = true;
    }
}
