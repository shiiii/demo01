﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayAdapter : MonoBehaviour
{
    public GameObject play;
    public Ease onShowEase;

    public delegate void OnClickedButtonPlay();
    public static OnClickedButtonPlay onClickedPlay;

    // Private Variables
    private Tween tween;

    private void OnEnable()
    {
        Verification.creditVerified += OnShowPlayButton;
    }

    private void Awake()
    {
        Verification.creditVerified += OnShowPlayButton;
    }

    private void OnDisable()
    {
        Verification.creditVerified -= OnShowPlayButton;
    }
    private void Destroy()
    {
        Verification.creditVerified -= OnShowPlayButton;
    }

    void OnShowPlayButton()
    {
        play.SetActive(true);
        play.transform.DOScale(Vector3.one, 0.1f).SetEase(onShowEase).SetDelay(0.15f);
    }

    public void OnHidePlay()
    {
        tween = play.transform.DOScale(Vector3.zero, 0.3f).SetEase(onShowEase).OnComplete(() => OnPLay());
     }

    void OnPLay()
    {

        if(!tween.IsComplete())
            return;

        play.SetActive(false);
        Debug.Log("Here");

        if (onClickedPlay != null)
            onClickedPlay.Invoke();
    }
}
