﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public int credit;
    public bool hasGiftBox;
    public Levels levels;
    public int currentLevelCount = 0;
    public GameStatus gameStatus = GameStatus.Start;
    public CurrentLevelData currentLevelData;

    // Events
    public delegate void OnPlayAgain();
    public static OnPlayAgain onPlayAgain;

    public delegate void OnInitializeGameUI();
    public static OnInitializeGameUI onInitializeGameUI;

    public delegate void OnGameStart();
    public static OnGameStart onGameStart;

    public delegate void OnPlaying();
    public static OnPlaying onPlaying;

    public delegate void OnLevelUp();
    public static OnLevelUp onLevelUp;

    public delegate void OnGameOver();
    public static OnGameOver onGameOver;

    public delegate void OnShowGiftbox();
    public static OnShowGiftbox onShowGiftbox;

    public delegate void OnShowPlayAgain();
    public static OnShowPlayAgain onShowPlayAgain;

    public enum GameStatus
    {
        Initializing,
        Start,
        Playing,
        Pause,
        Reward,
        GameOver
    }

    [Serializable]
    public class CurrentLevelData
    {
        public int score;
        public Sprite stickSprite;
        public int objectCount;
        public Sprite rotatingSprite;
        public float rotationSpeed;
        public float rotationDuration;
        public bool showGiftBox;
        public bool showBomb;
        public int numberOfBomb;
        public int numberOfGiftbox;
    }

    private static GameController _instance;
    public static GameController Instance { get { return _instance; } }
    
    void Awake()
    {
        //if(FindObjectOfType(typeof(GameController)) == null)
        //{
        //    GameObject gameController = new GameObject();
        //    gameController.AddComponent<GameController>();
        //    _instance = gameController.GetComponent<GameController>();
        //} else
        //{
        //    _instance = (GameController)FindObjectOfType(typeof(GameController));
        //    DontDestroyOnLoad(gameObject);
        //}

        if(_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }

        Verification.creditVerified += InitializeGameUI;
        PlayAdapter.onClickedPlay += OnClickedPlay;
    }


    private void OnEnable()
    {
        Verification.creditVerified += InitializeGameUI;
        PlayAdapter.onClickedPlay += OnClickedPlay;
    }

    private void OnDisable()
    {
        Verification.creditVerified -= InitializeGameUI;
        PlayAdapter.onClickedPlay -= OnClickedPlay;
    }
    private void Destroy()
    {
        Verification.creditVerified -= InitializeGameUI;
        PlayAdapter.onClickedPlay -= OnClickedPlay;
    }

    public void InitializeGameUI()
    {
        gameStatus = GameStatus.Initializing;

        //InitializeLevelData();
        if(this != Instance)
            return;

        if(onInitializeGameUI != null)
            onInitializeGameUI.Invoke();
    }

    public void InitializeLevelData()
    {
        currentLevelCount++;

        PopulateLevelData();
    }

    public void PopulateLevelData()
    {
        if(gameStatus == GameStatus.GameOver)
            return;

        if(currentLevelCount > levels.level.Length)
        {
            currentLevelData.score = levels.level[levels.level.Length - 1 ].Score;
            currentLevelData.stickSprite = levels.level[levels.level.Length - 1].StickSprite;
            currentLevelData.objectCount = levels.level[levels.level.Length - 1].ObjectCount;
            currentLevelData.rotatingSprite = levels.level[levels.level.Length - 1].RotatingSprite;
            currentLevelData.rotationSpeed = levels.level[levels.level.Length - 1].RotationSpeed;
            currentLevelData.rotationDuration = levels.level[levels.level.Length - 1].RotationDuration;
            currentLevelData.showGiftBox = levels.level[levels.level.Length - 1].ShowGiftBox;
            currentLevelData.showBomb = levels.level[levels.level.Length - 1].ShowBomb;
            currentLevelData.numberOfBomb = levels.level[levels.level.Length - 1].NumberOfBomb;
            currentLevelData.numberOfGiftbox = levels.level[levels.level.Length - 1].NumberOfGiftbox;
        } else
        {
            currentLevelData.score = levels.level[currentLevelCount - 1 ].Score;
            currentLevelData.stickSprite = levels.level[currentLevelCount - 1].StickSprite;
            currentLevelData.objectCount = levels.level[currentLevelCount - 1].ObjectCount;
            currentLevelData.rotatingSprite = levels.level[currentLevelCount - 1].RotatingSprite;
            currentLevelData.rotationSpeed = levels.level[currentLevelCount - 1].RotationSpeed;
            currentLevelData.rotationDuration = levels.level[currentLevelCount - 1].RotationDuration;
            currentLevelData.showGiftBox = levels.level[currentLevelCount - 1].ShowGiftBox;
            currentLevelData.showBomb = levels.level[currentLevelCount - 1].ShowBomb;
            currentLevelData.numberOfBomb = levels.level[currentLevelCount - 1].NumberOfBomb;
            currentLevelData.numberOfGiftbox = levels.level[currentLevelCount - 1].NumberOfGiftbox;
        }
    }

    public void ResetGame()
    {
        gameStatus = GameStatus.Start;

        //currentLevelCount = 0;
        
        //InitializeGameData();
    }

    public void ResetLevel()
    {
        gameStatus = GameStatus.Initializing;

        PopulateLevelData();
    }

    public void OnClickedPlay()
    {
        gameStatus = GameStatus.Start;

        InitializeLevelData();

        if(this != Instance)
            return;

        if(onGameStart != null)
            onGameStart.Invoke();
    }

    public void Playing()
    {
        gameStatus = GameStatus.Playing;

        if(this != Instance)
            return;

        if(onPlaying != null)
            onPlaying.Invoke();
    }

    public void LevelUp()
    {
        gameStatus = GameStatus.Pause;

        InitializeLevelData();

        if(this != Instance)
            return;

        if(onLevelUp != null)
            onLevelUp.Invoke();

        gameStatus = GameStatus.Playing;
    }

    public void OnCollisionWithSameObject()
    {
        gameStatus = GameStatus.GameOver;
        Debug.Log("Status : " + GameController.Instance.gameStatus);

    }

    public void GameOver()
    {
        gameStatus = GameStatus.Reward;

        currentLevelData.rotationSpeed = 0;

        if (onGameOver != null)
            onGameOver.Invoke();


        OnGiftboxOpened();

    }

    public void HasGiftbox()
    {
        hasGiftBox = true;
    }

    public void OnGiftboxOpened()
    {
        if(hasGiftBox)
            return;

        Debug.Log("onShowPlayAgain");
        if(onShowPlayAgain != null)
            onShowPlayAgain.Invoke();
    }

    public void PlayAgain()
    {
        gameStatus = GameStatus.Initializing;

        currentLevelCount = 0;

        //InitializeLevelData();


        if(onPlayAgain != null)
            onPlayAgain.Invoke();

    }

    public void Pause()
    {
        Debug.Log("Pause The game");
    }
}
