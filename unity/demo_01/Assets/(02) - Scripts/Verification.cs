﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class Verification : MonoBehaviour
{
    public TMP_Text verif_text;
    public GameObject verifContainer;
    public GameObject loading;

    public delegate void CreditVerified();
    public static CreditVerified creditVerified;

    private void Awake()
    {
        UnityMessageManager.Instance.OnRNMessage += OnReceivedReactNativeMessage;
        GameController.onPlayAgain += OnPlayAgain;
    }

    private void OnEnable()
    {
        UnityMessageManager.Instance.OnRNMessage += OnReceivedReactNativeMessage;
        GameController.onPlayAgain += OnPlayAgain;
    }

    private void OnDisable()
    {
        UnityMessageManager.Instance.OnRNMessage -= OnReceivedReactNativeMessage;
        GameController.onPlayAgain -= OnPlayAgain;
    }

    private void Destroy()
    {
        UnityMessageManager.Instance.OnRNMessage -= OnReceivedReactNativeMessage;
        GameController.onPlayAgain -= OnPlayAgain;
    }

    public void OnReceivedReactNativeMessage(MessageHandler message)
    {

    }

    private void Start()
    {
        if (GameController.Instance.credit == 0)
            ShowVerificationPopUp();
    }

    public void VerificationMessage()
    {
        UnityMessageManager.Instance.SendMessageToRN(new UnityMessage() {
            name = "verification",
            callBack = (data) => { Verified(); }
        });
    }

    public void Verified()
    {
        Time.timeScale = 0;
        //Debug.Log("Verified()");
        //HideVerificationPopUp();

        //GameController.Instance.credit = 1;

        //if(creditVerified != null)
        //    creditVerified.Invoke();
    }

    public void HideVerificationPopUp()
    {
        Debug.Log("HideVerificationPopUp()");
        loading.SetActive(false);
        verifContainer.SetActive(false);
        verifContainer.transform.localScale = Vector3.zero;
    }

    void OnPlayAgain()
    {
        ShowVerificationPopUp();
    }

    public void ShowVerificationPopUp()
    {
        loading.SetActive(true);
        verifContainer.SetActive(true);
        verifContainer.transform.DOScale(new Vector3(1, 1, 1), 0.3f).SetDelay(0.2f).SetEase(Ease.InElastic);
    }

}
