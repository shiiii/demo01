﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Giftbox : MonoBehaviour
{
    public ParticleSystem vfx;
    public Ease explosionEase;
    public GameObject gifboxSprite;

    // Private Varialbes
    Vector3 origsize;

    void Awake()
    {
        origsize = new Vector3();
        origsize = gifboxSprite.transform.localScale;
        //GameController.onInitializeGameUI += OnShowScore;
    }

    private void OnEnable()
    {
        //GameController.onInitializeGameUI += OnShowScore;
        gifboxSprite.transform.localScale = origsize;
        gifboxSprite.SetActive(true);
    }

    private void OnDisable()
    {
        //GameController.onInitializeGameUI -= OnShowScore;
    }
    private void Destroy()
    {
        //GameController.onInitializeGameUI -= OnShowScore;
    }
    public void Explode()
    {
        Debug.Log("Explode!!!!!!!!!!!!!!!!!");
        gifboxSprite.transform.DOScale(new Vector3(0.37f, 0.37f, 0.37f), 0.13f).SetEase(explosionEase).OnComplete(() => PlayVfx());
    }

    void PlayVfx()
    {
        vfx.Play();
        gifboxSprite.SetActive(false);
    }
}
