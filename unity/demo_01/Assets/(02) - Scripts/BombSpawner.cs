﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombSpawner : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    public ObjectSpawner bombSpawner;
    [SerializeField]
    public List<Transform> bombPos;
    [SerializeField]
    public List<GameObject> bombs;

    //Private Variables
    private int bombCounts = 0;
    private List<Transform> _bombPos;

    private void OnEnable()
    {
        GameController.onGameStart += OnLevelUp;
        GameController.onLevelUp += OnLevelUp;
    }

    private void OnDisable()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
    }

    private void Destroy()
    {
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
    }

    private void Start()
    {
        _bombPos = new List<Transform>();

        for(int i = 0; i < bombPos.Count; i++)
            _bombPos.Add(bombPos[i]);

        bombs = new List<GameObject>();
    }

    void OnLevelUp()
    {
        RestBomb();

        ShowBomb();
    }

    void RestBomb()
    {
        _bombPos.Clear();

        _bombPos = new List<Transform>();
        for(int i = 0; i < bombPos.Count; i++)
            _bombPos.Add(bombPos[i]);

        bombSpawner.DespawnAll();
    }

    void ShowBomb()
    {
        Debug.Log("GameController.Instance.currentLevelData.showBomb: " + GameController.Instance.currentLevelData.showBomb);
        if(!GameController.Instance.currentLevelData.showBomb)
            return;

        bombCounts = GameController.Instance.currentLevelData.numberOfBomb;

        for(int i = 0; i < bombCounts; i++)
        {
            int randomPos = Random.Range(0, (_bombPos.Count));

            bombSpawner.prefabPosition = _bombPos[randomPos].position;
            bombSpawner.useRotation = true;
            Debug.Log("localRotation : " + _bombPos[randomPos].localRotation);
            Debug.Log("eulerAngles : " + _bombPos[randomPos].eulerAngles);
            Debug.Log("localEulerAngles : " + _bombPos[randomPos].localEulerAngles);
            //Quaternion rot = _bombPos[randomPos].localRotation;
            bombSpawner.prefabRotation = _bombPos[randomPos].eulerAngles;            
            _bombPos.Remove(_bombPos[randomPos]);

            bombSpawner.SpawnGameObject();
        }
    }
}
