﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class Timer : MonoBehaviour
{
    // Public Variables
    [SerializeField]
    public TMP_Text timer;
    [SerializeField]
    public float timerValue;
    [SerializeField]
    public Image timerDisplay;
    [SerializeField]
    public GameObject timerContainer;
    public Ease onShowEase;

    // Private Variables
    private float _timer;
    private float _timerValue;

    public void Awake()
    {
        _timerValue = timerValue;
        _timer = 1 / timerValue;
        //GameController.onInitializeGameUI += OnInitializeTimer;
    }

    private void OnEnable()
    {
        GameController.onInitializeGameUI += OnInitializeTimer;
        GameController.onPlayAgain += ResetTimer;
    }

    private void OnDisable()
    {
        GameController.onInitializeGameUI -= OnInitializeTimer;
        GameController.onPlayAgain -= ResetTimer;
    }

    private void Destroy()
    {
        GameController.onInitializeGameUI -= OnInitializeTimer;
        GameController.onPlayAgain -= ResetTimer;
    }

    public void Start()
    {
        ResetTimer();
    }

    private void Update()
    {
        if(GameController.Instance.gameStatus == GameController.GameStatus.Playing)
        {
            timerValue -= Time.deltaTime;
          
            if(timerValue <= 0)
            {
                GameController.Instance.GameOver();
                timer.text = "0";
                timerValue = 0;
            }

            SetTimerDisplayValue();
        }
    }

    public void DeductTime()
    {
        timerValue = timerValue - 0.5f;
        timer.text = timerValue.ToString();
    }

    public void ResetTimer()
    {
        timerValue = _timerValue;

        SetTimerDisplayValue();
    }

    private void SetTimerDisplayValue()
    {
        timerDisplay.fillAmount = _timer * timerValue;
        timer.text = Mathf.RoundToInt(timerValue).ToString();
    }

    private void OnInitializeTimer()
    {
        timerContainer.SetActive(true);
        timerContainer.transform.DOScale(Vector3.one, 0.2f).SetEase(onShowEase).OnComplete(() =>ResetTimer());
    }

}
