﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventSender : MonoBehaviour
{
    public void onSendEvent(/*string _gameobejctName,*/ string _methodName)
    {
        //GameObject obj = GameObject.Find(_gameobejctName);
        if(this.gameObject != null)
            this.gameObject.SendMessage(_methodName);
    }
}
