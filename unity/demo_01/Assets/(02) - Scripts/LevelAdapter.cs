﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class LevelAdapter : MonoBehaviour
{
    public GameObject levelContainer;
    public TMP_Text levelText;
    public TMP_Text level;
    public Ease onShowEase;

    // Private Variable
    //private int _level = 0;

    private void OnEnable()
    {
        GameController.onInitializeGameUI += OnInitializeLevel;
        GameController.onGameStart += OnLevelUp;
        GameController.onLevelUp += OnLevelUp;
        GameController.onPlayAgain += OnLevelUp;
    }

    private void Awake()
    {
        //GameController.onInitializeGameUI += OnInitializeLevel;
        //GameController.onGameStart += OnLevelUp;
        //GameController.onLevelUp += OnLevelUp;
    }

    private void OnDisable()
    {
        GameController.onInitializeGameUI -= OnInitializeLevel;
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onPlayAgain -= OnLevelUp;
    }
    private void Destroy()
    {
        GameController.onInitializeGameUI -= OnInitializeLevel;
        GameController.onGameStart -= OnLevelUp;
        GameController.onLevelUp -= OnLevelUp;
        GameController.onPlayAgain -= OnLevelUp;
    }

    void UpdateLevel()
    {
        level.text = GameController.Instance.currentLevelCount.ToString();
    }

    public void OnInitializeLevel()
    {
        levelContainer.SetActive(true);
        levelContainer.transform.DOScale(Vector3.one, 0.3f).SetEase(onShowEase);
        levelText.text = "LEVEL";
        UpdateLevel();
    }

    public void OnLevelUp()
    {
        Debug.Log("OnLevelUp");
        UpdateLevel();
    }
}
