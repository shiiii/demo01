﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class CollisionEventSender : MonoBehaviour
{
    [Serializable]
    public class OnSendEventMessage
    {
        public string objectTag;
        public string gameobejctName;
        public string methodName;
        public string value;
        public bool thisGameobject;
        public bool parent;

    }

    public List<OnSendEventMessage> sendEventOnCollision;
    public List<OnSendEventMessage> sendEventOnTriggered;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(sendEventOnCollision.Count == 0)
            return;

        for(int i = 0; i < sendEventOnCollision.Count; i++)
        {
            if(collision.gameObject.tag == sendEventOnCollision[i].objectTag)
            {
                GameObject obj = GameObject.Find(sendEventOnCollision[i].gameobejctName);
                obj.SendMessage(sendEventOnCollision[i].methodName, sendEventOnCollision[i].value);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("OnTriggerEnter2D!!!!!!!!!!!!!!!!!!!!");
        if(sendEventOnTriggered.Count == 0)
            return;


        Debug.Log("transform.parent.gameObject : " + transform.parent);
        Debug.Log("transform : " + transform);

        for(int i = 0; i < sendEventOnTriggered.Count; i++)
        {
            if(collision.gameObject.tag == sendEventOnTriggered[i].objectTag)
            {
                if(sendEventOnTriggered[i].gameobejctName != "")
                {
                    GameObject obj = GameObject.Find(sendEventOnTriggered[i].gameobejctName);
                    obj.SendMessage(sendEventOnTriggered[i].methodName);
                }

                if (sendEventOnTriggered[i].thisGameobject)
                    gameObject.SendMessage(sendEventOnTriggered[i].methodName);

                if (sendEventOnTriggered[i].parent)
                {
                    Debug.Log("Ontrigger Enter ");
                    GameObject obj = this.transform.parent.gameObject;
                    obj.SendMessage(sendEventOnTriggered[i].methodName);
                }
                
            }
        }
    }
}
